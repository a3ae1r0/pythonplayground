# Section 10: Regular Expressions

### Table of content
- [Section : Regular Expressions](#section-10-regular-expressions)
  - [Table of content](#table-of-content)
  - [23. Regular Expression Basics](#23-regular-expression-basics)
    - [No-regex Progam](#non-regex-program)
    - [Regex Progam](#regex-program)
      - [The `re` Module](#the-re-module)
  - [24. Regex Groups and the Pipe Character](#24-regex-groups-and-the-pipe-character)
    - [Groups](#groups)
    - [Escape Parenthesis](#escape-character)
    - [`|` Pipe Character](#pipe-character)
  - [25. Repetition in Regex Patterns and Greedy/Nongreedy Matching](#25-repetition-in-regex-patterns-and-greedy-nongreedy-matching)
    - [`?` -- zero or one](#-zero-or-one)
    - [`*` -- zero or more](#-zero-or-more)
    - [`+` -- one or more](#-one-or-more)
    - [`{x}` -- exact x times](#-exact-x-times)
    - [`{x,y}` -- minimum and maximum times](#-minimum-and-maximum-times)
    - [Greedy/Nongreedy](#greedy-nongreedy)

### 23. Regular Expressions Basic
* Regular expressions allow to specify a pattern to search and work for

#### Non-RegEx Program
* Disadvantages:
  * Longer code
  * More complicated to read
  * Perhaps with duplicated code
>Program :: See [`sample_non_regex.py`](./sample_non_regex.py) for a sample of program not using regular expressions

#### RegEx Program
* Advantages:
  * Less lines of code
  * Easier to read and to debug

##### The `re` module
* Module with all the regular expressions functions
  * Usually the patterns use raw strings - to avoid confusing when working with backslashes
  * Import with:
  ```python
  import re
  ```
* Functions available through `re`:
  * `compile()` function
    * Compiles a pattern into a regex object
    * `\d` is the regex for a numeric digit character
  * `search()` method
    * Returns the first occurrence of the pattern
      * Creates a match object
  * `findall()` method
    * Returns a list of all occurrence of the pattern
  * `group()` method
    * Get the matched string from pattern
>Program :: See [`sample_regex.py`](./sample_regex.py) for a sample of program using regular expressions


### 24. Regex Groups and the Pipe Character

#### Groups
* Parenthesis create groups of patterns
  ```python
  # Pattern without groups
  phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
  # With two groups
  phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d-\d\d\d\d)')
  ```
* Parenthesis mark where a group begins and ends
* Inside the `group()` method we can specify a group
  * `group()` or `group(0)`: full matching string
  * `group(1)`: 1st group (parenthesis)
  * `group(2)`: 2nd group
  * And so on...
  * Example:
  ```python
  phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d-\d\d\d\d)')
  mo = phoneNumRegex.search("My number is 415-555-4242")
  print("Complete Number: " + mo.group())
  print("Area code: " + mo.group(1))
  print("Main phone number: " + mo.group(2))
  ```

#### Escape Parenthesis
* Use backslash to escape parenthesis -- literal parenthesis
* Both on open and close parenthesis
  ```python
  phoneNumRegex = re.compile(r'\(\d\d\d\) \d\d\d-\d\d\d\d')
  mo = phoneNumRegex.search("My number is (415) 555-4242")
  ```

#### `|` Pipe Character
* Used to match on of many possible patterns in a regular expression
   ```python
  phoneNumRegex = re.compile(r'Bat(man|mobile|copter|bat)')
  mo = phoneNumRegex.search("Batmobile lost a wheel")
  print(mo.group())
  print(mo.group(1))
  ```
* If the search method don't find the regex pattern, then will return `None`
  * When printing beforehand, will be return a error
  ```python
  phoneNumRegex = re.compile(r'Bat(man|mobile|copter|bat)')
  mo = phoneNumRegex.search("Batmotorcycle lost a wheel")
  print(mo.group())
  ```


### 25. Repetition in Regex Patterns and Greedy/Nongreedy Matching

#### `?` -- zero or one
* Match the preceding group zero or one time
  ```python
  batRegex = re.compile(r'Bat(wo)?man')
  mo0 = batRegex.search("The Adventures of Batman")
  mo1 = batRegex.search("The Adventures of Batwoman")
  mo2 = batRegex.search("The Adventures of Batwowowoman")
  print(mo0.group())
  print(mo1.group())
  # print(mo2.group())      # Returns an error (None value)
  ```
  * Using the phone number again as an example:
    ```python
    phoneRegex = re.compile(r'(\d\d\d-)?\d\d\d-\d\d\d\d')   # Area code is optional
    mo = phoneRegex.search("My phone number is 555-1234. Call me!")
    print(mo.group())
    ```
* Use the backslash character to escape the `?`
  ```python
  re.compile(r'dinner\?')   # "dinner" is not optional. The use of ? is literal
  ```

#### `*` -- zero or more
* Match pattern zero or more times
  ```python
  batRegex = re.compile(r'Bat(wo)*man')
  mo0 = batRegex.search("The Adventures of Batman")
  mo1 = batRegex.search("The Adventures of Batwoman")
  mo2 = batRegex.search("The Adventures of Batwowowoman")
  print(mo0.group())
  print(mo1.group())
  print(mo2.group())
  ```
* Use backslash for escaping
```python
re.compile(r'u r a \*')
```

#### `+` -- one or more
* Match one or more times
  ```python
  batRegex = re.compile(r'Bat(wo)+man')
  mo0 = batRegex.search("The Adventures of Batman")
  mo1 = batRegex.search("The Adventures of Batwoman")
  mo2 = batRegex.search("The Adventures of Batwowowoman")
  # print(mo0.group())      # Return None
  print(mo1.group())
  print(mo2.group())
  ```
* The group preceding the `+` character must appear at least one time. Is not optional.
* Also use backslash character for escape
  ```python
  re.compile(r'2\+2 = 5')
  # Escaping multiple characters
  literal_regex = re.compile(r'\+\*\?')
  mo = literal_regex.search("Today I learned about +*? regex syntax!")
  print(mo.group())

  literal_regex = re.compile(r'(\+\*\?)+')
  mo0 = literal_regex.search("Today I learned about +*?+*?+*? regex syntax!")
  mo1 = literal_regex.search("Today I learned about nice regex syntax!")
  print(mo0.group())
  # print(mo1.group())      # Return None
  ```

#### `{x}` -- exact x times
* Match a pattern a specific number of repetitions of a group
  ```python
  ha_regex = re.compile(r'(Ha){3}')
  mo = ha_regex.search("And he said 'HaHaHa' in my face!")
  print(mo.group())

  phone_regex = re.compile(r"((\d\d\d-)?\d\d\d-\d\d\d\d(,)?){3}")
  mo = phone_regex.search("My numbers are 415-555-1234,555-4242,212-555-0000")
  print(mo.group())
  ```
* Phone number example know more compact
  ```python
  phoneRegex = re.compile(r'((\d){3}-)?(\d){3}-(\d){4}')
  mo = phoneRegex.search("My phone number is 555-12334. Call me!")
  print(mo.group())
  ```

import re                               # Module with regular expressions

# Source string
msg = "Call me 415-555-1011 tomorrow, or at 415-555-9999 for my office line"

# Pass to the compile function the string (raw) with the regex pattern, wich is
# saved in the regular expression object (variable)
phone_number_regex = re.compile(r"\d\d\d-\d\d\d-\d\d\d\d")      # \d --> digit

# Search pattern in variable. Save result in "mo" variable (Match Object)
mo  = phone_number_regex.search(msg)                 # Find 1st match
mo1 = phone_number_regex.findall(msg)                # Find all matches

# Show text (group)
print(mo.group())                                    # Group search together
print(mo1)

def is_phone_number(text):  # 415-555-6633
    if len(text) != 12:
        return False        # Not phone number-sized
    for i in range(3):
        if not text[i].isdecimal():
            return False    # No area code
    if text[3] != "-":
        return False        # Missing dash
    for i in range(4, 7):
        if not text[i].isdecimal():
            return False    # No first 3 digits
    if text[7] != "-":
        return False        # Missing second dash
    for i in range(8, 12):
        if not text[i].isdecimal():
            return False    # Missing last 4 digits
    return True

# Manual testing
# print(is_phone_number("415-555-1234"))
# print(is_phone_number("415-55551234"))
# print(is_phone_number("phone"))
# print(is_phone_number("415-55-15234"))

# Run against a string
msg = "Call me 415-555-1011 tomorrow, or at 415-555-9999 for my office line"
found_number = False
for i in range(len(msg)):
    chunk = msg[i:i+12]                 # Work with "chunks" of 12 chars
    if is_phone_number(chunk):
        print("Phone number found: " + chunk)
        found_number = True
if not found_number:
    print("Could not find any phone number.")

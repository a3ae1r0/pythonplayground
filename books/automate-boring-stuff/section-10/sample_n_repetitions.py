import re

## # ? - zero or one
## batRegex = re.compile(r'Bat(wo)?man')
## mo0 = batRegex.search("The Adventures of Batman")
## mo1 = batRegex.search("The Adventures of Batwoman")
## mo2 = batRegex.search("The Adventures of Batwowowoman")
## print(mo0.group())
## print(mo1.group())
## # print(mo2.group())      # Returns an error (None)
## 
## phoneRegex = re.compile(r'(\d\d\d-)?\d\d\d-\d\d\d\d')   # Optional are code
## mo = phoneRegex.search("My phone number is 555-1234. Call me!")
## print(mo.group())
## 
## # Escape ?
## re.compile(r'dinner\?')     # "dinner" is not optional. The use of ? is literal

## print()

# # * - zero or more
# batRegex = re.compile(r'Bat(wo)*man')
# mo0 = batRegex.search("The Adventures of Batman")
# mo1 = batRegex.search("The Adventures of Batwoman")
# mo2 = batRegex.search("The Adventures of Batwowowoman")
# print(mo0.group())
# print(mo1.group())
# print(mo2.group())

# # Escape *
# re.compile(r'u r a \*')     # "dinner" is not optional. The use of ? is literal

# print()

# # + - one or more
# batRegex = re.compile(r'Bat(wo)+man')
# mo0 = batRegex.search("The Adventures of Batman")
# mo1 = batRegex.search("The Adventures of Batwoman")
# mo2 = batRegex.search("The Adventures of Batwowowoman")
# # print(mo0.group())      # Return None
# print(mo1.group())
# print(mo2.group())

# # Escape *
# re.compile(r'2\ + 2 = 5')     # "dinner" is not optional. The use of ? is literal

# print()

# # Escaping multiple characters
# literal_regex = re.compile(r'\+\*\?')
# mo = literal_regex.search("Today I learned about +*? regex syntax!")
# print(mo.group())

# literal_regex = re.compile(r'(\+\*\?)+')
# mo0 = literal_regex.search("Today I learned about +*?+*?+*? regex syntax!")
# mo1 = literal_regex.search("Today I learned about nice regex syntax!")
# print(mo0.group())
# # print(mo1.group())      # Return None

# print()

# # {x} = match exactly x times
# ha_regex = re.compile(r'(Ha){3}')
# mo = ha_regex.search("And he said 'HaHaHa' in my face!")
# print(mo.group())

# phone_regex = re.compile(r"((\d\d\d-)?\d\d\d-\d\d\d\d(,)?){3}")
# mo = phone_regex.search("My numbers are 415-555-1234,555-4242,212-555-0000")
# print(mo.group())

# # Now more compact
# phoneRegex = re.compile(r'((\d){3}-)?(\d){3}-(\d){4}')   # More compact code
# mo = phoneRegex.search("My phone number is 555-12334. Call me!")
# print(mo.group())

# {x,y} = match at least x, and less than y
ha_regex = re.compile(r'(Ha){3,5}')
mo = ha_regex.search("And he said 'HaHaHa' in my face!")
print(mo.group())
mo = ha_regex.search("And he said 'HaHaHaHaHa' in my face!")
print(mo.group())
mo = ha_regex.search("and he said 'ha' in my face!")
# print(mo.group())     # Returns None
mo = ha_regex.search("and he said 'HaHaHaHaHaHa' in my face!")  # it works, but only show at maximum of 5 repetitions
print(mo.group())

# Just like slices
ha_regex = re.compile(r'(Ha){,5}')      # Range from 0 to 5
ha_regex = re.compile(r'(Ha){3,}')      # Range from 3 to no upper limit (3 or more)

# Experiment (huhu)
# Greedy
digit_regex = re.compile(r'(\d){3,5}')
mo = digit_regex.search("1234567890")
print(mo.group())

# Nongreedy
digit_regex = re.compile(r'(\d){3,5}?')
mo = digit_regex.search("1234567890")
print(mo.group())

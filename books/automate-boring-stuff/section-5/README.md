# Section 5: Writing a Complete Program: Guess the Number

### Table of content
- [Section 5: Writing a Complete Program: Guess the Number](#section-5-criting-a-complete-program-guess-the-number)
  - [Table of content](#table-of-content)
  - [12. Writing a "Guess the Number" Program](#12-writing-a-guess-the-number-progra)

### 12. Writing a "Guess the Number" Program
>**Program ::** See [`guess_number.py`](./guess_number.py) for own version, and [`guess_number_v2.py`](./guess_number_v2.py) for the author's version

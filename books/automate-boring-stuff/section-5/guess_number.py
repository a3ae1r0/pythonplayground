#------------------------------------------------------------------------------
# ma attempt to write the program. Also trying some experiments, and of course
# making it a little more complicated
#------------------------------------------------------------------------------
import random
import sys

#
# getUserName : get name from user input.
#               returns user_name
# 
def getUserName():
    print("Hello. What is your name?")
    user_name = input()
    return user_name                    # return name entered to main

#------------------------------------------------------------
# genRandomNumber : generate a random number - between 
#                   min_number and max_number - using
#                   random.randinit()
#------------------------------------------------------------
# 
def genRandomNumber(min_number, max_number):
    chosen_number = random.randint(min_number, max_number)
    return chosen_number

#------------------------------------------------------------
# getUserGuess : get a number guess from user input. Get 
#                user_name, min_number  and max_number 
#                from main.
#                returns guess
#------------------------------------------------------------
def getUserGuess(user_name, min_number, max_number):
    print("\nWell " + user_name + ", I am thinking of a number between "
            + str(min_number) + " and " + str(max_number) + ".",
            "\nTake a guess.")

    #--- user input---#
    user_guess = int(input())               # cast to integer before saved

    # check input boundaries
    if not ( min_number <= user_guess <= max_number):
        print("\n!!ERROR!!\nThe number entered is not between "
                + str(min_number) + " and " + str(max_number)
                + ". \nTry running the program again, please.")
        sys.exit()                          # exit immediately

    return user_guess
#------------------------------------------------------------
# compareNumbers: compare user guess and random number.
#                 receive userGuess and chosen_number from 
#                 main.
#                 returns number guesses until correct answer
#                 if user takes more than five guesses, then
#                 exit function with exit(), and main should
#                 handle that
#------------------------------------------------------------
def compareNumbers(chosen_number, user_guess, min_number, max_number):
    # 5 attempts. The 6th is not possible (but needed)
    for n_guess in range(1, 7):
        # for n_guess == 6 the user failed. Exit function
        if ( n_guess == 6):
            return n_guess

        #--- compare numbers ---#
        if ( user_guess == chosen_number ):
            return n_guess                      # corret. exit function
        elif ( user_guess > chosen_number ):
            print("\nYour guess is too high")
        elif ( user_guess < chosen_number ):
            print("\nYour guess is too low")
        print("Take a guess.")

        #--- user retry ---#
        user_guess = int(input())

        # check input boundaries
        if not ( min_number <= user_guess <= max_number):
            print("\n!!ERROR!!\nThe number entered is not between "
                    + str(min_number) + " and " + str(max_number)
                    + ". \nTry running the program again, please.")
            sys.exit()

#------------------------------------------------------------
# main : just trying some things. I really don't know what
#        I'm doing
#------------------------------------------------------------
def main():
    # max number to limit random number generator
    min_number = 1
    max_number = 20

    user_name     = getUserName()
    chosen_number = genRandomNumber(min_number, max_number)
    user_guess    = getUserGuess(user_name, min_number, max_number)
    n_guess       = compareNumbers(chosen_number, user_guess, min_number, max_number)

    # final output. print accordingly to n_guess return by compareNumbers()
    if ( n_guess != 6 ):                    # success message
        print("\nGood job, " + user_name + '!' " You guessed my number in "
                + str(n_guess) +  " guesses!")
    else:                                   # insuccess message
        print("\nNope. The number I was thinkin of was " + str(chosen_number))

# other functions inside main
main()

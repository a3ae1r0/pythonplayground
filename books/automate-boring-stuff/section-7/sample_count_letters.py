#
# Count number of occurrences of a letter in a string
#
import pprint               # Pretty Print module

# message = "It was a bright cold day in April, and the clocks were striking \
#           thirteen."
message = "This is a message alsdasdasd asj ajd"
# message = ''' How oft when men are at the point of death
#     Have they been merry! which their keepers call
#     A lightning before death. O, how may I
#     Call this a lightning? O my love! my wife!
#     Death, that hath suck'd the honey of thy breath,
#     Hath had no power yet upon thy beauty.
#     Thou art not conquer'd. Beauty's ensign yet
#     Is crimson in thy lips and in thy cheeks,
#     And death's pale flag is not advanced there.
#     Tybalt, liest thou there in thy bloody sheet?
#     O, what more favour can I do to thee
#     Than with that hand that cut thy youth in twain
#     To sunder his that was thine enemy?
#     Forgive me, cousin.' Ah, dear Juliet,
#     Why art thou yet so fair? Shall I believe
#     That unsubstantial Death is amorous,
#     And that the lean abhorred monster keeps
#     Thee here in dark to be his paramour?
#     For fear of that I still will stay with thee
#     And never from this palace of dim night
#     Depart again. Here, here will I remain
#     With worms that are thy chambermaids. O, here
#     Will I set up my everlasting rest
#     And shake the yoke of inauspicious stars
#     From this world-wearied flesh. Eyes, look your last!
#     Arms, take your last embrace! and, lips, O you
#     The doors of breath, seal with a righteous kiss
#     A dateless bargain to engrossing death!
#     Come, bitter conduct; come, unsavoury guide!
#     Thou desperate pilot, now at once run on
#     The dashing rocks thy seasick weary bark!
#     Here's to my love! [Drinks.] O true apothecary!
#     Thy drugs are quick. Thus with a kiss I die.'''
count = {}              # key-value pair --> "character": number occurrences

for character in message.upper():           # loop through all chars in string
    count.setdefault(character, 0)          # set all chars to start at count 0
    count[character] += 1                   # increment number of occurrence

print(count)
pprint.pprint(count)                        # same as print, but better format
rjtext = pprint.pformat(count)              # save output as a string
print(rjtext)

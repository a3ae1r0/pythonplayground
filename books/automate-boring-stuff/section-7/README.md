# Section 7: Dictionaries

### Table of content
- [Section 7: Dictionaries](#section-7-dictionaries)
  - [Table of content](#table-of-content)
  - [17. The Dictionary Data Type](#17-the-dictionary-datat-ype)
    - [Key-Value Pairs](#key-value-pairs)
    - [`In` and `not in` Operators](#in-and-not-in-operators)
    - [Dictionary Methods](#dictionary-methods)
      - [`keys()`](#keys)
      - [`values()`](#values)
      - [`items()`](#items)
      - [`get()`](#get)
      - [`setdefault()`](#setdefault)
    - [`pprint()` Module](#pprint-module)
  - [18. Data Structures](#18-data-structures)
    - [`type()` Function](#type-function)

### 17. The Dictionary Data Type
* Like a list, a dictionary is a collection of many values
* Dictionaries contain [key-value pairs](#key-value-pairs)
* Are also mutable like lists
  * Variables hold references to dictionary values, not the value itself
* Enclose in curl brackets, instead of square brackets
  ```python
  myPenguin = {"size": "fat", "color": "black-white", "disposition": "funny"}
  ```

#### Key-Value Pairs
```python
myPenguin = {"size": "fat", "color": "black-white", "disposition": "funny"}
|-------|                   |----|  |-----------|
dictionary                   key        value
variable                    |-------------------|
                               key-value pair
```
* Keys are like list's indexes
* Dictionaries are unordered
  * There is no "first" key-value pair in a dictionary
  * This property can be compared with:
    ```python
    [1, 2, 3] == [3, 2, 1]    # List
    False
    {1, 2, 3} == {3, 2, 1}    # Dictionary
    True
    ```
* Keys ("indexes") can have different data types - int, strings, etc.
* Accessing key:
  ```python
  myPenguin["size"]
  'fat'
  ```
  * If key don't exist, an KeyError will exist
    ```python
    myPenguin["ears"]
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      KeyError: 'ears' 
    ```
* Indexes (keys) don't need to start at zero, can be any number
    ```python
    spam = {9999: "Luggage combination", 41: "The answer"}
    ```
#### `In` and `not in` Operators
* Check a dictionary for:
  * Key
    ```python
    "color" in myPenguin    # OR "color" in myPenguin.keys()
    True
    "color" not in myPenguin
    False
    ```
  * Value
    ```python
    "fat" in myPenguin.values()
    True
    ```
#### Dictionary Methods

##### keys()
* Get keys from a dictionary
* Returns a list-like value
* Syntax:
  ```python
  list(myPenguin.keys())
  ['size', 'color', 'disposition']
  ```
* Usage in a for loop:
  ```python
  for k in myPenguin.keys():
      print(k)
  # Output
  size
  color
  disposition
  ```

##### values()
* Get values from a dictionary
* Returns a list-like value
* Syntax:
  ```python
  list(myPenguin.values())
  ['fat', 'black-white', 'funny']
  ```
* Usage in a for loop:
  ```python
  for v in myPenguin.values():
      print(v)
  # Output
  fat
  black-white
  funny
  ```

##### items()
* Get the key-value pair from a dictionary
* Returns a tuple of two items -- key-value pair
* Syntax:
  ```python
  list(myPenguin.items())
  [('size', 'fat'), ('color', 'black-white'), ('disposition', 'funny')]
  ```
* Usage in a for loop:
  ```python
  for k,v in myPenguin.items():
    print(k, v)
  # Output
  size fat
  color black-white
  disposition funny
  ```
>Note: A tuple is similiar to a list, but it is imutable and use parenthesis instead of square brackets 

##### get()
* Get a key from a list; if the key don't exist display a default value (optional parameter)
* Syntax:
  ```python
  myPenguin.get("colorr", 0)    # dictionary.get(key, default value (optional))
  0
  ```
* Prevents error display
  ```python
  # Using a default value
  print("The shape of the penguin is " + str(myPenguin.get("shape", "square")) + ".")
  The shape of the penguin is square.

  # Accessing non-existing key
  print("The shape of the penguin is " + str(myPenguin["shape"]) + ".")
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    KeyError: 'shape'
  ```

##### setdefault()
* Opposite of `get()` method
* Set a value to a key, if the key don't have already one value
* Syntax:
  ```python
  myPenguin.setdefault("food": 3)       # dictionary.setdefault(key, value)
  ```
* Example:
  ```python
  myPenguin = {"size": "fat", "color": "black-white", "disposition": "funny"}
  myPenguin.setdefault("shape", "round")
  'round'
  myPenguin
  {'size': 'fat', 'color': 'black-white', 'disposition': 'funny', 'shape': 'round'}

  # Same as:
  if "shape" not in myPenguin:
    myPenguin["shape"] = "round"
  myPenguin
  {'size': 'fat', 'color': 'black-white', 'disposition': 'funny', 'shape': 'round'}
  ```
* If a key already exists, the value will not change
  ```python
  myPenguin = {"size": "fat", "color": "black-white", "disposition": "funny", "shape": "round"}
  myPenguin.setdefault("shape","big")
  'round'                       # previous value
  myPenguin
  {'size': 'fat', 'color': 'black-white', 'disposition': 'funny', 'shape': 'round'}
  ```
* Good to ensure a key exists

#### `pprint()` Module
* `pprint()` : display a dictionary in a clean way
  ```python
  # With print()
  {'H': 1, 'I': 2, ' ': 6, 'A': 7, 'M': 1, 'E': 2, 'G': 1, 'L': 1, 'D': 4, 'J': 2}

  # With pprint()
  {' ': 6,
   'A': 7,
   'D': 4,
   'E': 2,
   'G': 1,
   'H': 1,
   'I': 2,
   'J': 2,
   'L': 1,
   'M': 1}
  ```
* `pformat()` : returns a string value of the output
  ```python
  # Saving the output in a variable
  rjtext = pprint.pformat(count)              # save output as a string
  print(rjtext)
  ```
>**Program ::** See [`sample_count_letters.py`](./sample_count_letters.py) for a sample program using `pprint()` module


### 18. Data Structures
* Representation of how the data store is structure
* List and dictionaries are useful
* [`pprint()`](#pprint-module) and [`type()`](#type-function) are useful to check/verify the data structure(s) in use
* Example:
  ```python
  # List dictionary data structure
  allPenguins = [{"name:": "john", "size": "fat", "color": "black-white", "disposition": "funny", "shape": "round"},
                 {"name:": "max", "size": "small", "color": "purple", "disposition": "horizontal", "shape": "box"}]

  # Dictionary data structure
  pprint.pprint(theBoard)
  {'low-L': ' ',
   'low-M': ' ',
   'low-R': ' ',
   'mid-L': ' ',
   'mid-M': 'X',
   'mid-M': ' ',
   'top-L': ' ',
   'top-M': ' ',
   'top-R': ' '}
  ```

#### `type()` Function
* Returns data type of the value passed to it
* Example(s):
  ```python
  type(42)                      # <class 'int'>
  type("hel")                   # <class 'str'>
  type(3.14)                    # <class 'float'>
  type([1, 2, 3])               # <class 'list'>
  type({1: "uno", 2: "dos"})    # <class 'dict'>
  ```

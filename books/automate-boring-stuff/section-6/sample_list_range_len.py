# use length of list, as range() argument
def office_lst(supplies):
    for i in range(len(supplies)):
        print("Index " + str(i) + " in supplies is: " + supplies[i])

office_lst(["pens", "staplers", "flame-throwers", "binders"])

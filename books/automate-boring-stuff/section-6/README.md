# Section 6: Lists

### Table of content
- [Section 6: Lists](#section-6-lists)
  - [Table of content](#table-of-content)
  - [13. The List Data Type](#13-the-list-data-type)
    - [Lists of Lists](#lists-of-lists)
    - [Negative Indexes](#negative-indexes)
    - [Slices](#slices)
    - [Changing a List's Items](#changing-a-lists-items)
    - [Slice Shortcuts](#slice-shortcuts)
    - [del Statement](#del-statement)
    - [Strings and Lists Similarities](#strings-and-lists-similarities)
      - [`list()` Function](#list-function)
      - [The in and not in operators](#the-in-and-not-in-operators)
  - [14. For loops with Lists, Multiple Assignment, and Augmented Operators](#14-for-loops-with-lists-multiple-assignment-and-augmented-operators)
    - [For loops with Lists](#for-loops-with-lists)
      - [Range Objects and List-like values](#range-objects-and-list-like-values)
      - [`list()`](#list)
      - [`for i in range(len(someList))`](#for-i-in-range-len-somelist)
    - [Multiple Assignment](#multiple-assignment)
    - [Augmented Operators](#augmented-operators)
  - [15. List Methods](#15-list-methods)
    - [`index()` method](#index-method)
    - [`append()` method](#append-method)
    - [`insert()` method](#insert-method)
    - [`remove()` method](#remove-method)
    - [`sort()` method](#sort-method)
      - [“ASCII-betical” Order](#ascii-betical-order)
  - [!6. Similarities Between Lists and Strings](#16-similarities-between-lists-and-strings)
   - [Mutable and Immutable Data Types](#mutable-and-immutable-data-types)
   - [Creating New Strings from Slices](#creating-new-strings-from-lices)
   - [References](#references)
   - [Passing Lists in Functions Calls](#passing-lists-in-functions-calls)
   - [The `copy.deepcopy()` Function](#the-copy-deepcopy-function)
   - [Line Continuation](#line-continuation)

### 13. The List Data Type
* **List:** a value that contains multiple values
* List values are called **items**
  * `>>> ["dog", "moose", "penguin"]` has three items
* Values are comma-delimited
  * `>>> ["dog", "moose", "penguin"]`
* Use a integer index to access items in a list
  * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> spam[2]`
    <br> `'dog'`
  >**Visualization ::** see [visualization-1](https://automatetheboringstuff.com/eval/13-1.html), and [visualization-2](https://automatetheboringstuff.com/eval/13-1.html)

##### Lists of Lists
* Lists can also contain other's lists values
  * Get item from entire list
    <br> `>>> spam = [["dog", "moose", "penguin"], [10, 20, 30]]`
    <br> `>>> spam[1]`
    <br> `[10, 20, 30]`
  *  Get item from inside the list in the list
    <br> `>>> spam = [["dog", "moose", "penguin"], [10, 20, 30]]`
    <br> `>>> spam[1][2]`
    <br> `30`
  >**Visualization ::** see [visualization-3](https://automatetheboringstuff.com/eval/13-3.html), and [visualization-4](https://automatetheboringstuff.com/eval/13-4.html)

##### Negative Indexes
* When referring to index `-1`, this will mean the last item of the list. `-2` will be second-to-last item, and so on
  * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> spam[-1]`
    <br> `'penguin'`
  * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> spam[-2]`
    <br> `'moose'`

#### Slices
* Index vs Slice
  * **Index:** gets a single value from a list
  * **Slice:** gets several values from a list
* A slice **returns a list**
* A slice has two indexeS values : `variable[start:end]`
  * Starts at the index `start` and goes up -- but not include -- to index `end`
    * `>>> spam = ["dog", "moose", "penguin"]`
      <br> `>>> spam[1:3]`
      <br> `['moose', 'penguin']`

#### Changing a List's Items
* Using index
  * `>>> spam = [10, 20, 30]`
    <br> `>>> spam[1] = "Hello"`
    <br> `>>> spam`
    <br> `[10, 'Hello', 30]`
* Using a slice (multiple values in a list)
  * `>>> spam = [10, 20, 30]`
    <br> `>>> spam[1:3] = ["DOG", "MOOSE", "PENGUIN"]`
    <br> `>>> spam`
    <br> `[10, 'DOG', 'MOOSE', 'PENGUIN']`

#### Slice Shortcuts
* Omit start
  * `spam[:x]` --> starts at index 0, and goes to -- but not include -- index `x`
    * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> spam[:2]`
    <br> `['dog', 'moose']`
* Omit end
  * `spam[x:]` --> starts at index `x`, and goes until the end of the list (length)
    * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> spam[1:]`
    <br> `['moose', 'penguin']`

#### `del` Statement
* Delete values form a list
* `del` statements = *unassignment* statement
* Doesn't leave a index empty -- when deleting, the **next item index will be decremented by one**
  * `>>> spam = ["dog", "moose", "penguin"]`
    <br> `>>> del spam[1]`
    <br> `['dog', 'penguin']`

##### Strings and Lists Similarities
* Many things that can be done with strings, can also be done with lists
  * len()
    * `>>> len([1, 2, 3])`
      <br> `3`
  * Concatenation
    * `>>> [1, 2, 3] + [4, 5, 6]`
      <br> `[1, 2, 3, 4, 5, 6]`
  * List replication
    * `>>> "Sup" * 3`
      <br> `SupSupSup`
* Strings can be seen as a **list of single characteres values**w

##### list() Function
* Convert a **value into a list** by passing it to the list() function
  * `>>> list("Hello")`
    <br> `['h', 'e', 'l', 'l', 'o']`

##### The `in` and `not in` operators
* Check for the **presence or absence of a value** in a list
  * `in`
    * `>>> "howdy" in ["hello", "hi", "howdy", "heyas"]`
      <br> `True`
    * `>>> 42 in ["hello", "hi", "howdy", "heyas"]`
      <br> `False`
  * `not in`
    * `>>> "howdy" not in ["hello", "hi", "howdy", "heyas"]`
      <br> `False`

### 14. For loops with Lists, Multiple Assignment, and Augmented Operators

#### For Loops with Lists
* Iterates over the values in a list

##### Range Objects and List-like values
  * `>>> range(4)`
  <br> `range(0, 4)`
    * `range()` function returns a **range object data type**
    * Similar to `[0, 1, 2, 3]` list --> both are a sequence
##### `list()`
  * Get list values from:
    * **Range object value (i)**
      * `>>> list(range(4))`
      <br> `[0, 1, 2, 3]`
  * **Collection of integers in a list (ii)**
      * `>>> list(range(0, 20, 2))`
      <br> `[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]`
##### `for i in range(len(someList))`
  * Common used to iterate over a list
  * Useful for **portability** and **scalability**
  >**Program ::** See [`sample_list_range_len.py`](./sample_list_range_len.py)

#### Multiple Assignment
* Multiple left-side operator assignment
  * `>>> moose = ["fat", "orange", "loud"]`
    <br> `>>> size, color, disposition = moose`
    <br> `>>> size`
    <br> `'fat'`
    <br> `>>> color`
    <br> `'orange'`
* Multiple left-side and rigth-side operator assignment
  * `>>> size, color, disposition = "skinny", "red", "quiet"`
##### Swapping variables
  * `>>> a = "AAA"`
    <br> `>>> b = "BBB"`
    <br> `>>> a, b = b, a`
    <br> `>>> a`
    <br> `'BBB'`
    <br> `>>> b`
    <br> `'AAA'`

#### Augmented Operators
* Assignment
  * Use `spam += 1` instead of `spam = spam + 1`
  * Possible with the five operators: +, -, \*, /, %

### 15. List methods
* Same as a function, but it's "called on" values
* List is modified **in place**
* The return value of this methods is `None`
* Lists methods are **exclusive for lists**
* When **duplicated values** exist, methods will act on the **first one that appears** (lower index)

#### `index()` method
* Returns the index of a item in a list
  * `>>> spam = ["moose", "dog", "penguin", "parrot"]`
  <br> `>>> spam.index("penguin")`
  <br> `2`

#### `append()` method
* Adds a value to the end of a list
	* `>>> spam = ["moose", "dog", "penguin", "parrot"]`
	<br> `>>> spam.append("cheeto")`
	<br> `>>> spam`
	<br> `['moose', 'dog', 'penguin', 'parrot', 'cheeto']`

#### `insert()` method
* Adds a value anywhere inside a list, by specifying a index
	* `>>> spam = ["moose", "dog", "penguin", "parrot"]`
	<br>`>>> spam.insert(2, "cheeto")`
	<br>`>>> spam`
	<br>`['moose', 'dog', 'cheeto', 'penguin', 'parrot']`

#### `remove()` method
* Removes an item from a list by specifying a value
	* `>>> spam = ["moose", "dog", "penguin", "parrot"]`
	<br> `>>> spam.remove("parrot")`
	<br> `>>> spam`
	<br> `['moose', 'dog', 'penguin']`
* Different from the [`del` statement](#del-statement), where we specify a index

#### `sort()` method
* Sorts the items in a list
	* `>>> spam = [2, 5, 3.14, 1, -7]`
  <br> `>>> spam.sort()`
  <br> `>>> spam`
  <br> `[-7, 1, 2, 3.14, 5]`
* `reverse=True` keyword argument sort in reverse order
  * `>>> spam.sort(reverse=True)`
  <br> `>>> spam`
  <br> `[5, 3.14, 2, 1, -7]`

* Cannot sort lists that have both int and strings
  * `>>> spam = [1, 2, 3, "Alice", "Mary"]`
  <br> `>>> spam.sort()`
  <br> `Traceback (most recent call last):`
  <br> `  File "<stdin>", line 1, in <module>`
  <br> `TypeError: '<' not supported between instances of 'str' and 'int'`

##### "ASCII-betical" Order
* Sort is made using "ASCII-betical" order -- not real alphabetical order
  * Meaning uppercase characters will appear before than lowercase
    * `>>> spam = ["Z", "a", "z", "A"]`
    <br> `>>> spam.sort()`
    <br> `>>> spam`
    <br> `['A', 'Z', 'a', 'z']`
* Use `key=str.lower` keyword argument to sort in alphabetical order
  * `>>> spam.sort(key=str.lower)`
  <br> `>>> spam`
  <br> `['A', 'a', 'Z', 'z']`

### 16. Similarities Between Lists and Strings
* Foor loops, indexes, slices, negative indexes, and in and not in operators are similar between the two

#### Mutable and Immutable Data Types
* Main difference between lists and strings. [References](#references) represent a import role
* **List** value is a **mutable data type**
  * Can have values added, removed or changed (in place)
* **String** value is **immutable** -- **cannot be changed**
  * `>>> name = "Zophie the cat"`
  <br> `>>> name[7] = "X"`
  <br> `Traceback (most recent call last):`
  <br> `  File "<stdin>", line 1, in <module>`
  <br> `TypeError: 'str' object does not support item assignment`

#### Creating New Strings from Slices
* Proper way to modify a string
  * `>>> name = "Zophie a cat"`
  <br> `>>> newName = name[:7] + "the" + name[8:]`
  <br> `>>> newName`
  <br> `'Zophie the cat'`

#### References
* **Mutable values** - e.g. lists - store **references to the mutable values** - e.g. list item values
  * `>>> spam = [0, 1, 2, 3, 4, 5]` &nbsp; &nbsp; `# assign reference to the list`
  <br> `>>> cheese = spam`          &nbsp; &nbsp; `# assign previous reference to the new variable`
  <br> `>>> cheese[1] = "dud"`
  <br> `>>> cheese`
  <br> `[0, 'dud', 2, 3, 4, 5]`
  <br> `>>> spam`
  <br> `[0, 'dud', 2, 3, 4, 5]`
* **Immutable values** - e.g. strings, tuples - **don't act like this**
  * This is because, they:
    * Can't be modified "in place"
    * Assign a new value is required to change the old one
  * `>>> spam = 100`
  <br> `>>> cheese = spam`
  <br> `>>> spam = 32`
  <br> `>>> spam`
  <br> `32`
  <br> `>>> cheese`
  <br> `100`

#### Passing Lists in Function Calls
* When **passing a list argument**, a **list reference** is what is actually **being passed**
* Changes made to a list in a function, **will affect the list outside the function**
>**Program ::** see [sample_references_in_funcalls.py](./sample_references_in_funcalls.py)

#### The `copy.deepcopy()` Function
* Creates a brand new copy, in a separated list, from another list
  * `>>> import copy`
  <br> `>>> spam = ["moouse", "dog", "penguin", "parrot"]`
  <br> `>>> cheese = copy.deepcopy(spam)`
  <br> `>>> cheese`
  <br> `['moouse', 'dog', 'penguin', 'parrot']`
  <br> `>>> cheese[1] = 9999`
  <br> `>>> cheese`
  <br> `['moouse', 9999, 'penguin', 'parrot']`
  <br> `>>> spam`
  <br> `['moouse', 'dog', 'penguin', 'parrot']`

#### Line Continuation
* The \ (slash) character can be used to express line continuation
* A line is then split across multiple lines
  * `>>> print("Today, tomorrow" + \`
  <br> `...    ", one day")`
  <br> `Today, tomorrow, one day`

#
# example of how mutable data types, and references works
#
# cheese refers to same the list of the global spam variable
def eggs(cheese):
    # although the cheese variable will be destroyed when the function returns,
    # since it's using a reference to a value in memory, the changes will still
    # persist after
    cheese.append("Hello")

# spam stores a reference to the list
spam = [1, 2, 3]
# pass a copy of the reference, and assign to function parameter
eggs(spam)
# print reference value
print(spam)

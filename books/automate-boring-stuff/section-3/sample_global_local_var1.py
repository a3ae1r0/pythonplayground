# Local scopes cannot variables that are in other's functions local scopes
def spam():
    egg = 99
    bacon()
    print(eggs)

def bacon():
    ham  = 101
    eggs = 0

spam()
print(eggs)

#--- OUTPUT ---#
# $ python sample_global_local_var1.py 
# Traceback (most recent call last):
#   File "/opt/data/projects/programming/python/pythonplayground/courses/automate-boring-stuff/section-3/sample_global_local_var1.py", line 11, in <module>
#     spam()
#   File "/opt/data/projects/programming/python/pythonplayground/courses/automate-boring-stuff/section-3/sample_global_local_var1.py", line 5, in spam
#     print(eggs)
# NameError: name 'eggs' is not defined

# Global variables can be read from a local scope
def spam():
    print(eggs)

eggs = 42
spam()

#--- OUTPUT ---#
# $ python sample_global_local_var2.py 
# 42

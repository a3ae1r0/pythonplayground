# Global variables can be read from a local scope: same variable name
def spam():
    eggs = 'Hello'
    print(eggs)

eggs = 42
spam()
print(eggs)

#--- OUTPUT ---#
# python sample_global_local_var3.py
# Hello
# 42

# Local variables cannot be use in the global scope
def spam():
    egg = 99

spam()
print(eggs)

#--- OUTPUT ---#
# $ python sample_global_local_var.py
# Traceback (most recent call last):
#   File "/opt/data/projects/programming/python/pythonplayground/courses/automate-boring-stuff/section-3/sample_global_local_var.py", line 5, in <module>
#     print(eggs)
# NameError: name 'eggs' is not defined

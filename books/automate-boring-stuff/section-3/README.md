# Section 3: Functions

### Table of content
- [Section 3: Functions](#section-3-functions)
  - [Table of content](#table-of-content)
  - [8. Python’s Built-In Functions](#8-pythons-built-in-functions)
    - [Build-in functions](#build-in)
    - [Standard Library functions](#standard-library)
    - [3rd-party functions](#3rd-party)
  - [9. Writing Your Own Functions](#9-writing-your-own-functions)
    - [Custom Functions](#custom-functions)
    - [None Value](#none-value)
    - [Keywork Arguments](#keyword-arguments)
  - [10. Global and Local Scopes](#10-global-and-local-scopes)
    - [Global Scope vs Local Scope](#global-scope-vs-local-scope)

### 8. Python's Built-In Functions
* There are three types of functions
  * [Build-in](#build-in)
  * [Standard Library](#standard-library)
  * [3rd-party](#3rd)

#### Build-in
* Available by default with Python -- `print()`, `input()`, `len()`

#### Standard Library
* Also available by default with Python, but need `import`
* Set of modules
  * Each module has related group of functions
    * e.g. math module --> mathematic functions
* To use a function, the respective **module has be be imported**
  ```python
  >>> import random
  >>> random.randint(1, 10)
  ```
  alternatively,
  ```python
  >>> from random import *
  >>> randint(1, 10)
  ```
  * The **first is recommended** for better code reading
* Multiple module can be imported at once
  `import random, os, sys`
* `sys.exit()` - from `sys` module - **terminates immediately** a program when executed

#### 3rd-party
* Needs instalation using `pip`
  `pip install pyperclip`
* Also need to be imported before using them

### 9. Writing Your Own Functions

#### Custom Functions
* Avoid duplication, by **reusing the same code**
* Functions can have:
  * **Arguments:** value passed in a function call
  * **Parameters:** variable inside the function declaration
* All functions calls has a return value
* **Default return value** for every function is the [None value](#none-value)
* `def` statement defines a function

#### None Value
* Represents the **lack of value** (duh!)
* None value is the **only value of the None value data type**
* If a function **doesn't have** specified a return statement, then the return value **will default to the None value**
* `print()` is a example of a function that return this type of value

#### Keyword arguments
* Optional argument(s) to pass to a function call
* Some functions support this, like the `print()` function
  * `print("str", end="")`  : specify a **end character** for the function to print (default: new line character)
  * `print("str1", "str2", "str3", sep="-")` : specify **delimiter between strings**, when using multiple

### 10. Global and Local Scope
* **Scope:** covers a area of code. Useful to be container of variables
* Two types of scope: [global scope](#global-scope) and [local scope](#local-scope)
* If there's **an assignment for a variable** in a function, that is a **local variable**
  * Assignement statement    == local variable
  * No assignement statement == global variable
* To assign a new value for a global variable from a inside a function, use te `global` statement
  * `global spam = 'Hello'`

#### Global Scope vs Local Scope
|                     |             Global Scope                   |                              Local Scope                                                    |
|         -           |                 -                          |                                 -                                                           |
| **Code scope**      | code outside of all functions              | code inside function block. Each function = own local scope                                 |
| **Variables scope** | variables inside here are global variables | variables inside this scope are called local variables                                      |
| **Rules**           | code here cannot use any local variable    | code inside one function scope, cannot use variables inside in another function local scope |
| **Scope begins...** | when program starts                        | when function is called                                                                     |
| **...and ends...**  | when the program terminates (return)       | when the function return (ends)                                                             |
| **Advantage**       | variables across all program               | code are limited in a smaller space -- good for debugging and readability                   |

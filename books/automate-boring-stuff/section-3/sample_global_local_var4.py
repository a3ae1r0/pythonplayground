# Global variables can be read from a local scope: assign new global value 
# inside a local scope
def spam():
    global eggs
    eggs = 'Hello'
    print(eggs)

eggs = 42
spam()

#--- OUTPUT ---#
# $ python sample_global_local_var4.py
# Hello

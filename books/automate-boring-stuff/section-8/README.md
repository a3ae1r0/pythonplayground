# Section 8: More About Strings

### Table of content
- [Section 8: More About Strings](#section-8-more-about-strings)
  - [Table of content](#table-of-content)
  - [19. Advanced String Syntax](#19-advanced-string-syntax)
    - [Escape Characters](#escape-characters)
    - [Raw Strings](#raw-strings)
    - [Multineline Strings with Triple Quotes](#multineline-strings-with-triple-quotes)
    - [Similarities Between Strings and Lists](#similarities-between-strings-and-lists)
  - [20. String Methods](#string-methods)
    - [Modify characters Case](#modify-characters-case)
      - [`upper()`](#isupper)
      - [`lower()`](#islower)
      - [`title()`](#istitle)
    - [Check String Conditions (`is`)](#check-string-conditions)
      - [`isupper()`](#isupper)
      - [`islower()`](#islower)
      - [`isalpha()`](#isalpha)
      - [`isalnum()`](#isalnum)
      - [`isdecimal()`](#isdecimal)
      - [`isspace()`](#isspace)
      - [`istitle()`](#istitle)
    - [String Start and End](string-start-and-end)
      - [startswith()](#startswith)
      - [endswith()](#endswith)
    - [Padded/Justified Text](#padding-justified-text)
      - [`rjust()`](#rjust)
      - [`ljust()`](#ljust)
      - [`center()`](#center)
    - [Strip Text](#strip-text)
      - [`rstrip()`](#rstrip)
      - [`lstrip()`](#lstrip)
      - [`strip()`](#strip)
    - [`replace()`](#replace)
    - [The pyper clip module](#the-pyper-clip-module)
      - [`pyperclip.copy()`](#pyperclip.copy)
      - [`pyperclip.paste()`](#pyperclip.paste)
  - [21. String Formatting](#string-formatting)


### 19. Advanced String Syntax

#### Escape Characters
* Use "\\" **before the character** you want to escape
  ```python
  print("This \\ is a backslash")
  This \ is a backslash

  print("Hello there!\nSup dude?\nJust chilling")
  Hello there!
  Sup dude?
  Just chilling
  ```
#### Raw Strings
* Helpful when working with **strings that contain multiple backslashes**
* Will literally print any backslash 
  ```python
  r"Use \n for a newline"
  # Output is the regular string that Python automatically interprets
  'Use \\n for a newline'

  # Using raw string in a print() call
  print(r"Use \n for a newline")
  Use \n for a newline

  # w/o a raw string
  print("Use\n for a newline")
  Use
   for a newline
  ```
>**See ::** more use for raw strings will be discussed in the lesson about [Regular Expressions](#../section-10/section-10-regular-expressions)

####  Multiline Strings with Triple Quotes
* String begins and ends with **triple quotes**, either with `'''` or `"""`
  ```python
  print("""Hello Mr. Penguin
  how are you doing?
  i hope you are sleeping well.
  sincerely,
  me""")
  Hello Mr. Penguin
  how are you doing?
  i hope you are sleeping well.
  sincerely,
  me
  ```
* Python understand the newlines entered in the string
  ```python
  spam = """Hello Mr. Penguin
  how are you doing?
  i hope you're sleeping well.
  sincerely,
  me"""

  spam
  "Hello Mr. Penguin\nhow are you doing?\ni hope you're sleeping well.\nsincerely,\nme"
  ```
>**Program ::** See [`sample_multiline.py`](./sample_multiline.py) for a sample program using multiline strings

#### Similarities Between Strings and Lists
* **Indexes**, **slices**, **`in` and `not in` operators**, works the same way as strings
  ```python
  spam = "Yes, this is a string"
  spam[0]
  spam[1:5]
  spam[-1]
  "string" in spam              # True
  "String" in spam              # False
  ```

---
### 20. String Methods
* String methods return a new string value, rather than modify the in place - like list methods
  * Strings are immutable so it's possible to do it

#### Modify characters Case
* Non-letter characters - like spaces and special chars - are not affected by this methods

##### `upper()`
* Returns uppercase version of the string
```python
spam = "Hello Penguin!"
spam.upper()
'HELLO PENGUIN!'
```

##### `lower()`
* Returns lowercase version of the string
```python
spam.lower()
'hello penguin!'
```

##### `title()`
* Converts string to title case
```python
"Is hot outside.".title()
'Is Hot Outside.'
```
#### Check String Conditions (`is`)
* Return `True` or `False` regarding the presence of the condition checked

##### `isupper()`
* Check if a string is uppercase
```python
spam = "Hello penguin"
spam.isupper()
False
```

##### `islower()`
* Check if a string is lowercase
```python
spam = "hello penguin"
spam.isupper()
True
```

##### `isalpha()`
* Check if a string consists only of letters
```python
spam = "hello penguin"
spam.isalpha()
False

spam = "hellopenguin"
spam.isalpa()
True
```

##### `isalnum()`
* Check if a string consists only of alphanumeric characters - letters and numbers
```python
spam = "hellopenguin123"
spam.isalnum()
True
```

##### `isdecimal()`
* Check if a string consists only of decimal values
```python
spam = "hellopenguin123"
spam.isdecimal()
False

spam = "123"
spam.isdecimal()
True
```

##### `isspace()`
* Check if a string only consists of space
```python
"    ".isspace()
True
```

##### `istitle()`
* Check if all words in a string starts with uppercase, and the rest of the word is in lowercase (title case)
```python
"This Is Title Case".istitle()
True

"ThisisnotInTitlecase".istitle()
False
```

#### String Start and End

##### startswith()
* Return `True` if a string value that is called on begins with the string pattern passed to the method
```python
"Testing string".startswith("test")
False

"Testing string".startswith("Test")
True
```

##### endswith()
* Return `False` if a strings with the string in the method argument
```python
"Testing string".endswith("ng")
True
```

##### Get Single or Multiple Strings

###### `join()`
* Joins multiple strings into a single string value
```python
".".join(["uno","dos","tres"])
'uno.dos.tres'
```

###### `slip()`
* Does the opposite of `join()`
* Act on a string value and return a list of strings
* Default splits at blank spaces.
  * Specify another char in functions arguments
```python
"uno, dos, tres".split()
['uno,', 'dos,', 'tres']

"uno, dos, tres".split(",")
['uno', ' dos', ' tres']'
```

#### Padded/Justified Text
* Both methods:
  * Method requires a length argument
    * Argument is the total length of the string
  * A second argument is optional for specify another char for fill the padding

##### `rjust()`
* Returns a padded version of the string -- right justified
```python
"Hello penguin".rjust(20)
'       Hello penguin'

"Hello penguin".rjust(20, "*")
'*******Hello penguin'
```

##### `ljust()`
* Returns a padded version of the string -- left justified
```python
"Hello penguin".ljust(20)
'Hello penguin       '

"Hello penguin".ljust(20, "-")
'Hello penguin-------'
```

##### `center()`
* Returns a string centered
```python
"Hello penguin".center(20, ".")
'...Hello penguin....'
```

#### Strip Text
* Returns a string with a specific character stripped - removed - from the string passed
* Remove characters from right, left, or both sides depending on the function the function used
* Default character stripped is whitespace, but others can be specify
  ```python
  this is a string".strip("t")
  'his is a string'
  ```

##### `rstrip()`
* Returns copy of the string with trailing characters removed (right side)
```python
"   this is a string    ".rstrip()
'   this is a string'
```

##### `lstrip()`
* Returns copy of the string with leading characters removed (left side)
```python
"   this is a string    ".rstrip()
'this is a string    '
```
##### `strip()`
* Returns copy of the string with both leading and trailing characters removed (both sides)
```python
"   this is a string    ".strip()
'this is a string'
```

#### `replace()`
* Replace a string for another
```python
"   this is a string    ".replace("this", "that")
'   that is a string    '
```

#### The pyper clip module
* External module with capabilities of interact with operating system clipboard
* Install module with:
  ```python
  pip install pyperclip
  ```

##### `pyperclip.copy()`
* Copy content to the operating system clipboard
```python
import pyperclip
pyperclip.copy("Copy this!")
```

##### `pyperclip.paste()`
* Paste content from the operating system clipboard
```python
import pyperclip
pyperclip.paste()
```

#### 21. String Formatting
* So far, we used the plus operator to concatenate strings, including for for variable replacement
  ```python
  "Hi " + "there"               # Two string

  name  = "Alice"               # Using variables
  place = "Main Street"
  time  = "5 pm"
  food  = "fast-food"
  "Hello " + name + ", you are invited to a party at " + place + " at " + time + ". Please bring " + food + "!"
  ```
* Using string formatting can be a lot more easier and simpler to read
  ```python
  "Hello %s, you are invited to a party at %s at %s. Please bring %s!" % (name, place, time, food)
         |                                                             |                |
    conversion                                                   separator (req)      variable
    specifier
  ```

>Note: Format specifiers - using the string formatting operator (%) - is possible using different data types (e.g. int, float). See the [official documentation](https://python-reference.readthedocs.io/en/latest/docs/str/formatting.html).

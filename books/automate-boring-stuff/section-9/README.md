# Section 9: Running Programs from the Command Line

### Table of content
- [Section 9: Running Programs from the Command Line](#section-9-running-programs-from-the-command-line)
  - [Table of content](#table-of-content)
  - [22. Launching Python Programs from Outside IDLE](#22-launching-python-programs-from-outside-idle)
    - [Shebang Line](#shebang-line)
    - [Shell Scripting](#shell-scripting)
    - [The PATH Environment Variable](#the-path-environment-variable)
    - [`sys.argv`](#sysargv)

### 22. Launching Python Programs from Outside IDLE

#### Shebang Line
* `#!/usr/bin/python3 # For GNU/LINUX`
  * Location differs for each Operating System
* First line of every Python script
* This instruction tells the OS the location of the Python binary
* Useful to specify a Python version if multiples are installed in the machine
>Program :: See sample_shebang.py for a sample of program using the shebang line

#### Shell Scripting
* File with command line instructions that the OS will running when the script is executed
* Python scripts/programs can be executed through the command line
  ```bash
  # By calling directly Python
  $ python sample_shebang.py          # Or python3 sample_shebang.py

  # By run a executable python program

  # By run a executable python program
  $ chmod +x sample_shebang.py        # Make python program executable (permission)
  $ ./sample_shebang.py
  ```
* We can combine shell scripting and Python program:
  * By creating a bash program that will run our Python program when executed
>Script :: See sample_run.sh for a sample of bash scripting running our previous Python program

#### The PATH Environment Variable
* Every OS have a PATH environment variable that hold the paths for directory that contain executable files (e.g. binary, scripts, etc.)
* We can add our directory that contain our bash scripting files
  ```bash
  export PATH=/path/to/scripts/python/directory/:$PATH
  ```
* That way we can run our custom scripts from every where (directory location)
  ```bash
  # Required: .sh script needs w/ execute permissions + correct path to .py
  $ sample_run.sh
  ```

#### `sys.argv`
* Command line programs can have additional arguments (or options)
  ```bash
  $ ls -ltr       # ls --> command | -ltr --> command options
  ```
* We can leverage this feature and forward (pass) these command line arguments to our Python programs
* `sys.argv` receive a list of these arguments
  * We can then work with these arguments in our Python program and treat them as we wish
>Program :: See sample_run.sh and sample_shebang.py for more information about how to forward and work with arguments, respectively

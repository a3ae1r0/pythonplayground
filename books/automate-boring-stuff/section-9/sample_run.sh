#!/bin/bash

# Run python script (and forward all bash arguments to python program)
python3 courses/automate-boring-stuff/section-9/sample_shebang.py $*
# Wait for user input before terminate script (pause)
read -p "Press any key to continue"

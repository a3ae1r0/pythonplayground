---
Date: 08.2021
Title: Automate the Boring Stuff with Python Programming
Author: Al Sweigart
Description: A practical programming course for office workers, academics, and administrators who want to improve their productivity.
Type: Programming
Language: Python
Last updated: 04.2021
URL: https://www.udemy.com/course/automate/
---

# Automate the Boring Stuff with Python Programming

## This repository
Notes and exercises from the online course. The sub-directories are divided by each chapter lesson.

See [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/) for more information about the book with the same name and others fantastic learning resources.

## Table of content
- [Section 1: Python Basics](section-1#section-1-python-basics)
- [Section 2: Flow Control](section-2#section-2-flow-control)
- [Section 3: Functions](section-3#section-3-functions)
- [Section 4: Handling Errors with try/except](section-4#section-4-handling-errors-with-try-except)
- [Section 5: Writing a Complete Program: Guess the Number](section-5#section-5-writing-a-complete-program-guess-the-number)
- [Section 6: Lists](section-6#section-6-lists)
- [Section 7: Dictionaries](section-7#section-7-dictionaries)
- [Section 8: More About Strings](section-8#section-8-more-about-strings)
- [Section 9: Running Programs from the Command Line](section-9#section-9-running-programs-from-the-command-line)
- [Section 10: Regular Expressions](section-10#section-10-regular-expressions)

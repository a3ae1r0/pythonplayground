def div42by(divideBy):
    try:
        return 42 / divideBy        # try clause
    except ZeroDivisionError:
    # except:                       # catch all errors, no only div by zero
        print("Error: You tried to divide by zero.")        # except clause

print(div42by(2))
print(div42by(12))
print(div42by(0))
print(div42by(1))

# Section 4: Handling Errors with try/except

### Table of content
- [Section 4: Handling Errors with try/except](#section-3-handling-erros-with-try-except)
  - [Table of content](#table-of-content)
  - [11. Try and Except Statements](#8-try-and-except-statements)

### 11. Try and Except Statements
* Errors cause the program to crash
* An error that happens **inside a try block clause** will cause the code in the **except block to be executed**
  * That code can then:
    1. **handle the error**
    2. **display a informative message** to the user
    3. allow the program **execution to continue**

>**Program ::** See [`sample_error.py`](./sample_error.py), [`sample_error_v2.py`](sample_error_v2.py), [`sample_input_Val.py`](.sample_input_val.py)

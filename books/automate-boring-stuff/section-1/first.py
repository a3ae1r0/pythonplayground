# This program says hello and asks for user input for name and age

# greetings
print('Hello!')                     # print is a function to output text

# name
print('What is your name?')         # ask for user name
myName = input()                    # calls input function, assign input to var
print('It is nice to meet you, ' + myName)      # string concatenation as arg
print('The lenght of your name is:')
print(len(myName))                  # call len() to get length of a string

# age
print('What is your age?')          # ask for user age
myAge = input()

# str(), int() (and float()) convert the function argument to the data type
# specify by the function we call, so:
# - convert integer to string
# str(26) --> '26'
# - convert string to integer
# int('48') --> 48
# - convert integer to float
# float (33) --> 33.0

#                      |--- to string --|
#                          |-to int-|
print('You will be ' + str(int(myAge) + 1) + ' in a year.')

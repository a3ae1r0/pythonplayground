# Section 1: Python Basics

### Basic Terminology and Using IDLE

#### IDLE
* Default from python instalations
* Consists of two parts:
  * Interactive shell (`>>>`)
  * File editor

#### Expressions
* All expressions are just **values** and **operators**
* Always evaluates down to a **single value**
* `2 + 3` -> expressions
  * 2 -> value
  * \+ -> operator

#### Data types
* Multiple date types exist:
  * **integers** (int) -> e.g. 4, 93, 3982
  * **float point** (float) -> e.g. 4.4, 3.14, 3982.88
  * **strings** -> john, house

#### Strings
* Always starts and ends with `'`
* **Strings concatenation**
  * `+` to concatenate multiple strings
  * `>>> 'my' + 'house'` <br> `'myhouse'`
* **Strings replication**
  * Needs: strings, `*`, and an integer
  * `>>> 'dud' * 3` <br> `'dudduddud'`
  * `>>> 'dud' + '!' * 3` <br> `'dud!!!'`
  * `>>> spam = 32` <br> `>>> spam = 'ham'`

#### Variables
* Store values
* **Assignment statement**
  * Stores a value to a variable

#### Statements
* Unlike expressions, **don't evaluate to a single value**
  * `>>> spam = spam + 1`
* Although assignment statements **can have an expression as part of the itself**
  * `>>> spam = 2 + 2`
* evaluates (single value) = expression
* doesn't evaluete = statement

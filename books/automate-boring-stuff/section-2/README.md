# Section 2: Flow Control

### Table of content
- [Section 2: Flow Control](#section-2%3A-flow-control)
  - [Table of content](#table-of-content)
  - [4. Flow Charts and Basic Flow Control Concepts](#4-flow-charts-and-basic-flow-control-concepts)
    - [Flow Charts](#flow-charts)
  - [5. If, Else, and ELif Statements](#5-if-else-and-elif-statements)
    - [If](#if)
    - [Elfi](#elfi)
    - [Else](#else)
    - [“Truthy” and “Falsey” values](#truthy-and-falsey-values)
  - [6. While Loops](#6-while-loops)
  - [7. For Loops](#7-for-loops)

### 4. Flow Charts and Basic Flow Control Concepts

#### Flow charts
* Good to detail the **decision making of the program** and the execution of instructions
* Depend of yes/no answers to run decisions
* **Flow control statements** decide which instructions to run under which conditions
* These decisions involves three things: 
  * **boolean values**
  * **comparison operators**
  * **boolean operators**
<br><br>
* **Boolean values**
  * Two values: **True** and **False**
* **Comparison operators**
  * Six operators:
    * **==** equal to
    * **!=** not equal to
    * **<** less than
    * **>** greater than
    * **<=** less than or equal to
    * **>=** greater than or equal to
* **Boolean operators** 
  * **and**, **or**, and **not**
  * **and**
    * **True** if both values are **true**; Otherwise evaluates to **false**
      * `>>> 'True and True` <br> `True`
      * `>>> 'True and False` <br> `False`
  * **or**
    * Evaluate to **True** if either or **both of the values are true**
      * `>>> 'True or False` <br> `True`
      * `>>> 'False or False` <br> `False`
  * **not**
    * Evaluates the **opposite boolean value**
      * `>>> 'not True` <br> `False`
      * `>>> 'not False` <br> `True`

### 5. If, Else, and ELif Statements

#### Blocks
* Line of code that are indented at the same level
* Begins when indentation increase, and ends when indentation returns to it's previous value
* Also know as **clause**
* New blocks begin only **after statements that end with a colon** -- e.g. if statement


#### If
* Used to **condionally run code**, depending on whether or not the if statement's **conditions are True or False**
* `if name == 'Alice'`
  * The above it's call **if statement**
  * `name == 'Alice'` is the **condition/expression**

#### Elif
* As many as you want
* Order of the statements matter
* The execution **enters the first block** that has a **true condition**
  * The rest of the conditions won't even be checked

#### Else
* Comes at the end of the block -- e.g. after if, elfi
* It's own block is executed if **all previous conditions** have been **False**

#### "Truthy" and "Falsey" values
* **0**, **0.0**, and **empty string** are considered to be **falsey values** -- the same as the **false boolean**
* Others **non-blank string** are **truthy values**
* Use `bool()` function to check if a passed value is truthy or falsey

### 6. While Loops
* Run code inside the block, **while condition is true**
* Each code run is called **iteration**
* At the end of each while block run, th execution **jumps back to the start to re-check the condition**
* Two new statements inside the loop:
  * **Break statement**
    * Jump out of the loop, without re-checking the loop condition
  * **Continue statement**
    * The execution jumps back to the start of the loop and reevaluates the condition

### 7. For Loops
* Iterates a specific number of times
* `range()` specify range/number of times the loop will run
  * `range(start, stop, step)`
    * `>>> range(10)` <br> `range(0,10)` -> **range data object**

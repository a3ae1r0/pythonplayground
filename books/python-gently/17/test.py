import dice_roll
assert dice_roll.rollDice(0) == 0
assert dice_roll.rollDice(1000) != dice_roll.rollDice(1000)

for i in range(1000):
    assert 1 <= dice_roll.rollDice(1) <= 6
    assert 2 <= dice_roll.rollDice(2) <= 12
    assert 3 <= dice_roll.rollDice(3) <= 18
    assert 100 <= dice_roll.rollDice(100) <= 600

# A program that rolls a dice n times and return the total sum of the dice
# roll.
import random


def rollDice(numberOfDice):
    total = 0
    for i in range(numberOfDice):
        total += random.randint(1, 6)
    return total

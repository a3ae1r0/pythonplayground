# Exercise #16: Dice Roll

## Exercise Description
Write a `rollDice()` function with a `numberOfDice` parameter that represents the number of six-sided dice. The function returns the sum of all of the dice rolls. For this exercise you must import Python’s `random` module to call its `random.randint()` function for this exercise.


**Full page:** http://inventwithpython.com/pythongently/exercise17/

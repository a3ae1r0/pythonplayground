import handshakes as hs

assert hs.printHandshakes(['Alice', 'Bob']) == 1
assert hs.printHandshakes(['Alice', 'Bob', 'Carol']) == 3
assert hs.printHandshakes(['Alice', 'Bob', 'Carol', 'David']) == 6

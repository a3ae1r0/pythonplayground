# A program that prints the handshakes given between the people from a list of
# names (string list parameter).
# Besides printing, the program also returns the number of handshakes.
def printHandshakes(people):
    """Use list slices to remove current person in list and get next person who
    shook the hand (handshaking pair).
    """
    count = 0

    for i in range(len(people)-1):
        # Create a new list with the remaining list (handshaking pair)
        pair = people[i+1:]
        for j in range(len(pair)):
            print("{} shakes hands with {}".format(people[i], pair[j]))
            count += 1

    return count


printHandshakes(['Alice', 'Bob', 'Carol', 'David'])

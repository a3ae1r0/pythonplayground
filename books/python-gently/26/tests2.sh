#!/bin/bash

# Compare output of the program with example output from exercise description

/usr/bin/python handshakes.py > handshakes.out

if diff handshakes.txt handshakes.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

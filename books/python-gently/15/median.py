# Returns median of a list
def median(numbers):
    length = len(numbers)
    numbers.sort()

    if length == 0:
        return None
    elif length % 2 == 1:
        return numbers[length//2]
    else:
        # Average of the two middle numbers
        return (numbers[length//2] + numbers[(length//2)-1])/2


# Test cases
assert median([]) == None
assert median([1, 2, 3]) == 2
assert median([3, 7, 10, 4, 1, 9, 6, 5, 2, 8]) == 5.5
assert median([3, 7, 10, 4, 1, 9, 6, 2, 8]) == 6
import random
random.seed(42)
testData = [3, 7, 10, 4, 1, 9, 6, 2, 8]
for i in range(1000):
    random.shuffle(testData)
    assert median(testData) == 6

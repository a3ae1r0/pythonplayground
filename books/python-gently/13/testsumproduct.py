# Generate n random numbers and calculate sum and product
import random, sum_product

numbers = []
# Decreased this value because of the following limit:
# ValueError: Exceeds the limit (4300) for integer string conversion; use
#             sys.set_int_max_str_digits() to increase the limit
n = 100
for i in range(n):
    numbers.append(random.randint(1, 1000000000))
print('Numbers:', numbers)
print('Sum is', sum_product.calculateSum(numbers))
print('Product is', sum_product.calculateProduct(numbers))

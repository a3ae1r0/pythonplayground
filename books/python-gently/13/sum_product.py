# Calculate sum and product of a list
def calculateSum(numbers):
    total = 0
    for n in numbers:
        total += n
    return total


def calculateProduct(numbers):
    product = 1
    for n in numbers:
        product *= n
    return product


# Test cases
assert calculateSum([]) == 0
assert calculateSum([2, 4, 6, 8, 10]) == 30
assert calculateProduct([]) == 1
assert calculateProduct([2, 4, 6, 8, 10]) == 3840

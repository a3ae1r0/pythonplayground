# Exercise #13: Sum & Product

## Exercise Description
Write two functions named `calculateSum()` and `calculateProduct()`. They both have a parameter named `numbers`, which will be a list of integer or floating-point values. The `calculateSum()` function adds these numbers and returns the sum while the `calculateProduct()` function multiplies these numbers and returns the product. If the list passed to `calculateSum()` is empty, the function returns `0`. If the list passed to `calculateProduct()` is empty, the function returns `1`. Since this function replicates Python’s `sum()` function, your solution shouldn’t call.


**Full page:** http://inventwithpython.com/pythongently/exercise13/

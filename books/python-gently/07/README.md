# Exercise #7: ASCII Table

## Exercise Description
Write a `printASCIITable()` function that displays the ASCII number and its corresponding text character, from 32 to 126. (These are called the printable ASCII characters.)


**Full page:** http://inventwithpython.com/pythongently/exercise7/

# Print simple ASCII table (printable characters only) with two columns:
# (a) ASCII number; (b) text character.
def printASCIITable():
    for ch in range(32, 126+1):
        print(str(ch) + " " + chr(ch))


printASCIITable()

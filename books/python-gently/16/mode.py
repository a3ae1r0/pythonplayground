# Find the mode - most frequently number - in a list
def mode(numbers):
    """ Find mode by creating a dictionary to keep track of number of
    appearances of each number in the input list.
    After that, check if the list has only one max number or multiple.
    """
    most = {}
    multi_max = []

    if len(numbers) == 0:
        return None

    # Create dictionary where pair is "number: count frequence"
    for n in numbers:
        most.setdefault(n, 0)
        most[n] += 1

    # Get index of the key with biggest value
    # Use max() and lambda function for this
    # From: https://stackoverflow.com/q/268272
    max_val = max(most, key=lambda key: most[key])

    for n in most:
        # Find if any another number (key) as the same max value
        if most[n] == most[max_val]:
            multi_max.append(n)

    # Return one integer or a list of integers
    if len(multi_max) <= 1:
        return max_val
    else:
        return multi_max


mode([1, 1, 3, 4, 4])
# mode([9, 13, 13, 15, 15, 16, 16, 18, 23, 24, 24])

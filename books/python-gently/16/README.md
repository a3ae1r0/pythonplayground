# Exercise #16: Mode

## Exercise Description
Write a `mode()` function that has a `numbers` parameter. This function returns the mode, or most frequently appearing number, of the list of integer and floating-point numbers passed to the function.


**Full page:** http://inventwithpython.com/pythongently/exercise16/

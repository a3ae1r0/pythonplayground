# Solution program for 'Solution Template' section
def mode(numbers):
    if len(numbers) == 0:
        return None

    numberCount = {}
    mostFreqNumber = None
    mostFreqNumberCount = 0

    for number in numbers:
        if number not in numberCount:
            numberCount[number] = 0
        numberCount[number] += 1

        if numberCount[number] > mostFreqNumberCount:
            mostFreqNumber = number
            mostFreqNumberCount = numberCount[number]

    return mostFreqNumber


# Self-note: like I said before in mode1.py, with the hits from 'Solution
# Design' it didn't seem to me that the author had in mind list of numbers with
# multiple modes numbers (e.g. [10, 2, 38, 23, 38, 23, 21]).
#
# So... aditional points for my poor implentation in mode.py

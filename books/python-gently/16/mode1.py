# New version of program after seeing the 'Solution Desing' and 'Special Cases
# and Gotchas' sections (didn't see the solution so far)
#
# Note: again, just like mode.py, this version also have a twist, that allow
# the mode to have multiples numbers.
# Don't know what the solution will be, but so far don't seems that the
# exercise that that into account.
def mode(numbers):
    mode_cnt = 0
    mode_n = 0
    mode_dict = {}

    if len(numbers) == 0:
        return None

    for n in numbers:
        # Append or update number of appearances of current number
        mode_dict.setdefault(n, 0)
        mode_dict[n] += 1

        # Check if current number is the (current) mode number
        if mode_dict[n] > mode_cnt:
            # Update count of mode number appearances
            mode_cnt = mode_dict[n]
            # mode_n = []
            # mode_n.append(n)
            mode_n = n
        elif mode_dict[n] == mode_cnt:
            # Both numbers are mode so convert int to list to hold both
            if type(mode_n) == int:
                _ = mode_n
                mode_n = [_]

            mode_n.append(n)

    return mode_n


mode([1, 1, 3, 4, 4])
# mode([9, 13, 13, 15, 15, 16, 16, 18, 23, 24, 24])

import mode2 as m
import random

assert m.mode([]) == None
assert m.mode([1, 2, 3, 4, 4]) == 4
assert m.mode([1, 1, 2, 3, 4]) == 1
# Additional
assert m.mode([9, 13, 13, 13, 15, 15, 16, 16, 18, 22, 23, 24, 24, 25]) == 13
assert m.mode([9, 13, 13, 15, 15, 16, 16, 18, 23, 24, 24]) == [13, 15, 16, 24]


# I changed this block below.
# I think the book has some typos... C-c & C-v ??
random.seed(42)
testData = [1, 2, 3, 2, 4]
for i in range(1000):
    random.shuffle(testData)
    assert m.mode(testData) == 2

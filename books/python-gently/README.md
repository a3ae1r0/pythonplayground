# Python Programming Exercises, Gently Explained


## About
>Many books and websites have aggressive programming challenges for top coders. <br>
>However, Python Programming Exercises, Gently Explained is for the rest of us. We want challenges that improve our coding skills, not leave us confused and discouraged.

By the same author of *Automate the Boring Stuff with Python*.

Resources:
* [PDF Book](https://inventwithpython.com/PythonProgrammingExercisesGentlyExplained.pdf)
* [Online Book](https://inventwithpython.com/pythongently/)


## Exercises
* [Exercise #1: Hello, World!](./01/)
* [Exercise #2: Temperature Conversion](./02/)
* [Exercise #3: Odd & Even](./03/)
* [Exercise #4: Area & Volume](./04/)
* [Exercise #5: Fizz Buzz](./05/)
* [Exercise #6: Ordinal Suffix](./06/)
* [Exercise #7: ASCII Table](./07/)
* [Exercise #8: Read Write File](./08/)
* [Exercise #9: Chess Square Color](./09/)
* [Exercise #10: Find and Replace](./10/)
* [Exercise #11: Hours, Minutes, Seconds](./11/)
* [Exercise #12: Smallest & Biggest](./12/)
* [Exercise #13: Sum & Product](./13/)
* [Exercise #14: Average](./14/)
* [Exercise #15: Median](./15/)
* [Exercise #16: Mode](./16/)
* [Exercise #17: Dice Roll](./17/)
* [Exercise #18: Buy 8 Get 1 Free](./19/)
* [Exercise #19: Password Generator](./19/)
* [Exercise #20: Leap Year](./20/)
* [Exercise #21: Validate Date](./21/)
* [Exercise #22: Rock, Paper, Scissors](./22/)
* [Exercise #23: 99 Bottles of Beer](./23/)
* [Exercise #24: Every 15 Minutes](./24/)
* [Exercise #25: Multiplication Table](./25/)
* [Exercise #26: Handshakes](./26/)
* [Exercise #27: Rectangle Drawing](./27/)
* [Exercise #28: Border Drawing](./28/)
* [Exercise #29: Pyramid Drawing](./29/)
* [Exercise #30: 3D Box Drawing](./30/)
* [Exercise #31: Convert Integers To Strings](./31/)
* [Exercise #32: Convert Strings To Integers](./32/)
* [Exercise #33: Comma-Formatted Numbers](./33/)
* [Exercise #34: Uppercase Letters](./34/)
* [Exercise #35: Title Case](./35/)
* [Exercise #36: Reverse String](./36/)
* [Exercise #37: Change Maker](./37/)
* [Exercise #38: Random Shuffle](./38/)
* [Exercise #39: Collatz Sequence](./39/)
* [Exercise #40: Merging Two Sorted Lists](./40/)
* [Exercise #41: ROT 13 Encryption](./41/)
* [Exercise #42: Bubble Sort](./42/)

# A program that draws a box using 3D ASCII art
def drawBox(size):
    """Split the box drawing into diferent sections.
    """
    if size < 1:
        return

    padding = size + 1      # For whitespace padding
    width = size * 2        # Required in order to be a box
    lateral_space = 0       # Use to emulate depth
    horizontal_line = "+" + "-" * width + "+"       # Commonly used line

    # Section 1: horizontal line with space padding at the beginning
    print(" " * padding + horizontal_line)

    # Section 2: upper face of the box. With depth
    for _ in range(size):
        padding -= 1
        print(" " * padding + "/" + " " * width + "/" + " " * lateral_space +
              "|")
        lateral_space += 1

    # Section 3: horizontal line with intersection with lateral face
    print(horizontal_line + " " * size + "+")

    # Remove extra line from previous loop
    lateral_space -= 1

    # Section 4: Front face of the box and lateral face
    for _ in range(size):
        print("|" + " " * width + "|" + " " * lateral_space + "/")
        lateral_space -= 1

    # Section 5: Similar to first horizontal line but without padding
    print(horizontal_line)


drawBox(1)
drawBox(2)
drawBox(3)
drawBox(4)
drawBox(5)

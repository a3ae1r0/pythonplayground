#!/bin/bash

# Compare output of the program with a file with the correct answers

/usr/bin/python 3d_box_drawing.py > 3d_box_drawing.out

if diff 3d_box_drawing.txt 3d_box_drawing.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

# Exercise #2: Temperature Conversion

## Exercise Description
Write a `drawBox()` function with a size parameter. The `size` parameter contains an integer for the width, length, and height of the box. The horizontal lines are drawn with `-` dash characters, the vertical lines with `|` pipe characters, and the diagonal lines with `/` forward slash characters. The corners of the box are drawn with `+` plus signs.

There are no Python assert statements to check the correctness of your program. Instead, you can visually inspect the output yourself. For example, calling drawBox(1) through drawBox(5) would output the following boxes, respectively:

```bash
                                                        +----------+
                                                       /          /|
                                      +--------+      /          / |
                                     /        /|     /          /  |
                       +------+     /        / |    /          /   |
                      /      /|    /        /  |   /          /    |
           +----+    /      / |   /        /   |  +----------+     +
          /    /|   /      /  |  +--------+    +  |          |    /
  +--+   /    / |  +------+   +  |        |   /   |          |   /
 /  /|  +----+  +  |      |  /   |        |  /    |          |  /
+--+ +  |    | /   |      | /    |        | /     |          | /
|  |/   |    |/    |      |/     |        |/      |          |/
+--+    +----+     +------+      +--------+       +----------+
Size 1  Size 2      Size 3         Size 4            Size 5
```


**Full page:** http://inventwithpython.com/pythongently/exercise30/

#!/bin/bash
# Download file and remove CRLF line terminators
wget -q https://inventwithpython.com/bottlesofbeerlyrics.txt -O- | tr -d '\r' > bottlesofbeerlyrics.txt
# Remove last line because of junk characters (">>>") from book's sample file
sed -i '$d' bottlesofbeerlyrics.txt

/usr/bin/python 99_bottles_beer.py > 99_bottles_beer.out

diff bottlesofbeerlyrics.txt 99_bottles_beer.out &> /dev/null
if [[ $? == 0 ]]; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

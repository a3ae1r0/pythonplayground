# A program that prints the lyrics of the "99 Bottles of Beer" song
def bottlesBeer():
    """Loop over 98 bottles and print strings. Since the last one stanza is a
    exception to the others, it stay outside the loop.
    """
    for i in range(99, 1, -1):
        print(f'{i} bottles of beer on the wall,')
        print(f'{i} bottles of beer,')
        print('Take one down,')
        print('Pass it around,')

        if i == 2:
            print(f'{i-1} bottle of beer on the wall,\n')
        else:
            print(f'{i-1} bottles of beer on the wall,\n')

    # Last bottle stanza
    print('{0} bottle of beer on the wall,'.format("1"))
    print('{0} bottle of beer,'.format("1"))
    print('Take one down,')
    print('Pass it around,')
    print('No more bottles of beer on the wall!')


bottlesBeer()

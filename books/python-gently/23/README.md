# Exercise #23: 99 Bottles of Beer

## Exercise Description
Write a program that displays the lyrics to "99 Bottles of Beer". Each stanza of the song goes like this:

>X bottles of beer on the wall,
>X bottles of beer,
>Take one down,
>Pass it around,
>X – 1 bottles of beer on the wall,

The X in the song starts at 99 and decreases by one for each stanza. When X is one (and X – 1 is zero), the last line is ―No more bottle.


**Full page:** http://inventwithpython.com/pythongently/exercise23/

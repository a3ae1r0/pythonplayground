# Return string with a number and the corresponding ordinal suffix based on the
# integer argument of the function.
def ordinalSuffix(number):
    number = str(number)
    if number[-2:] in ( "11", "12", "13"):
        return number + "th"
    elif number[-1:] == "1":
        return number + "st"
    elif number[-1:] == "2":
        return number + "nd"
    elif number[-1:] == "3":
        return number + "rd"
    else:
        return number + "th"


# Test cases
assert ordinalSuffix(0) == '0th'
assert ordinalSuffix(1) == '1st'
assert ordinalSuffix(2) == '2nd'
assert ordinalSuffix(3) == '3rd'
assert ordinalSuffix(4) == '4th'
assert ordinalSuffix(10) == '10th'
assert ordinalSuffix(11) == '11th'
assert ordinalSuffix(12) == '12th'
assert ordinalSuffix(13) == '13th'
assert ordinalSuffix(14) == '14th'
assert ordinalSuffix(101) == '101st'

# Additional
assert ordinalSuffix(21) == '21st'
assert ordinalSuffix(32) == '32nd'
assert ordinalSuffix(103) == '103rd'
assert ordinalSuffix(111) == '111th'

# Exercise #2: Temperature Conversion

## Exercise Description
Write a `convertToFahrenheit()` function with a `degreesCelsius` parameter. This function returns the number of this temperature in degrees Fahrenheit. Then write a function named `convertToCelsius()` with a `degreesFahrenheit` parameter and returns a number of this temperature in degrees Celsius.


**Full page:** http://inventwithpython.com/pythongently/exercise2/

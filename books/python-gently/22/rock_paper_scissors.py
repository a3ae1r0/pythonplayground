# A program that emulates the classic rock, paper, scissors game
def rpsWinner(player1, player2):
    """A dictionary set the winning conditions for player1. If players hands
    match one of this conditions, then player1 is the winner.
    """
    outcomes_p1 = {"rock": "scissors",
                   "paper": "rock",
                   "scissors": "paper"}

    if player1 == player2:
        return "tie"
    elif outcomes_p1[player1] == player2:
        return "player one"
    else:
        return "player two"

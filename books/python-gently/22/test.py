import rock_paper_scissors as rps

assert rps.rpsWinner('rock', 'paper') == 'player two'
assert rps.rpsWinner('rock', 'scissors') == 'player one'
assert rps.rpsWinner('paper', 'scissors') == 'player two'
assert rps.rpsWinner('paper', 'rock') == 'player one'
assert rps.rpsWinner('scissors', 'rock') == 'player two'
assert rps.rpsWinner('scissors', 'paper') == 'player one'
assert rps.rpsWinner('rock', 'rock') == 'tie'
assert rps.rpsWinner('paper', 'paper') == 'tie'
assert rps.rpsWinner('scissors', 'scissors') == 'tie'

# Exercise #22: Rock, Paper, Scissors

## Exercise Description
Write a `rpsWinner()` function with parameters `player1` and `player2`. These parameters are passed one of the strings `'rock'`, `'paper'`, or `'scissors'` representing that player’s move. If this results in player 1 winning, the function returns `'player one'`. If this results in player 2 winning, the function returns `'player two'`. Otherwise, the function returns `'tie'`.


**Full page:** http://inventwithpython.com/pythongently/exercise22/

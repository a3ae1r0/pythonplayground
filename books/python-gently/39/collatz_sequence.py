# A program that shows the Collatz conjecture a.k.a "of the most famous
# unsolved problems in mathematics."
def collatz(startingNumber):
    """Return if `startingNumber` is less than 1. Create a new list with
    `startingNumber` and loop until `startingNumber` is 1, in order to get the
    sequence.
    """
    if startingNumber < 1:
        return []

    seq = [startingNumber]
    while startingNumber != 1:
        if startingNumber % 2 == 0:
            startingNumber //= 2
        else:
            startingNumber = 3 * startingNumber + 1

        seq.append(startingNumber)

    return seq

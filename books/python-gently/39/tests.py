import collatz_sequence as collatz
import random

assert collatz.collatz(0) == []
assert collatz.collatz(10) == [10, 5, 16, 8, 4, 2, 1]
assert collatz.collatz(11) == [11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
assert collatz.collatz(12) == [12, 6, 3, 10, 5, 16, 8, 4, 2, 1]
assert len(collatz.collatz(256)) == 9
assert len(collatz.collatz(257)) == 123

random.seed(42)
for i in range(1000):
    startingNum = random.randint(1, 10000)
    seq = collatz.collatz(startingNum)
    assert seq[0] == startingNum # Make sure it includes the starting number.
    assert seq[-1] == 1 # Make sure the last integer is 1.

# Exercise #10: Find and Replace

## Exercise Description
Write a `findAndReplace()` function that has three parameters: `text` is the string with text to be replaced, `oldText` is the text to be replaced, and `newText` is the replacement text. Keep in mind that this function must be case-sensitive: if you are replacing `'dog'` with `'fox'`, then the `'DOG'` in `'MY DOG JONESY'` won’t be replaced.


**Full page:** http://inventwithpython.com/pythongently/exercise10/

# Find text pattern in string and replace it with new value
def findAndReplace(text, oldText, newText):
    """
    Iterate over each character of the text. If the current char is the same of
    the first char in oldText, then check for the consecutive chars in order to
    see if it's a match. If it's append newText to string.
    Otherwise keep appending individual chars to the new text/string.
    """
    newStr = ""
    lenNew = len(newText)
    lenOld = len(oldText)
    i = 0

    # Loop every char in text until the oldText is present
    while oldText in text and i < len(text):
        # Compare first chars. If this character is not the same, then we know
        # for sure that is also not the beginning of oldText
        if text[i] != oldText[0]:
            newStr += text[i]
        else:
            # "One level deep".
            # Check the remain chars in oldText to find if it's a match
            if oldText not in text[i:i+lenOld]:
                newStr += text[i]
            else:
                newStr += newText
                # Also increment remain chars, not only one
                i += lenNew-1
        i += 1
    return newStr


# Test cases
assert findAndReplace('The fox', 'fox', 'dog') == 'The dog'
assert findAndReplace('fox', 'fox', 'dog') == 'dog'
assert findAndReplace('Firefox', 'fox', 'dog') == 'Firedog'
assert findAndReplace('foxfox', 'fox', 'dog') == 'dogdog'
assert findAndReplace('The Fox and fox.', 'fox', 'dog') == 'The Fox and dog.'

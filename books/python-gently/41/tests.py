import rot13
import rot13_v2 as rot13v2
import rot13_v3 as rot13v3
import rot13_v4 as rot13v4


assert rot13.rot13('Hello, world!') == 'Uryyb, jbeyq!'
assert rot13.rot13('Uryyb, jbeyq!') == 'Hello, world!'
assert rot13.rot13(rot13.rot13('Hello, world!')) == 'Hello, world!'
assert rot13.rot13('abcdefghijklmnopqrstuvwxyz') == 'nopqrstuvwxyzabcdefghijklm'
assert rot13.rot13('ABCDEFGHIJKLMNOPQRSTUVWXYZ') == 'NOPQRSTUVWXYZABCDEFGHIJKLM'

# Version 2
assert rot13v2.rot13('Hello, world!') == 'Uryyb, jbeyq!'
assert rot13v2.rot13('Uryyb, jbeyq!') == 'Hello, world!'
assert rot13v2.rot13(rot13v2.rot13('Hello, world!')) == 'Hello, world!'
assert rot13v2.rot13('abcdefghijklmnopqrstuvwxyz') == 'nopqrstuvwxyzabcdefghijklm'
assert rot13v2.rot13('ABCDEFGHIJKLMNOPQRSTUVWXYZ') == 'NOPQRSTUVWXYZABCDEFGHIJKLM'

# Version 3
assert rot13v3.rot13('Hello, world!') == 'Uryyb, jbeyq!'
assert rot13v3.rot13('Uryyb, jbeyq!') == 'Hello, world!'
assert rot13v3.rot13(rot13v3.rot13('Hello, world!')) == 'Hello, world!'
assert rot13v3.rot13('abcdefghijklmnopqrstuvwxyz') == 'nopqrstuvwxyzabcdefghijklm'
assert rot13v3.rot13('ABCDEFGHIJKLMNOPQRSTUVWXYZ') == 'NOPQRSTUVWXYZABCDEFGHIJKLM'

# Version 4
assert rot13v4.rot13('Hello, world!') == 'Uryyb, jbeyq!'
assert rot13v4.rot13('Uryyb, jbeyq!') == 'Hello, world!'
assert rot13v4.rot13(rot13v4.rot13('Hello, world!')) == 'Hello, world!'
assert rot13v4.rot13('abcdefghijklmnopqrstuvwxyz') == 'nopqrstuvwxyzabcdefghijklm'
assert rot13v4.rot13('ABCDEFGHIJKLMNOPQRSTUVWXYZ') == 'NOPQRSTUVWXYZABCDEFGHIJKLM'

# Version 4 | Implementation of Rot 13 cipher
# Slight different version where the wrap around is done with % operator
def rot13(text):
    """Wrap around done with % operator"""
    ciphertext = ""

    for letter in text:
        if letter.isalpha():
            if letter.isupper():
                # Reuse the same variable. Avoids the need for when not alpha
                letter = chr((ord(letter) - 65 + 13) % 26 + 65)
            else:
                letter = chr((ord(letter) - 97 + 13) % 26 + 97)

        ciphertext += letter

    return ciphertext

# Version 2 | Implementation of Rot 13 cipher
def rot13(text):
    """Use the position of the current letter in the dictionary to wrap
    around
    """
    ciphertext = ""

    for letter in text:
        if letter.isalpha():
            # "Left-half" of the dictionary
            if 'a' <= letter <= 'm' or 'A' <= letter <='M':
                ciphertext += chr(ord(letter)+13)
            else:
                ciphertext += chr(ord(letter)-13)
        else:
            ciphertext += letter

    return ciphertext

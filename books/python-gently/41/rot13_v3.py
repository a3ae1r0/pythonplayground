# Version 3 | Implementation of Rot 13 cipher
# Version with hints from 'Solution Design' (still rusty with wrapping around..)
def rot13(text):
    """Wrap letters around if the new value goes pass the range end"""
    ciphertext = ""

    for letter in text:
        if not letter.isalpha():
            ciphertext += letter
            continue

        new_letter = ord(letter) + 13
        # Wrap around if goes past the end of the alphabet
        if letter.islower() and new_letter > 122:
            new_letter -= 26
        elif letter.isupper() and new_letter > 90:
            new_letter -= 26

        ciphertext += chr(new_letter)

    return ciphertext

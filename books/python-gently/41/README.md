# Exercise #41: Rot 13 Encryption

## Exercise Description
Write a `rot13()` function with a `text` parameter that returns the ROT 13 encrypted version of `text`. Uppercase letters encrypt to uppercase letters and lowercase letters encrypt to lowercase letters. For example, `'HELLO, world!'` encrypts to `'URYYB, jbeyq!'` and `'hello, WORLD!'` encrypts to `'uryyb, JBEYQ!'`.

You may use the following Python functions and string methods as part of your solution: `ord()`, `chr()`, `isalpha()`, `islower()`, and `isupper()`.


**Full page:** http://inventwithpython.com/pythongently/exercise41/

# A program that draws a pyramid with n height
def drawPyramid(height):
    """Whitespace padding on the left side, and the remaining with '#' using
    some simple arithmetic calculations.
    """
    for row in range(1, height+1):
        print(" " * (height-row) + "#" * (row + row-1))


drawPyramid(8)
# drawPyramid(3)

#!/bin/bash

# Compare output of the program with a file with the correct answer

/usr/bin/python pyramid_drawing.py > pyramid_drawing.out

if diff pyramid_drawing.txt pyramid_drawing.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

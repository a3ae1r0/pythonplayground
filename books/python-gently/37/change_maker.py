# Calculate and return the correct change for a give amount of change (input)
#
# Version 1: initial thought and, although it works, the loop makes it too slow
#            when working with big 'amount' values

from collections import defaultdict  # Useful when key is not present in dict


def makeChange(amount):
    change = defaultdict(int)

    while amount != 0:
        if amount >= 25:
            amount -= 25
            change['quarters'] += 1
        elif amount >= 10:
            amount -= 10
            change['dimes'] += 1
        elif amount >= 5:
            amount -= 5
            change['nickels'] += 1
        else:
            amount -= 1
            change['pennies'] += 1

    return change

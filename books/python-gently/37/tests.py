import change_maker as change
import change_maker_v2 as change2


assert change.makeChange(30) == {'quarters': 1, 'nickels': 1}
assert change.makeChange(10) == {'dimes': 1}
assert change.makeChange(57) == {'quarters': 2, 'nickels': 1, 'pennies': 2}
assert change.makeChange(100) == {'quarters': 4}
assert change.makeChange(125) == {'quarters': 5}

# Program version 2
assert change2.makeChange(30) == {'quarters': 1, 'nickels': 1}
assert change2.makeChange(10) == {'dimes': 1}
assert change2.makeChange(57) == {'quarters': 2, 'nickels': 1, 'pennies': 2}
assert change2.makeChange(100) == {'quarters': 4}
assert change2.makeChange(125) == {'quarters': 5}

# Calculate and return the correct change for a give amount of change (input)
#
# Version 2: the integer division makes the calculation much faster than the
#            previous loop (same approach of the Solution section)
from collections import defaultdict  # Useful when key is not present in dict


def makeChange(amount):
    change = defaultdict(int)

    coins = amount // 25
    if coins >= 1:
        change['quarters'] += coins
        amount -= coins * 25

    coins = amount // 10
    if coins >= 1:
        change['dimes'] += coins
        amount -= coins * 10

    coins = amount // 5
    if coins >= 1:
        change['nickels'] += coins
        amount -= coins * 5

    # Remaining amount should use pennies
    if amount > 0:
        change['pennies'] += amount

    return change

#!/bin/bash

# Compare output of the program with a correct table format from book's website
# Source: http://inventwithpython.com/pythongently/exercise25/

/usr/bin/python multiplication_table.py > multiplication_table.out

if diff table.txt multiplication_table.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

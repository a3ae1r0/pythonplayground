# A program that displays a 10x10 multiplication table
def multiTable():
    """Column header using loop. Condition and rjust for padding.
    Two nested loops to handle row header and inner section of the table with
    products.
    """
    # Column header
    print("  |", end="")
    for a in range(1, 11):
        print(f'{a}'.rjust(2), end="")
        if a < 10:
            print(" ", end="")
    print("\n--+------------------------------")

    for a in range(1, 11):
        # Row header
        print(f'{a}|'.rjust(3), end="")

        # Product result section
        for b in range(1, 11):
            print(f'{a*b}'.rjust(2), end="")
            # Handles trailing whitespace
            if b < 10:
                print(" ", end="")
        print()


multiTable()

# An implementation of random.shuffle()
from random import randint


def shuffle(values):
    """Iterate over every index of the mutable object; generate a random index
    and swap both values
    """
    length = len(values)
    for i in range(length):
        rand_index = randint(i, length-1)
        values[i], values[rand_index] = values[rand_index], values[i]

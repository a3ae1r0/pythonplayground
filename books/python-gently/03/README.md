# Exercise #3: Odd & Even

## Exercise Description
Write two functions, `isOdd()` and `isEven()`, with a single numeric parameter named number. The `isOdd()` function returns `True` if `number` is odd and `False` if `number` is even. The `isEven()` function returns the `True` if `number` is even and `False` if `number` is odd. Both functions return `False` for numbers with fractional parts, such as `3.14` or `-4.5`j. Zero is considered an even number.

**Full page:** http://inventwithpython.com/pythongently/exercise3/

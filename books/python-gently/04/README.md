# Exercise #3: Area & Volume

## Exercise Description
You will write four functions for this exercise. The functions `area()` and `perimeter()` have `length` and `width` parameters and the functions `volume()` and `surfaceArea()` have `length`, `width`, and `height` parameters. These functions return the area, perimeter, volume, and surface area, respectively.

**Full page:** http://inventwithpython.com/pythongently/exercise4/

#!/bin/bash

# Compare output of the program with a file with the correct answer

/usr/bin/python border_drawing.py > border_drawing.out

if diff border_drawing.txt border_drawing.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

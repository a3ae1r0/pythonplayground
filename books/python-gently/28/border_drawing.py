# A program that draws the border of quadrilateral
def drawBorder(width, height):
    """The output is split in three sections: top, middle, and bottom.
    Use simple arithmetic to get/remove 'pre-defined' characters.
    """
    if width < 2 or height < 2:
        return

    # Strings creation
    top_bottom = "+" + (width-2) * "-" + "+"
    middle = "|" + (width-2) * " " + "|"

    # Top border
    print(top_bottom)

    # Middle section border
    for row in range(height-2):
        print(middle)

    # Bottom border
    print(top_bottom)


drawBorder(16, 4)
# drawBorder(1, 4)

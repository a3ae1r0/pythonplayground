# Exercise #28: Border Drawing

## Exercise Description
Write a `drawBorder()` function with parameters `width` and `height`. The function draws the border of a rectangle with the given integer sizes. There are no Python `assert` statements to check the correctness of your program. Instead, you can visually inspect the output yourself. For example, calling `drawBorder(16, 4)` would output the following:
```bash
+--------------+
|              |
|              |
+--------------+
```

The interior of the rectangle requires printing spaces. The sizes given include the space required for the corners. If the `width` or `height` parameter is less than `2`, the function should print nothing.



**Full page:** http://inventwithpython.com/pythongently/exercise28/

# Exercise #40: Merging Two Sorted Lists

## Exercise Description
Write a `mergeTwoLists()` function with two parameters `list1` and `list2`. The lists of numbers passed for these parameters are already in sorted order from smallest to largest number. The function returns a single sorted list of all numbers from these two lists.

You could write this function in one line of code by using Python’s `sorted()` function: `return sorted(list1 + list2)`
But this would defeat the purpose of the exercise, so don’t use the `sorted()` function or `sort()` method as part of your solution.


**Full page:** http://inventwithpython.com/pythongently/exercise40/

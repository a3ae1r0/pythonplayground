import merge_lists as merge


assert merge.mergeTwoLists([1, 3, 6], [5, 7, 8, 9]) == [1, 3, 5, 6, 7, 8, 9]
assert merge.mergeTwoLists([1, 2, 3], [4, 5]) == [1, 2, 3, 4, 5]
assert merge.mergeTwoLists([4, 5], [1, 2, 3]) == [1, 2, 3, 4, 5]
assert merge.mergeTwoLists([2, 2, 2], [2, 2, 2]) == [2, 2, 2, 2, 2, 2]
assert merge.mergeTwoLists([1, 2, 3], []) == [1, 2, 3]
assert merge.mergeTwoLists([], [1, 2, 3]) == [1, 2, 3]

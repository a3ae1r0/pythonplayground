# A program that merge to sorted lists
def mergeTwoLists(list1, list2):
    """Function compare head values (index 0) from both lists and append to a
    new one. Repeat for every value.
    Add any remaining values before return.
    """
    # If one list is empty, return the other (already sorted)
    if len(list1) == 0:
        return list2
    elif len(list2) == 0:
        return list1

    merged_lst = []
    # Since we are using slices below, we need to call len() each iteration
    while len(list1) > 0 and len(list2) > 0:
        # Both list heads (index 0)
        head1 = list1[0]
        head2 = list2[0]

        if head1 < head2:
            # Add smaller value to merged list
            merged_lst.append(head1)
            # Remove smaller value of that list (head)
            list1 = list1[1:]
        elif head1 > head2:
            merged_lst.append(head2)
            list2 = list2[1:]
        else:
            # Both values are equal. Add both to merged list and remove from
            # original lists
            merged_lst.append(head1)
            merged_lst.append(head2)
            list1 = list1[1:]
            list2 = list2[1:]

    # Merged list plus any leftover values from original lists at the end
    return merged_lst + list1 + list2

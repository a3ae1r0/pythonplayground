import validate_date as val
import datetime


assert val.isValidDate(1999, 12, 31) == True
assert val.isValidDate(2000, 2, 29) == True
assert val.isValidDate(2001, 2, 29) == False
assert val.isValidDate(2029, 13, 1) == False
assert val.isValidDate(1000000, 1, 1) == True
assert val.isValidDate(2015, 4, 31) == False
assert val.isValidDate(1970, 5, 99) == False
assert val.isValidDate(1981, 0, 3) == False
assert val.isValidDate(1666, 4, 0) == False

d = datetime.date(1970, 1, 1)
oneDay = datetime.timedelta(days=1)
for i in range(1000000):
    assert val.isValidDate(d.year, d.month, d.day) == True
    d += oneDay

# Additional
assert val.isValidDate(0, 4, 4) == False
assert val.isValidDate(2070, 11, 31) == False

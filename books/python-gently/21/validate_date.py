# A program that checks if a date is valid
import sys
# Leap year program is in another directory
sys.path.insert(0, '../20')
import leap_year as lp      # noqa: E402


def isValidDate(year, month, day):
    months30 = [9, 4, 6, 11]

    # Valid y, m, d bounds
    if (not year > 0) or (not 0 < month <= 12) or (not 0 < day <= 31):
        return False
    # Months with 30 days
    elif day == 31 and month in months30:
        return False
    # Leap years
    elif month == 2 and day == 29 and not lp.isLeapYear(year):
        return False
    # Default: valid
    else:
        return True

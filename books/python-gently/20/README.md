# Exercise #20: Leap Year

## Exercise Description
Write a `isLeapYear()` function with an integer `year` parameter. If `year` is a leap year, the function returns `True`. Otherwise, the function returns `False`.


**Full page:** http://inventwithpython.com/pythongently/exercise20/

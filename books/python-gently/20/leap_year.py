# A program that check if a year is or not a leap year
def isLeapYear(year):
    """Check "primary requirement" (every 4 years) in outside condition, check
    exceptions in inside statements.
    """
    if year % 4 == 0:
        if year % 100 != 0 or (year % 100 == 0 and year % 400 == 0):
            return True
        else:
            return False
    else:
        return False

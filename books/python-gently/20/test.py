import leap_year as lp

assert lp.isLeapYear(1999) == False
assert lp.isLeapYear(2000) == True
assert lp.isLeapYear(2001) == False
assert lp.isLeapYear(2004) == True
assert lp.isLeapYear(2100) == False
assert lp.isLeapYear(2400) == True
# Addition
assert lp.isLeapYear(1800) == False
assert lp.isLeapYear(1900) == False
assert lp.isLeapYear(2100) == False
assert lp.isLeapYear(2200) == False
assert lp.isLeapYear(2300) == False
assert lp.isLeapYear(2012) == True
assert lp.isLeapYear(2016) == True
assert lp.isLeapYear(2020) == True
assert lp.isLeapYear(2024) == True
assert lp.isLeapYear(2023) == False
assert lp.isLeapYear(2022) == False

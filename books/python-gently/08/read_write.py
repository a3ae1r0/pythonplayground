# Write text to file and read it after
def writeToFile(filename, text):
    # Context manager: open file and manages the file as long as the context is
    #                  active. Close file after it's done (no more actions).
    with open(filename, "w") as file:
        file.write(text)
        file.close()


def appendToFile(filename, text):
    with open(filename, "a") as file:
        file.write(text)


def readFromFile(filename):
    with open(filename, "r") as file:
        return file.read()


# Text case
writeToFile('greet.txt', 'Hello!\n')
appendToFile('greet.txt', 'Goodbye!\n')
assert readFromFile('greet.txt') == 'Hello!\nGoodbye!\n'

# Version 2 | Implementation of the bubble sort algorithm
# A better (?) approach that avoids the infinite loop from previous version.
# Performance wise looks the same.
def bubbleSort(numbers):
    """Two nested for loops to sort the list"""
    length = len(numbers)

    for _ in range(length - 1):
        swapped = False
        for i in range(length - 1):
            if numbers[i] > numbers[i+1]:
                numbers[i], numbers[i+1] = numbers[i+1], numbers[i]
                swapped = True

        if swapped == False:
            break

    return numbers

# Version 3 | Implementation of the bubble sort algorithm
# Solution from 'Solution Template'. A good approach because the inner loop
# range starts at the beginning outer loop. Much faster for real big lists.
def bubbleSort(numbers):
    """Inner loop starts from the value of previous loop (outter)"""
    for i in range(len(numbers) - 1):
        for j in range(i, len(numbers)):
            if numbers[i] > numbers[j]:
                numbers[i], numbers[j] = numbers[j], numbers[i]
    return numbers

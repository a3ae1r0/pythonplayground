# Exercise #42: Bubble Sort

## Exercise Description
Write a `bubbleSort()` function with a list parameter named `numbers`. The function rearranges the values in this list in-place. The function also returns the now-sorted list. There are many sorting algorithms, but this exercise asks you to implement the bubble sort algorithm.

The objective of this exercise is to write a sorting algorithm, so avoid using Python’s `sort()` method or `sorted()` function as that would defeat the purpose of the exercise.


**Full page:** http://inventwithpython.com/pythongently/exercise42/

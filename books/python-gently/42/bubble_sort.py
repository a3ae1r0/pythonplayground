# Implementation of the bubble sort algorithm
def bubbleSort(numbers):
    """Loop infinitively until no more swaps"""
    swapped = True
    while swapped:
        swapped = False
        for i in range(len(numbers) - 1):
            if numbers[i] > numbers[i+1]:
                numbers[i], numbers[i+1] = numbers[i+1], numbers[i]
                swapped = True
    return numbers

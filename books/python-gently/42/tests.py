import bubble_sort as bs
import bubble_sort_v2 as bsv2
import bubble_sort_v3 as bsv3
import random


assert bs.bubbleSort([2, 0, 4, 1, 3]) == [0, 1, 2, 3, 4]
assert bs.bubbleSort([2, 2, 2, 2]) == [2, 2, 2, 2]
x = [random.randint(1, 10000) for x in range(100)]
assert bs.bubbleSort(x) == sorted(x)

# Version 2
assert bsv2.bubbleSort([2, 0, 4, 1, 3]) == [0, 1, 2, 3, 4]
assert bsv2.bubbleSort([2, 2, 2, 2]) == [2, 2, 2, 2]
x = [random.randint(1, 10000) for x in range(100)]
assert bsv2.bubbleSort(x) == sorted(x)

# Version 2
assert bsv3.bubbleSort([2, 0, 4, 1, 3]) == [0, 1, 2, 3, 4]
assert bsv3.bubbleSort([2, 2, 2, 2]) == [2, 2, 2, 2]
x = [random.randint(1, 10000) for x in range(100)]
assert bsv3.bubbleSort(x) == sorted(x)

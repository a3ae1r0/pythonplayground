#!/bin/bash

# Compare output of the program with a file with the correct answer

/usr/bin/python every_15min.py > every_15min.out

if diff every_15min.txt every_15min.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

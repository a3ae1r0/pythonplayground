# Exercise #24: Every 15 Minutes

## Exercise Description
Write a program that displays the time for every 15 minute interval from 12:00 am to 11:45 pm.

Your solution should produce the following output:
```bash
12:00 am
12:15 am
12:30 am
12:45 am
1:00 am
1:15 am
--cut--
11:30 pm
11:45 pm
12:00 am
```

There are 96 lines in the full output.


**Full page:** http://inventwithpython.com/pythongently/exercise24/

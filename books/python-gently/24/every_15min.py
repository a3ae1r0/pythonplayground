# A program that prints the time every 15min from 12AM to 11:45PM

def displayTime():
    """Use an outside loop with 24 hours. Apply condition in this hour to
    "convert" to 12h format.
    An inside loop iterate over the minutes
    """
    am = True

    for hour in range(24):
        # AM or PM?
        if hour >= 12:
            am = False

        # 12AM "exception"
        if hour == 0:
            hour = 12

        # Convert hour to 12h format. Except 12PM
        if not am and hour != 12:
            hour -= 12

        for minutes in range(0, 60, 15):
            print(f'{hour}:{minutes:02d}', end="")

            if am:
                print(" am")
            else:
                print(" pm")


displayTime()

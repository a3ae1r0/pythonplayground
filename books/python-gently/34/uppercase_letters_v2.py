# A second implementation of upper() now using additional functions
def getUppercase(text):
    """Use Unicode code of a character and a simple calculation to return the
    upper version of the char
    """
    text_upper = ""

    for ch in text:
        # If the char is a lowercase between 'a' and 'z' then use Unicode
        # numbers to get the value of the corresponding uppercase char
        if 'a' <= ch <= 'z':
            ch = chr(ord(ch) - 32)
        text_upper += ch

    return text_upper

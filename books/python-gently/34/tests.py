import uppercase_letters as upper
import uppercase_letters_v2 as upper2       # For version 2.0

assert upper.getUppercase('Hello') == 'HELLO'
assert upper.getUppercase('hello') == 'HELLO'
assert upper.getUppercase('HELLO') == 'HELLO'
assert upper.getUppercase('Hello, world!') == 'HELLO, WORLD!'
assert upper.getUppercase('goodbye 123!') == 'GOODBYE 123!'
assert upper.getUppercase('12345') == '12345'
assert upper.getUppercase('') == ''

assert upper2.getUppercase('Hello') == 'HELLO'
assert upper2.getUppercase('hello') == 'HELLO'
assert upper2.getUppercase('HELLO') == 'HELLO'
assert upper2.getUppercase('Hello, world!') == 'HELLO, WORLD!'
assert upper2.getUppercase('goodbye 123!') == 'GOODBYE 123!'
assert upper2.getUppercase('12345') == '12345'
assert upper2.getUppercase('') == ''

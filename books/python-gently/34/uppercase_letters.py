# An implementation of upper()
def getUppercase(text):
    """Use a dictionary with lower and upper case letters to perform
    case transformation.
    """
    UPPPER_CHARS = {'a': 'A', 'b': 'B', 'c': 'C', 'd': 'D', 'e': 'E',
                    'f': 'F', 'g': 'G', 'h': 'H', 'i': 'I', 'j': 'J',
                    'k': 'K', 'l': 'L', 'm': 'M', 'n': 'N', 'o': 'O',
                    'p': 'P', 'q': 'Q', 'r': 'R', 's': 'S', 't': 'T',
                    'u': 'U', 'v': 'V', 'w': 'W', 'x': 'X', 'y': 'Y',
                    'z': 'Z'}
    text_upper = ""

    for ch in text:
        if ch in UPPPER_CHARS:
            text_upper += UPPPER_CHARS[ch]
        else:
            text_upper += ch

    return text_upper

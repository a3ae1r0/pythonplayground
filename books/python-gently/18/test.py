import buy8_1free as b

assert b.getCostOfCoffee(7, 2.50) == 17.50
assert b.getCostOfCoffee(8, 2.50) == 20
assert b.getCostOfCoffee(9, 2.50) == 20
assert b.getCostOfCoffee(10, 2.50) == 22.50

for i in range(1, 4):
    assert b.getCostOfCoffee(0, i) == 0
    assert b.getCostOfCoffee(8, i) == 8 * i
    assert b.getCostOfCoffee(9, i) == 8 * i
    assert b.getCostOfCoffee(18, i) == 16 * i
    assert b.getCostOfCoffee(19, i) == 17 * i
    assert b.getCostOfCoffee(30, i) == 27 * i

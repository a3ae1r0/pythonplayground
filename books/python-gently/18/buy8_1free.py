# A program that calculates the price of n coffees.
# Although, for each 8 coffees, the next one is free!
def getCostOfCoffee(numberOfCoffees, pricePerCoffee):
    """ Calculates the number of free coffees for this orders.
    Returns the numbers of coffees minus the free ones times the price/coffee
    """
    free = 9        # nth free coffee
    return (numberOfCoffees - (numberOfCoffees//free)) * pricePerCoffee


# This solution don't follow the "prerequisite concepts" but I think this way
# is much simpler

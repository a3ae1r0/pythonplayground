import comma_formatted as cf

assert cf.commaFormat(1) == '1'
assert cf.commaFormat(10) == '10'
assert cf.commaFormat(100) == '100'
assert cf.commaFormat(1000) == '1,000'
assert cf.commaFormat(10000) == '10,000'
assert cf.commaFormat(100000) == '100,000'
assert cf.commaFormat(1000000) == '1,000,000'
assert cf.commaFormat(1234567890) == '1,234,567,890'
assert cf.commaFormat(1000.123456) == '1,000.123456'

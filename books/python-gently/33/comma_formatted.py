# Formats a string of a number to use comma formatting (e.g. 79,033,516)
def commaFormat(number):
    """Iterate 3 by 3 numbers from end to beginning, and append to new string.
    Temporarily remove floating part when working with number, and added it at
    the end.
    """
    number = str(number)
    int_part = ""
    fractional_part = ""

    if len(number) <= 3:
        return number

    # Remove fractional part if floating number
    if "." in number:
        _ = number.index(".")
        fractional_part = number[_:]
        number = number[:_]

    # Iterate every 3 numbers from end to begin
    for i in range(len(number)-1, -1, -3):
        # Last 3 digits of number
        int_part = "," + number[i-2:i+1] + int_part
        # Remove last 3 digits from number
        number = number[:i-2]

        # Exit loop if we only have 3 or less numbers
        if len(number) <= 3:
            int_part = number + int_part
            break

    return int_part + fractional_part

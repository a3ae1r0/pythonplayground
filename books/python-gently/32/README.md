# Exercise #32: Convert Strings to Integer

## Exercise Description
Write a `convertStrToInt()` function with a `stringNum` parameter. This function returns an integer form of the parameter just like the `int()` function. For example, `convertStrToInt('42')` should return the integer `42`. The function doesn’t have to work for floating-point numbers with a decimal point, but it should work for negative number values.

Avoid using `int()` in your code, as that would do the conversion for you and defeat the purpose of this exercise. However, we do use `int()` with assert statements to check that your `convertStrToInt()` function works the same as `int()` for all integers from `-10000` to `9999`:
```python
for i in range(-10000, 10000):
    assert convertStrToInt(str(i)) == i
```


**Full page:** http://inventwithpython.com/pythongently/exercise32/

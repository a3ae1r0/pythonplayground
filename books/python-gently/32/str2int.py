# A program the implements int()
def convertStrToInt(stringNum):
    """Iterate over chars in string and append them to integer. Increase "size"
    of integer if needed using arithmetic.
    """
    STRING_TO_INT = {"1": 1, "2": 2, "3": 3, "4": 4, "5": 5,
                     "6": 6, "7": 7, "8": 8, "9": 9, "0": 0}
    negative = False
    first_ch = 0            # First numeric char of the string

    # Negative numbers: set flag for "mirror" number later, and remove "-"
    if stringNum[first_ch] == "-":
        negative = True
        stringNum = stringNum[1:]

    # Raise errors and debug information for other scenarios
    if len(stringNum) == 0:
        raise ValueError("Invalid! Empty string.")
    elif "." in stringNum:
        raise ValueError("Invalid! String has a floating number.")
    elif not stringNum.isdecimal():
        raise ValueError("Invalid! String has alphabet letters.")

    # Starting digit
    number = STRING_TO_INT[stringNum[0]]

    for ch in stringNum[1:]:
        number = (number * 10) + STRING_TO_INT[ch]

    if negative:
        return number * -1
    else:
        return number

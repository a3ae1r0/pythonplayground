import str2int as s2i

for i in range(-10000, 10000):
    assert s2i.convertStrToInt(str(i)) == i

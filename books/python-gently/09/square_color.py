# Receive a coordinate for a square in a chess board a return the color of that
# square.
# Note: coordinates in this program range from 0-7
def getChessSquareColor(column, row):
    """
    Use odd and even numbers to get square color.
    But first check off boundaries coordinates.
    """
    if (row not in range(1, 9)) or (column not in range(1, 9)):
        return ""
    elif row % 2 == column % 2:
        return "white"
    else:
        return "black"


# Test cases
assert getChessSquareColor(1, 1) == 'white'
assert getChessSquareColor(2, 1) == 'black'
assert getChessSquareColor(1, 2) == 'black'
assert getChessSquareColor(8, 8) == 'white'
assert getChessSquareColor(0, 8) == ''
assert getChessSquareColor(2, 9) == ''

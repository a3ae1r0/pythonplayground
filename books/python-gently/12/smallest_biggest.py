# Returns the smallest and biggest number of a list
def getSmallest(numbers):
    if len(numbers) == 0:
        return None
    else:
        smallest = numbers[0]
        for n in range(1, len(numbers)):
            if numbers[n] < smallest:
                smallest = numbers[n]
    return smallest


def getBiggest(numbers):
    if len(numbers) == 0:
        return None
    else:
        biggest = numbers[0]
        for n in range(1, len(numbers)):
            if numbers[n] > biggest:
                biggest = numbers[n]
    return biggest


# Test cases
assert getSmallest([1, 2, 3]) == 1
assert getSmallest([3, 2, 1]) == 1
assert getSmallest([28, 25, 42, 2, 28]) == 2
assert getSmallest([1]) == 1
assert getSmallest([]) == None

assert getBiggest([1, 2, 3]) == 3
assert getBiggest([3, 2, 1]) == 3
assert getBiggest([28, 25, 42, 2, 28]) == 42
assert getBiggest([1]) == 1
assert getBiggest([]) == None

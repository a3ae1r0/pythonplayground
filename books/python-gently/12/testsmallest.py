# Generate n random random numbers and find the smallest
import random, smallest_biggest

numbers = []
n = 1000000
for i in range(n):
    numbers.append(random.randint(1, 1000000000))
print('Numbers:', numbers)
print('Smallest number is', smallest_biggest.getSmallest(numbers))

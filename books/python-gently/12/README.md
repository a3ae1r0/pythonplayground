# Exercise #12: Smallest * Biggest

## Exercise Description
Write a `getSmallest()` function that has a `numbers` parameter. The `numbers` parameter will be a list of integer and floating-point number values. The function returns the smallest value in the list. If the list is empty, the function should return `None`. Since this function replicates Python’s `min()`  function, your solution shouldn’t use it.


**Full page:** http://inventwithpython.com/pythongently/exercise12/

# Fizz Buzz game implementation
def fizzBuzz(upTo):
    for i in range(1, upTo+1):
        if i % 15 == 0:         # Both divisible by 3 and 5
            print("FizzBuzz", end=" ")
        elif (i % 3 == 0):
            print("Fizz", end=" ")
        elif (i % 5 == 0):
            print("Buzz", end=" ")
        else:
            print(i, end=" ")
    print()


# Test cases
fizzBuzz(5)
fizzBuzz(35)

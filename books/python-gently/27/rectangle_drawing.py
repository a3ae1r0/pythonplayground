# A program that uses '#' character to draw a rectangle
def drawRectangle(width, height):
    """Nested loop that prints a width * height quadrilateral.
    """
    for row in range(height):
        for column in range(width):
            print("#", end="")
        print()


drawRectangle(10, 4)
# drawRectangle(0, -1)

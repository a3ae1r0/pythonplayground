#!/bin/bash

# Compare output of the program with a file with the correct answer

/usr/bin/python rectangle_drawing.py > rectangle_drawing.out

if diff rectangle_drawing.txt rectangle_drawing.out > /dev/null; then
  echo "SUCCESS"
else
  echo "FAILED"
fi

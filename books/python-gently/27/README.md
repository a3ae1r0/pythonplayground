# Exercise #27: Rectangle Drawing

## Exercise Description
Write a `drawRectangle()` function with two integer parameters: `width` and `height`. The function doesn’t return any values but rather prints a rectangle with the given number of hashtags in
the horizontal and vertical directions.

There are no Python `assert` statements to check the correctness of your program. Instead, you can visually inspect the output yourself. For example, calling `drawRectangle(10, 4)` should produce the following output:
```bash
##########
##########
##########
##########
```

If either the width or height parameter is 0 or a negative number, the function should print nothing.


**Full page:** http://inventwithpython.com/pythongently/exercise27/

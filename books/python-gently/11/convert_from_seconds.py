# Convert total seconds to '0d 0h 0m 0s' format
def getHoursMinutesSeconds(totalSeconds):
    """
    Create list with all four values: days, hours, minutes, and seconds.
    Append each one of these value (string) by calculating number of seconds.
    Final string is done by concatenating all four (only >0 values).
    """
    days = 0
    hours = 0
    minutes = 0
    time = []

    days = int(totalSeconds/86400)
    totalSeconds -= days * 86400
    hours = int(totalSeconds/3600)
    totalSeconds -= hours * 3600
    minutes = int(totalSeconds/60)
    totalSeconds -= minutes * 60

    if days > 0:
        time.append(str(days) + "d")
    if hours > 0:
        time.append(str(hours) + "h")
    if minutes > 0:
        time.append(str(minutes) + "m")
    if totalSeconds > 0 or (totalSeconds == minutes == hours == 0):
        time.append(str(totalSeconds) + "s")

    return " ".join(time)


# Test cases
assert getHoursMinutesSeconds(30) == '30s'
assert getHoursMinutesSeconds(60) == '1m'
assert getHoursMinutesSeconds(90) == '1m 30s'
assert getHoursMinutesSeconds(3600) == '1h'
assert getHoursMinutesSeconds(3601) == '1h 1s'
assert getHoursMinutesSeconds(3661) == '1h 1m 1s'
# assert getHoursMinutesSeconds(90042) == '25h 42s'
assert getHoursMinutesSeconds(90042) == '1d 1h 42s'
assert getHoursMinutesSeconds(0) == '0s'

# Exercise #11: Temperature Conversion

## Exercise Description
Write a `getHoursMinutesSeconds()` function that has a `totalSeconds` parameter. The argument for this parameter will be the number of seconds to be translated into the number of hours, minutes, and seconds. If the amount for the hours, minutes, or seconds is zero, don’t show it: the function should return `'10m'` rather than `'0h 10m 0s'`. The only exception is that `getHoursMinutesSeconds(0)` should return `'0s'`.

**Full page:** http://inventwithpython.com/pythongently/exercise11/

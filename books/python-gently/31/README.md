# Exercise #31: Convert Integers to Strings

## Exercise Description
Write a `convertIntToStr()` function with an `integerNum` parameter. This function operates similarly to the `str()` function in that it returns a string form of the parameter. For example, `convertIntToStr(42)` should return the string `'42'`. The function doesn’t have to work for floating-point numbers with a decimal point, but it should work for negative integer values.

Avoid using Python’s `str()` function in your code, as that would do the conversion for you and defeat the purpose of this exercise. However, we use `str()` with assert statements to check that your `convertIntToStr()` function works the same as `str()` for all integers from `-10000`to `9999`:
```python
for i in range(-10000, 10000):
 assert convertIntToStr(i) == str(i)
```


**Full page:** http://inventwithpython.com/pythongently/exercise31/

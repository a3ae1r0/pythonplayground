# An implementation of str()
def convertIntToStr(integerNum):
    """Iterate over each digit in number, convert digit to integer and append
    to string.
    """
    out = ""
    negative = False
    dec2ascii = {1: "1", 2: "2", 3: "3", 4: "4", 5: "5",
                 6: "6", 7: "7", 8: "8", 9: "9", 0: "0"}

    # Zero
    if integerNum == 0:
        return "0"

    # Negative numbers: set flag for later  and "mirror" number to positive.
    if integerNum < 0:
        negative = True
        integerNum *= -1

    # Get string
    while integerNum > 0:
        digit = integerNum % 10
        integerNum //= 10
        # Improved after Solution Template. Previous: 'out += dec2ascii[digit]'
        out = dec2ascii[digit] + out

    if negative:
        return "-" + out
    else:
        return out

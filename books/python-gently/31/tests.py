import int2str as i2s

for i in range(-10000, 10000):
    assert i2s.convertIntToStr(i) == str(i)

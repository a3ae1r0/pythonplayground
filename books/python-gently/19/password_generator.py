# A program that generates a password.
# The returned password must respect minimum requirements.
import random
# Useful module that helps with string constants (e.g. alphanumerics)
import string

SPECIAL_CHARS = "~!@#$%^&*()_+."


def generatePassword(length):
    """First add the minimum chars requirements to the password. After, draw a
    type of character (e.g upper or digit) from a list a choose a char from
    that string type. Repeat until password length is met.
    """
    passwd = []
    type_char = ["upper", "lower", "numeric", "special"]

    # Minimum password length
    if length < 12:
        length = 12

    # Generate minimum password requirements
    passwd.append(random.choice(string.ascii_lowercase))
    passwd.append(random.choice(string.ascii_uppercase))
    passwd.append(random.choice(string.digits))
    passwd.append(random.choice(SPECIAL_CHARS))

    for i in range(4, length):
        # Which type of char draw next
        ch = random.choice(type_char)

        if ch == "upper":
            passwd.append(random.choice(string.ascii_lowercase))
        elif ch == "lower":
            passwd.append(random.choice(string.ascii_uppercase))
        elif ch == "numeric":
            passwd.append(random.choice(string.digits))
        else:
            # Special characters has the "same weight" than any other type, so
            # this may lead to very strange passwords. But, hey, security
            # first!
            passwd.append(random.choice(SPECIAL_CHARS))

    # Required to avoid the predefined characters at the beginning
    random.shuffle(passwd)

    return "".join(passwd)

generatePassword(8)
generatePassword(16)

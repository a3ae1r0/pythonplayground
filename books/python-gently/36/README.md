# Exercise #36: Reverse String

## Exercise Description
Write a `reverseString()` function with a `tex`t parameter. The function should return a string with all of `text`’s characters in reverse order. For example, `reverseString('Hello')` returns `'olleH'`. The function should not alter the casing of any letters. And, if `text` is a blank string, the function returns a blank string.


**Full page:** http://inventwithpython.com/pythongently/exercise36/

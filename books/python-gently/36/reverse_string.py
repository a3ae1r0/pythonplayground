# Reverse a string using lists and join()
def reverseString(text):
    """Use a list from `text` and use slices step to reverse it"""
    return "".join(list(text)[::-1])

import reverse_string as reverse
import random


assert reverse.reverseString('Hello') == 'olleH'
assert reverse.reverseString('') == ''
assert reverse.reverseString('aaazzz') == 'zzzaaa'
assert reverse.reverseString('xxxx') == 'xxxx'

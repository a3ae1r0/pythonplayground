import title_case as title
import random


assert title.getTitleCase('Hello, world!') == 'Hello, World!'
assert title.getTitleCase('HELLO') == 'Hello'
assert title.getTitleCase('hello') == 'Hello'
assert title.getTitleCase('hElLo') == 'Hello'
assert title.getTitleCase('') == ''
assert title.getTitleCase('abc123xyz') == 'Abc123Xyz'
assert title.getTitleCase('cat dog RAT') == 'Cat Dog Rat'
assert title.getTitleCase('cat,dog,RAT') == 'Cat,Dog,Rat'

random.seed(42)
chars = list('abcdefghijklmnopqrstuvwxyz1234567890 ,.')
for i in range(1000):
    random.shuffle(chars)
    assert title.getTitleCase(''.join(chars)) == ''.join(chars).title()

# An implementation of title()
def getTitleCase(text):
    """Start will a copy of the original text but with only lowercase letters.
    Iterate over the string and uppercase the required ones.
    """
    titled_text = list(text.lower())
    to_upper = True  # Ctrl variable to ensure upercase only one alpha char

    for i in range(len(text)):
        if not text[i].isalpha():
            to_upper = True
        elif text[i].isalpha() and to_upper:
            titled_text[i] = titled_text[i].upper()
            to_upper = False

    return "".join(titled_text)

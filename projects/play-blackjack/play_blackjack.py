#******************************************************************************
# This is program to emulate a game of Blackjack.
#
# Each version of the program will increase the features of the game and the
# complexity of the code.
#
# Codename : countB
# Version  : v1.0
# Date     : 08/2021
#******************************************************************************
#--- MODULES ---#
import random
import sys

#--- GLOBAL VARIABLES ---#
deck = {
    # Clubs
    "AC": 0, "2C": 0, "3C": 0, "4C": 0, "5C": 0, "6C": 0, "7C": 0, "8C": 0,
    "9C": 0, "10C": 0, "JC": 0, "QC": 0, "KC": 0,
    # Diamonds
    "AD": 0, "2D": 0, "3D": 0, "4D": 0, "5D": 0, "6D": 0, "7D": 0, "8D": 0,
    "9D": 0, "10D": 0, "JD": 0, "QD": 0, "KD": 0,
    # Hearts
    "AH": 0, "2H": 0, "3H": 0, "4H": 0, "5H": 0, "6H": 0, "7H": 0, "8H": 0,
    "9H": 0, "10H": 0, "JH": 0, "QH": 0, "KH": 0,
    # Spades
    "AS": 0, "2S": 0, "3S": 0, "4S": 0, "5S": 0, "6S": 0, "7S": 0, "8S": 0,
    "9S": 0, "10S": 0, "JS": 0, "QS": 0, "KS": 0,
}

#************************************************************
# deal_initial_cards: Give the initials cards for every
#                     player
#************************************************************
def deal_initial_cards(game_information):
    #--- BETS ---#
    for player in game_information:
        print("Insert your bet, " + player + ": ", end = "")
        game_information[player]["bet"] = int(input())
        game_information[player]["balance"] -= game_information[player]["bet"]

    #--- DEAL CARDS ---#
    for player in game_information:
        game_information[player]["cards"] = []          # Card field list
        for n_card in range(2):
            #... DEBUGGING ...#
            x = choose_random_card()
            print("retrieved -- " + x + "\n")
            #... DEBUGGING ...#
            game_information[player]["cards"].append(x)
            # game_information[player]["cards"].append(choose_random_card())

    # Create dealer player - update() - and add list w/ 1 card and facedown k-v
    game_information.update( {"dealer": {"cards": [choose_random_card()]}} )
    game_information["dealer"]["facedown"] = choose_random_card()

    return game_information

#************************************************************
# get_initial_players: Get input from user in order to get:
#                      (i) number of players; (ii) each
#                      players name; (iii) initial balance
#                      for each player
#************************************************************
def get_initial_players(game_information):
    game_stats = ["name", "cards", "balance", "bet", "decision", "values",
                  "burst"]

    #--- Number of players ---#
    print("How many players for this game: ", end = "")
    while True:                             # Keep looping until input is valid
        try:
            n_players = int(input())
            break                           # Exit loop
        except ValueError:
            print("Not a valid number. Try again: ", end = "")

    #--- Get players information ---#
    for i in range(n_players):
        #--- PLAYERS NAME ---#
        print("Insert player " + str(i+1) + " name: ", end = "")
        player_name = input()
        game_information.setdefault(player_name, {})    # Add player dict

        # Append game "fields" to dictionary
        for stat in game_stats:
            game_information[player_name].setdefault(stat, None)

        #--- PLAYERS INITIAL BALANCE ---#
        print("Insert player " + str(i+1) + " initial balance: ", end = "")
        while True:                         # Input validaton for balance
            try:
                player_initial_balance = int(input())
                break                           # Exit loop
            except ValueError:
                print("Not a valid number. Try again: ", end = "")
        # Add balance value to player section (dictionary)
        game_information[player_name]["balance"] = player_initial_balance
        print()

    return game_information

#************************************************************
# choose_random_card(): Choose random card available from the
#                       the deck. Mark dealt cards as seen,
#                       so they're not dealt again.
#************************************************************
def choose_random_card():
    chosen_card = random.choice(list(deck.keys()))      # Generate card

    if deck[chosen_card] == 1:
        #... DEBUGGING ...#
        print("repeated -- " + chosen_card)
        #... DEBUGGING ...#
        choose_random_card()
    else:
        deck[chosen_card] = 1                           # Set cards as seen
        #... DEBUGGING ...#
        print("chosen -- " + chosen_card)
        #... DEBUGGING ...#
        return chosen_card

#************************************************************
# player_decisions: Ask how the user wants to proceed after
#                   receiving the initial two cards.
#                   The players has five options: hit, stand
#                   double down, split, or surrender.
#                   After, act upon this decision.
#************************************************************
def player_decisions(game_information):
    for player in game_information:
        # Check cards vale before proceed to any decision
        player_cards_values(game_information)           # Call to update values
        if game_information[player]["burst"] == "yes":
            print("The player " + player + " has burst.")

        # Exclude dealer and players that already have a decision made
        if player != "dealer" and game_information[player]["decision"] == None and game_information[player]["burst"] != "yes":
            print("\n" + player + ", how do you want to proceed? ", end = "")
            player_decision = input().lower()

            if player_decision == "hit":
                game_information[player]["cards"].append(choose_random_card())
                player_cards_values(game_information)           # Update cards value
                display_player_info(game_information)
                player_decisions(game_information)
            elif player_decision == "stand":
                game_information[player]["decision"] = player_decision  # End
                continue;
            elif player_decision == "double down":
                game_information[player]["balance"] -= game_information[player]["bet"]
                game_information[player]["bet"] *= 2
                game_information[player]["cards"].append(choose_random_card())
                player_cards_values(game_information)           # Update cards value
                display_player_info(game_information)
                game_information[player]["decision"] = player_decision  # End
            elif player_decision == "split":
                # Split the two cards in two separated hands
                # Maybe call the calc_card_points and let that function check
                # if the two cards from the starting hand have the same value.
                # Then act upon that.
                print("TODO")
            elif player_decision == "surrender":
                game_information[player]["balance"] += int(game_information[player]["bet"] / 2)
                game_information[player]["decision"] = player_decision  # End
                continue;
            else:
                print("Enter a valid decision, please.\n")
                player_decisions(game_information)

    # BELOW REPEAT BELOW UNTIL PLAYER BUST OR SURRENDER

#************************************************************
# calc_cards_values: Calculate the card points for each player
#                    including the dealer. Return/add to the
#                    dictionary the points and if bust or not
#************************************************************
def player_cards_values(game_information):
    # LOOP AND APPEND VALUES TO FIELD
    for player in game_information:
        game_information[player]["values"] = 0      # Set default
        for cards in game_information[player]["cards"]:
            if (cards[0] == 'A') or (cards[0] == 'J') or (cards[0] == 'Q') or (cards[0] == 'K') or  (cards[0] == '10'):
                game_information[player]["values"] += 10
            else:
                game_information[player]["values"] += int(cards[0])
            # Ace card: change value from 10 (default) to one
            if game_information[player]["values"] > 21:
                game_information[player]["values"] -= 9


        # ADDITIONAL APPEND VALUE FOR DEALER FACEDOWN CARD

        # CHECK IF ANY PLAYER BURST.
        # SHOULD THIS CHECK BE IN THESE FUNCTION OR ON THE FUNCTION THAT
        # CALLS?? MAYBE ADD AN OPTION PARAMETER TO THIS ONE?
        if game_information[player]["values"] > 21:
            game_information[player]["burst"] = "yes";
        else:
            game_information[player]["burst"] = "no";

#************************************************************
# deal_cards_dealer: After after player finish their mode,
#                    the dealer deal himself the cards.
#                    The round ends after. Update dictionary.
#************************************************************

#************************************************************
# display_player_info: Print player information - name, bet,
#                      balance, cards.
#                      This function can be directly called
#                      by the others.
#************************************************************
def display_player_info(game_information):
    print("==============================", end="")
    for player in game_information:
        print("\n- " + player)              # Header
        print("   Cards: " + str(game_information[player]["cards"]))

        if player != "dealer":
            print("     Bet: " + str(game_information[player]["bet"]))
            print(" Balance: " + str(game_information[player]["balance"]))
            print("Decision: " + str(game_information[player]["decision"]))
        else:
            print("Facedown: " + str(game_information[player]["facedown"])) # Debugging
        print("   Value: " + str(game_information[player]["values"]))
        print("   Burst: " + str(game_information[player]["burst"]))
    print("==============================")

#************************************************************
# MAIN SECTION
#************************************************************
def main():
    min_bet_allow = 100
    max_bet_allow = 1000
    game_information = {}               # Matrix dict for all game info

    # Initial screeen
    print("\n*** Welcome to the 'You Always Lose' Casino ***\n\n"
          "* House Rules *\n"
          "Minimum Bet: $" + str(min_bet_allow) + "    "
          "Maximum Bet: $" + str(max_bet_allow) + "\n")
    game_information = get_initial_players(game_information)
    display_player_info(game_information)
    game_information = deal_initial_cards(game_information)
    # player_cards_values(game_information)           # DUPLICATED CALL. FIX THIS
    # display_player_info(game_information)
    # player_decisions(game_information)
    # display_player_info(game_information)

    #... DEBUGGING ...#
    import json
    print(json.dumps(game_information, sort_keys=False, indent=4))
    #... DEBUGGING ...#

if __name__ == "__main__":
    main()

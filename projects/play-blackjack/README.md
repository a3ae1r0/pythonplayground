# Play Blackjack

### Research (Blackjack Game)

#### Objective:
Get 21 or closer with going over

#### Basic Rules:
1. House set min and max bets - usually starts @ $10 or $15
2. Player place bet
3. Dealer give 2x cards face-up to every player. Dealer gets 1st face-up, and 2nd facedown
4. Each player choose how he wants to play (games options below)
(After all players decisions, is the dealer turn)
5. Dealer flips over his card
6. Gets cards in order to beat the players in game
7. Act upon game results (see below)
8. Dealers sweep cards (clean) 
9. Players take the bets back
10. Repeat

#### Cards Value:
* 2 to 9 : worth their value
* 10, Jack, King, and Queen : worth 10 points
* Ace : count as 1 or 11 (player choice)

#### Cards Combination:
* Blackjack : A + 10-value card (on the first two cards)
* Pair      : can be split (see below)

#### Payout:
* 3:2 ($750 payout on a $500 bet) (only on Blackjack??)

#### Player Game Options (starting hands):
* Hit  : get another card from the deck
* Stay : stay with card on the hand. Wait for the end
* Double down : increase the initial bet by 100% before new card
* Slit : If cards == pair, slip in two hands. Requires additional bet. Hands are played independently (new cards loss, etc.).

#### Game Results:
* 21 : house pay
* player over (burst) : house get bet
* After dealers get cards :
  * Pays all players above his value
  * Get money from players with below card value
  * Pays all if it goes over 21

### TODO:
* v1.0:
  * Ask player decision, and act upon it
  * Add game rules (house) as a header when program starts
  * Deal more cards (1p + dealer)
  * Keep track of money (win/loss function)
  * Another game

* Some day:
  * card_values : make it static/read-only
  * card_values : "A" (ace) should have two values. Possible?
  * Save game state

# g) For the given input string, display all lines not containing start
# irrespective of case.
# Source: https://github.com/learnbyexample/py_regular_expressions/blob
#                /master/exercises/Exercises.md#re-introduction
import re
para = """good start
Start working on that
project you always wanted
stars are shining brightly
hi there
start and try to
finish the book
bye"""

pat = re.compile(r"start", re.IGNORECASE)        # Solution
for line in para.split('\n'):
    if not pat.search(line):
        print(line)

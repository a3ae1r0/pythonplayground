# j) For the given string, replace 0xA0 with 0x7F and 0xC0 with 0x1F.
# Source: https://github.com/learnbyexample/py_regular_expressions/blob/
#                 master/exercises/Exercises.md#re-introduction
import re

def replace_addresses(ip):
    return re.sub(r"0xA0", "0x7F", re.sub("0xC0", "0x1F", ip))

print(replace_addresses("start address: 0xA0, func1 address: 0xC0"))
print('start address: 0x7F, func1 address: 0x1F')

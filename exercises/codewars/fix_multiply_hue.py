#******************************************************************************
# Here we have a function that help us spam our hearty laughter. But is not
# working! I need you to find out why...
#
# Expected results:
# spam(1)  ==> "hue"
# spam(6)  ==> "huehuehuehuehuehue"
# spam(14) ==> "huehuehuehuehuehuehuehuehuehuehuehuehuehue"
#
# Kata: 52e9aa89b5acdd26d3000127
#******************************************************************************
def spam(n_repetition):
        string = "hue"          # Variable for more portability
        # Error: This return a list of the word multiplied
        # return ['hue' for i in range(number+1)]
        # Fix: Directly multiply the string with a desired number
        return string * n_repetition

#--- SAMPLE TESTS ---#
print(spam(6))

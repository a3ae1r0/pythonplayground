###############################################################################
# The code provided is supposed replace all the dots . in the specified
# String str with dashes -
#
# But it's not working properly.
#
# Task
# Fix the bug so we can all go home early.
#
# Notes
# String str will never be null.
#
# Kata: 596c6eb85b0f515834000049
###############################################################################
#--- SOLUTION START ---#
import re
def replace_dots(str):
    # >A period matches any single character (except newline '\n').
    # So we need to escapte the dot character to replace only that type of char
    # https://www.programiz.com/python-programming/regex
    return re.sub(r"\.", "-", str)
#--- SOLUTION END ---#

#--- SAMPLE TESTS ---#
print(replace_dots("one.two.three"))

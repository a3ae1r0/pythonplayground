###############################################################################
# Return the number (count) of vowels in the given string.
#
# We will consider a, e, i, o, u as vowels for this Kata (but not y).
#
# The input string will only consist of lower case letters and/or spaces.
#
# Kata: 54ff3102c1bad923760001f3
###############################################################################
#--- SOLUTION START ---#
def get_count(input_str):
    num_vowels = 0
    vowels_lst = ["a", "e", "i", "o", "u"]
    for letter in str.lower(input_str):
        if letter in vowels_lst:
            num_vowels = num_vowels + 1
    return num_vowels
#--- SOLUTION END ---#

print(get_count("abracadabra"))     # 5
print(get_count("pear tree"))       # 4
print(get_count("PEAR TREEI"))      # 5

'''
The code provided has a method hello which is supposed to show only those
attributes which have been explicitly set. Furthermore, it is supposed to say
them in the same order they were set.

But it's not working properly.

Notes
There are 3 attributes
    name
    age
    sex ('M' or 'F')

When the same attribute is assigned multiple times the hello method shows it
only once. If this happens the order depends on the first assignment of that
attribute, but the value is from the last assignment.

Examples
    Hello.
    Hello. My name is Bob. I am 27. I am male.
    Hello. I am 27. I am male. My name is Bob.
    Hello. My name is Alice. I am female.
    Hello. My name is Batman.

Task
Fix the code so we can all go home early.

kata: https://www.codewars.com/kata/5b0a80ce84a30f4762000069/
kyu: 6
'''


class Dinglemouse:
    def __init__(self):
        self.person_attr = {}

    def setAge(self, age):
        self.person_attr["age"] = age
        return self

    def setSex(self, sex):
        self.person_attr["sex"] = sex
        return self

    def setName(self, name):
        self.person_attr["name"] = name
        return self

    def hello(self):
        out = "Hello. "

        for attr, val in self.person_attr.items():
            if attr == "age":
                out += "I am " + str(val) + '. '
            elif attr == "sex":
                out += "I am "
                out += "male. " if val == 'M' else "female. "
            else:
                out += "My name is " + val + '. '

        return out.strip()


# Sample Tests
dm = Dinglemouse().setName("Bob").setAge(27).setSex('M')
EXPECTED = "Hello. My name is Bob. I am 27. I am male."
assert dm.hello() == EXPECTED

dm = Dinglemouse().setAge(27).setSex('M').setName("Bob")
EXPECTED = "Hello. I am 27. I am male. My name is Bob."
assert dm.hello() == EXPECTED

dm = Dinglemouse().setName("Alice").setSex('F')
EXPECTED = "Hello. My name is Alice. I am female."
assert dm.hello() == EXPECTED

dm = Dinglemouse().setName("Batman")
EXPECTED = "Hello. My name is Batman."
assert dm.hello() == EXPECTED

#******************************************************************************
# This function should take two string parameters: a person's name (name) and
# a quote of theirs (quote), and return a string attributing the quote to the
# person in the following format:
# '[name] said: "[quote]"'
#
# For example, if name is 'Grae' and 'quote' is 'Practice makes perfect' then
# your function should return the string
# 'Grae said: "Practice makes perfect"'
#
# Unfortunately, something is wrong with the instructions in the function
# body. Your job is to fix it so the function returns correctly formatted
# quotes.
#
# Click the "Train" button to get started, and be careful with your
# quotation marks.
#
# Kata: 5859c82bd41fc6207900007a
#******************************************************************************
def quotable(name, quote):
    # Fix: Missing string concatenaion and start quoatation mark
    # return name + quote + '"'
    # return name + ' said: ' + '"' + quote + '"'     # v1
    return '%s said: "%s"' % (name, quote)            # With string formatting

#--- SAMPLE TESTS ---#
print(quotable('Dan', 'Get back to work, Grae'))

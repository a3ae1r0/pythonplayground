#******************************************************************************
# You probably know the "like" system from Facebook and other pages. People can
# "like" blog posts, pictures or other items. We want to create the text that
# should be displayed next to such an item.
#
# Implement the function which takes an array containing the names of people
# that like an item. It must return the display text as shown in the examples:
#
# []                                -->  "no one likes this"
# ["Peter"]                         -->  "Peter likes this"
# ["Jacob", "Alex"]                 -->  "Jacob and Alex like this"
# ["Max", "John", "Mark"]           -->  "Max, John and Mark like this"
# ["Alex", "Jacob", "Mark", "Max"]  -->  "Alex, Jacob and 2 others like this"
#
# Note: For 4 or more names, the number in "and 2 others" simply increases.
#
# Kata: 5266876b8f4bf2da9b000362
#******************************************************************************
def likes(names):
    like_msg = ""                       # To be able to user only one return
    if len(names) == 0:
        like_msg = "no one likes this"
    elif len(names) == 1:
        like_msg = " and ".join(names) + " likes this"
    elif len(names) == 2:
        like_msg = " and ".join(names) + " like this"
    # Two join() because of different separators. Use slices do get each one
    elif len(names) == 3:
        like_msg = ", ".join(names[:2]) + " and " + "".join(names[2:]) + " like this"
    # Slices to get 1st two names, and calculate the leftover names
    else:
        like_string = ", ".join(names[:2]) + " and " + str(len(names)-2) + " others like this"
    return like_msg

#--- SAMPLE TESTS ---#
print(likes([]))
print(likes(['Peter']))
print(likes(['Jacob', 'Alex']))
print(likes(['Max', 'John', 'Mark']))
print(likes(['Alex', 'Jacob', 'Mark', 'Max']))
print(likes(['Alex', 'Jacob', 'Mark', 'Max', 'Julian', 'Victoria', 'Marg']))

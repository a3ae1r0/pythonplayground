#******************************************************************************
# Write a function that doubles every second integer in a list, starting from
# the left.
# Example:
# double_every_other([1,2,3,4]) # -> [1, 4, 3, 8]
#
# Kata: 5809c661f15835266900010a
#******************************************************************************
def double_every_other(lst):
    for n in range(len(lst)):
        if (n+1)%2 == 0:
            lst[n] *= 2
    return lst

print(double_every_other([1,2,3,4,5]))
print(double_every_other([1,19,6,2,12,-3]))

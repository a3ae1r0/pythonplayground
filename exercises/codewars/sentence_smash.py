###############################################################################
# Sentence Smash
#
# Write a function that takes an array of words and smashes them together into
# a sentence and returns the sentence. 
#
# You can ignore any need to sanitize words or add punctuation, but you should
# add spaces between each word.
# Be careful, there shouldn't be a space at the beginning or the end of the 
# sentence!
#
# ['hello', 'world', 'this', 'is', 'great']  =>  'hello world this is great'
###############################################################################
#--- SOLUTION START ---#
def smash(words):
    sentence = ""
    current_word = 0
    for word in words:
        # set counter for number of words processed
        current_word = current_word + 1

        # add list words to string
        sentence = sentence + word

        # add a space after each word, except if it is the last one
        if ( current_word != len(words)):
            sentence = sentence + " "

    return sentence
#--- SOLUTION ENT ---#

#--- SAMPLE TESTS ---#
print(smash(["hello"]))
print(smash(["aww", "mustard", "come", "man"]))
print(smash(["smashes", "them", "together", "into", "a", "sentence"]))

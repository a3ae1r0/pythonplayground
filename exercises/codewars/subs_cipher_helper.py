'''
Description:
A simple substitution cipher replaces one character from an alphabet with a
character from an alternate alphabet, where each character's position in an
alphabet is mapped to the alternate alphabet for encoding or decoding.

E.g.
map1 = "abcdefghijklmnopqrstuvwxyz";
map2 = "etaoinshrdlucmfwypvbgkjqxz";

cipher = Cipher(map1, map2);
cipher.encode("abc") # => "eta"
cipher.encode("xyz") # => "qxz"
cipher.encode("aeiou") # => "eirfg"

cipher.decode("eta") # => "abc"
cipher.decode("qxz") # => "xyz"
cipher.decode("eirfg") # => "aeiou"

If a character provided is not in the opposing alphabet, simply leave it as be.

kata: https://www.codewars.com/kata/52eb114b2d55f0e69800078d
kyu: 6
'''


class Cipher():
    def __init__(self, map1, map2):
        '''
        Defines to maps to be used in the substitution cipher.

        :param map1: map that act as the source alphabet
        :type map1: str

        :param map2: map that act as the alternate alphabet
        :type map2: str
        '''
        self.map1 = map1
        self.map2 = map2

    def encode(self, s):
        '''
        Encode a string into another using a substitution cipher.

        :param s: source string that will be transposed into another.
        :type s: str

        :return: encoded string
        :rtype: str
        '''
        encoded = ''
        for char in s:
            if char not in self.map2:
                encoded += char
            else:
                encoded += self.map2[self.map1.index(char)]
        return encoded

    def decode(self, s):
        '''
        Decode a string into another using a substitution cipher.

        :param s: source string that will be transposed into another.
        :type s: str

        :return: decoded string
        :rtype: str
        '''
        decoded = ''
        for char in s:
            if char not in self.map1:
                decoded += char
            else:
                decoded += self.map1[self.map2.index(char)]
        return decoded


map1 = "abcdefghijklmnopqrstuvwxyz"
map2 = "etaoinshrdlucmfwypvbgkjqxz"

cipher = Cipher(map1, map2)
assert cipher.encode("abc") == "eta"
assert cipher.encode("xyz") == "qxz"
assert cipher.decode("eirfg") == "aeiou"
assert cipher.decode("erlang") == "aikcfu"

map2 = 'tfozcivbsjhengarudlkpwqxmy'
cipher = Cipher(map1, map2)
assert cipher.encode('abc') == 'tfo'
assert cipher.decode('tfo') == 'abc'
assert cipher.decode('kjpphi') == 'tjuukf'
assert cipher.encode('ajqqtb') == 'tjuukf'

#******************************************************************************
# Function receives start number and the end number of a region.
# Function return the count of all numbers except numbers with a 5 in it.
#
# - The start and the end number are both inclusive
# - The result may contain fives
# - The start number will always be smaller than the end number. 
# Both numbers can be also negative!
#
# Examples:
# 1,9  -> 1,2,3,4,6,7,8,9 -> Result 8
# 4,17 -> 4,6,7,8,9,10,11,12,13,14,16,17 -> Result 12
#
# Kata: 5813d19765d81c592200001a
#******************************************************************************
def dont_give_me_five(start, end):
    length = 0
    for i in range(start, end+1):       # include end number, so end+1
        if "5" not in str(i):           # cast to string, check if "5" is in it
            length += 1
    return length

# Another solution - one liner - from another users:
    return print("5" not in str(i) for i in range(start, end + 1))

#--- SAMPLE TESTS ---#
print(dont_give_me_five(1, 9))
print(dont_give_me_five(4, 17))

# More tests when it seems that failed after submit it
print(dont_give_me_five(50, 57))
print(dont_give_me_five(54, 102))
print(dont_give_me_five(81, 160))
print(dont_give_me_five(58, 96))

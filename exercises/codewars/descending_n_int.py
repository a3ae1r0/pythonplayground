#******************************************************************************
# Your task is to make a function that can take any non-negative integer as an
# argument and return it with its digits in descending order.
# Essentially, rearrange the digits to create the highest possible number.
#
# Examples:
# Input: 42145     Output: 54421
# Input: 145263    Output: 654321
# Input: 123456789 Output: 987654321
#
# Kata: 5467e4d82edf8bbf40000155
#******************************************************************************
def descending_order(num):
    num = list(str(num))                    # Convert int to list
    num.sort(reverse=True)                  # Sort list

    # Convert list to str
    reversed_num = ""
    for digit in num:                       # Loop through list indexes
        reversed_num += digit               # append list value to string
    return int(reversed_num)                # From str to int

#--- SAMPLE TESTS ---#
print(descending_order(1234))
print(descending_order(0))
print(descending_order(15))
print(descending_order(123456789))

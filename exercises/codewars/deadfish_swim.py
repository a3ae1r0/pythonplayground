"""
Write a simple parser that will parse and run Deadfish.

Deadfish has 4 commands, each 1 character long:
- i increments the value (initially 0)
- d decrements the value
- s squares the value
- o outputs the value into the return array

Invalid characters should be ignored.

parse("iiisdoso")  ==>  [8, 64]

kata: https://www.codewars.com/kata/51e0007c1f9378fa810002a9/train/python
kyu: 6
"""


def parse(data):
    out = []
    value = 0

    for command in data:
        if command == 'i':
            value += 1
        elif command == 'd':
            value -= 1
        elif command == 's':
            value = pow(value, 2)
        elif command == "o":
            out.append(value)

    return out


# Sample tests
assert parse("ooo") == [0, 0, 0]
assert parse("ioioio") == [1, 2, 3]
assert parse("idoiido") == [0, 1]
assert parse("isoisoiso") == [1, 4, 25]
assert parse("codewars") == [0]
assert parse("dssfijdssoomookabsizazi") == [1, 1, 1, 1]
assert parse("diddimadizdiisgyisyhss") == []
assert parse("ddisoukoiondistsimsbnipsdi") == [1, 1, 2]

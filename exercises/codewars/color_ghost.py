'''
Create a class Ghost

Ghost objects are instantiated without any arguments.

Ghost objects are given a random color attribute of "white" or "yellow" or
"purple" or "red" when instantiated

ghost = Ghost()
ghost.color  #=> "white" or "yellow" or "purple

kata: https://www.codewars.com/kata/53f1015fa9fe02cbda00111a/
kyu: 8
'''
import random


class Ghost(object):
    def __init__(self):
        self.color = random.choice(['white', 'yellow', 'purple', 'red'])


ghosts = [Ghost().color for _ in range(100)]
assert ghosts.count('white') >= 1
assert ghosts.count('yellow') >= 1
assert ghosts.count('purple') >= 1
assert ghosts.count('red') >= 1

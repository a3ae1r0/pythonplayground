#******************************************************************************
# Create a function taking a positive integer as its parameter and returning a
# string containing the Roman Numeral representation of that integer.
#
# Modern Roman numerals are written by expressing each digit separately
# starting with the left most digit and skipping any digit with a value of
# zero. In Roman numerals 1990 is rendered: 1000=M, 900=CM, 90=XC; resulting in
# MCMXC. 2008 is written as 2000=MM, 8=VIII; or MMVIII. 1666 uses each Roman
# symbol in descending order: MDCLXVI.
#
# Example:
# solution(1000) # should return 'M'
#
# Help:
# Symbol    Value
# I          1
# V          5
# X          10
# L          50
# C          100
# D          500
# M          1,000
#
# Remember that there can't be more than 3 identical symbols in a row.
#
# More about roman numerals - http://en.wikipedia.org/wiki/Roman_numerals
#
# Kata: 51b62bf6a9c58071c600001b
#******************************************************************************
def solution(n):
    roman_values = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D",
                    1000: "M"}

    # GET EACH INDIVIDUAL DIGIT FROM LEFT TO RIGHT
    for digit in len(str(n)):
        print(digit)



#--- TEST SAMPLES ---#
print(solution(1))          # 'I'
print(solution(4))          # 'IV'
print(solution(6))          # 'VI'
print(solution(14))         # 'XIV'
print(solution(21))         # 'XXI'
print(solution(89))         # 'LXXXIX'
print(solution(91))         # 'XCI'
print(solution(984))        # 'CMLXXXIV'
print(solution(1000))       # 'M'
print(solution(1889))       # 'MDCCCLXXXIX'
print(solution(1989))       # 'MCMLXXXIX'

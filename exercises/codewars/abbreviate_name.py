#******************************************************************************
# Write a function to convert a name into initials. 
# This kata strictly takes two words with one space in between them.
# 
# The output should be two capital letters with a dot separating them.
# 
# It should look like this:
# Sam Harris => S.H
# Patrick Feeney => P.F
#
# Kata: 57eadb7ecd143f4c9c0000a3
#******************************************************************************
def abbrev_name(name):
    # Loop to get character of the last name
    for i, ch in enumerate(name):           # Get index and values of string
        if ch == " ":                       # Exit if encounter a space char 
            break

    # Concatenate: first letter of name + dot + first letter second name
    return name[0].upper() + "." + name[i+1].upper()

print(abbrev_name("Evan Cole"))
print(abbrev_name("ucYYkcDs anPASdRDFzYdWCCXSwCr"))


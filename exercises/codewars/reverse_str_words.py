#******************************************************************************
# Complete the function that accepts a string parameter, and reverses each 
# word in the string. All spaces in the string should be retained.
#
# Examples
# "This is an example!" ==> "sihT si na !elpmaxe"
# "double  spaces"      ==> "elbuod  secaps"
#
# Kata: 5259b20d6021e9e14c0010d4
#******************************************************************************
def reverse_words(text):
    reversed_str = ""                       # Empty string to be returned
    # Is this the more simplier way to reverse the words?
    for words in text.split(" "):           # Slit string in only words
        reversed_str += words[::-1] + " "   # Reverse word + space and append
    return reversed_str[:-1]                # Remove last space in the string

#--- SAMPLE TESTS ---#
print(reverse_words('The quick brown fox jumps over the lazy dog.'),
      '  Expected:', 'ehT kciuq nworb xof spmuj revo eht yzal .god')
print(reverse_words('apple'),
      '                                         Expected:', 'elppa')
print(reverse_words('a b c d'),
      '                                       Expected:', 'a b c d')
print(reverse_words('double  spaced  words'),
      '                         Expected:', 'elbuod  decaps  sdrow')

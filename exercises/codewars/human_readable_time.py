"""
Write a function, which takes a non-negative integer (seconds) as input and
returns the time in a human-readable format (HH:MM:SS)

HH = hours, padded to 2 digits, range: 00 - 99
MM = minutes, padded to 2 digits, range: 00 - 59
SS = seconds, padded to 2 digits, range: 00 - 59
The maximum time never exceeds 359999 (99:59:59)

You can find some examples in the test fixtures.
"""


def make_readable(seconds):
    """Use integer division and f-strings to convert seconds to HH:MM:SSS"""
    time = ["", "", ""]

    time[0] = f'{(seconds // 3600):02d}'
    time[1] = f'{(seconds - int(time[0]) * 3600) // 60:02d}'
    time[2] = f'{(seconds - int(time[0]) * 3600 - int(time[1]) * 60):02d}'

    return ":".join(time)


# Sample tests
assert make_readable(0) == "00:00:00"
assert make_readable(5) == "00:00:05"
assert make_readable(60) == "00:01:00"
assert make_readable(86399) == "23:59:59"
assert make_readable(359999) == "99:59:59"

#******************************************************************************
# Complete the solution so that the function will break up camel casing, using
# a space between words.
#
# Example
# "camelCasing"  =>  "camel Casing"
# "identifier"   =>  "identifier"
# ""             =>  ""
#
# Kata: 5208f99aee097e6552000148
#******************************************************************************
def solution(string):
    no_camel = ""
    for letter in string:
        if letter.isupper():
            no_camel += " "
        no_camel += letter
    return no_camel

#--- SAMPLE TESTS ---#
print(solution("helloWorld"))
print(solution("camelCase"))
print(solution("breakCamelCase"))

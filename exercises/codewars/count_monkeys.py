###############################################################################
# Given the number (n), populate an array with all numbers up to and including
# that number, but excluding zero.
# 
# For example:
# monkeyCount(10) # --> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# monkeyCount(1) # --> [1]
#
# Kata: 56f69d9f9400f508fb000ba7
###############################################################################
#--- SOLUTION START ---#
def monkey_count(n):
    n_monkeys = []
    for i in range(1, n+1):
        n_monkeys.append(i)
    return n_monkeys
#--- SOLUTION END ---#

#--- SAMPLE TESTS ---#
print(monkey_count(9))
print(monkey_count(20))

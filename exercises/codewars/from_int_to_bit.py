#******************************************************************************
# Write a function that takes an integer as input, and returns the number of
# bits that are equal to one in the binary representation of that number.
# You can guarantee that input is non-negative.
#
# Example: The binary representation of 1234 is 10011010010, so the function
# should return 5 in this case
#
# Kata: 526571aae218b8ee490006f4
#******************************************************************************
def count_bits(n):
    # Formula:
    # bit #0      int/2 = q + r   --> remain bit 1 or 0
    # ...
    # bit #n      int/2 = 0 + r   --> ends when quotient = 0
    binary   = ""                          # Binary representation of the int
    quotient = 1                           # Non-zero value (0 --> exit state)

    while quotient != 0:
        quotient = int(n / 2)              # Get decimal part only
        if (n/2 - quotient) == 0:          # Check if remainder is 0
            binary += "0"                  # Append bit "0" to binary string
        else:
            binary += "1"
        # Another possible (better) alternative for the if/else (answer section)
        # binary = str(n%2)
        n = quotient                       # Use quotient as new n (continue)

    return int(binary.count("1"))

    # 2nd option: using the already existing bin() method
    # return bin(n).count("1")

#--- SAMPLE TESTS ---#
print(count_bits(0))       # 0
print(count_bits(4))       # 1
print(count_bits(7))       # 3
print(count_bits(9))       # 2
print(count_bits(10))      # 2
print(count_bits(1234))    # 5

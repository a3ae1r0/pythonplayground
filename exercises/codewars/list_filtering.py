###############################################################################
# In this kata you will create a function that takes a list of non-negative
# integers and strings and returns a new list with the strings filtered out.
#
# Example
# filter_list([1,2,'a','b']) == [1,2]
# filter_list([1,'a','b',0,15]) == [1,0,15]
# filter_list([1,2,'aasf','1','123',123]) == [1,2,123]
###############################################################################
#--- SOLUTION START ---#
def filter_list(lst):
    filtered_lst = []
    for item in lst:
        try:
            # int(c) dont work because of strings like "123"
            if ( 0 <= item ):
                filtered_lst.append(item)   # append to the new list created
        except:
            continue                        # do nothing if it's not a pos int
    return filtered_lst
#--- SOLUTION END ---#

#--- SAMPLE TESTS ---#
print("[1,2,'a','b']             -->", filter_list([1,2,'a','b']))
print("[1,'a','b',0,15]          -->", filter_list([1,'a','b',0,15]))
print("[1,2,'asf','1','123',123] -->", filter_list([1,2,'asf','1','123',123]))

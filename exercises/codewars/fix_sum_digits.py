###############################################################################
# Debug function getSumOfDigits that takes positive integer to calculate sum 
# of it's digits. Assume that argument is an integer.
#
# Example:
# 123  => 6
# 223  => 7
# 1337 => 14
#
# Kata: 563d59dd8e47a5ed220000ba
###############################################################################
def get_sum_of_digits(num):
    # Fix: TypeError: unsupported operand type(s) for +: 'NoneType' and 'str'
    #      Assignment value should be a int
    # sum = None
    sum = 0
    # Fix: TypeError: 'int' object is not iterable
    #      Cast to str to allow creation of a list. (list() is optional, could
    #      only be converted to str)
    digits = list(str(num))
    for x in digits:
        # Fix: TypeError: unsupported operand type(s) for +: 'int' and 'str'
        # Fix: TypeError: unsupported operand type(s) for +=: 'int' and 'str'
        #      Wrong operand sign, and cast x value to int (again)
        # sum + x
        sum += int(x)
    return sum

#--- SAMPLE TESTS ---#
print("123  -->", get_sum_of_digits(123))
print("223  -->", get_sum_of_digits(223))
print("1337 -->", get_sum_of_digits(1337))

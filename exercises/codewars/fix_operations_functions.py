#******************************************************************************
# Should be easy, begin by looking at the code. Debug the code and the
# functions should work.
#
# There are three functions: Multiplication (x) Addition (+) and
# Reverse (!esreveR)
#
#  Kata: 5844a422cbd2279a0c000281
#******************************************************************************
def multi(l_st):
    # Fix: TypeError: can't multiply sequence by non-int of type 'list'
    result = 1
    for i in l_st:
        result *= i
    # return l_st * l_st
    return result
def add(l_st):
    # Fix: output returned is the duplication of the list, and not the result
    return sum(l_st)
    # return l_st + l_st
def reverse(string):
    # Fix: AttributeError: 'str' object has no attribute 'reverse'
    # return string.reverse
    return "".join(reversed(string))

#--- SAMPLE TESTS ---#
print(multi([8,2,5]), "\tExpected: 80")
print(add([1,15,3]), "\tExpected: 19")
print(add([7,8,6,5,4,9]), "\tExpected: 39")
print(reverse("Hello World"), "\tExpected:" + "dlroW olleH")

###############################################################################
# Complete the solution so that it reverses the string passed into it.
# 'world'  =>  'dlrow'
#
# kata: 5168bb5dfe9a00b126000018
###############################################################################
#--- SOLUTION START ---#
def solution(string):
    reverse_str = ""
    # the last element of a string is string[-1], followed by string[-2], etc.
    # make len negative and subtract 1 in order to get all positions needed
    for letter in range(-1, (-len(string) - 1), -1):
        reverse_str += string[letter]           # add reversed char to new var
    return reverse_str
#--- SOLUTION END ---#

print(solution("world"))

# By accident (by typing "reversed") I found another possible way to do it
# using the reversed() function
# (not used in this kata)
og_string  = "hello"
rev_string = reversed(og_string)
for letter in rev_string:
    print(letter, end="")
print()

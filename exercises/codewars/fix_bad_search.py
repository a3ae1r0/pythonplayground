#******************************************************************************
# The function must return the sequence of titles that match the string passed
# as an argument.
# 
# titles = ['Rocky 1', 'Rocky 2', 'My Little Poney']
# search(titles, 'ock') --> ['Rocky 1', 'Rocky 2']
# 
# But the function return some weird result and skip some of the matching
# results.
# 
# Does the function have special movie taste?
# Let's figure out !
# 
# Kata: 52cd53948d673a6e66000576
#******************************************************************************
def search(titles, term): 
    # Problem: the case sensitive search between the term and the titles
    # Fix: convert search term and titles list to lower case and save to a new
    #      variable in order to revert the case switching later (return)
    # return list(filter(lambda title: term in title, titles))

    # 1st and muli-line attempt...
    # filtered_titles = list(filter(lambda title: term.lower() in title, 
    #                               [item.lower() for item in titles]))
    # for loop to revert the item case
    # return [item.title() for item in filtered_titles]

    # Same thing as mine, but in one line and more simpler to read
    # from https://www.codewars.com/kata/reviews/5eb92f980f61730001e7040c
    # /groups/5eb94c4081e68600014a3229
    return list(filter(lambda title: term.lower() in title.lower(), titles))

#--- SAMPLE TESTS ---#
titles = ['The Big Bang Theory', 'How I Met Your Mother', 'Dexter',
          'Breaking Bad', 'Doctor Who', 'The Hobbit',
          'Pacific Rim', 'Pulp Fiction', 'The Avengers', 'Shining']
print(search(titles, 'ho'))     # ['How I Met Your Mother', 'Doctor Who',
                                # 'The Hobbit']
print(search(titles, 'exte'))   # ['Dexter']
print(search(titles, 'the'))    # ['The Big Bang Theory',
                                # 'How I Met Your Mother', 'The Hobbit',
                                # 'The Avengers']
print(search(titles, 'The'))    # ['The Big Bang Theory',
                                # 'How I Met Your Mother', 'The Hobbit',
                                # 'The Avengers']

#
# Fix bug(s) :: If you pass the list of your favourite animals to the function,
# you should get the list of the animals with orderings and newlines added.
#
# animals = [ 'dog', 'cat', 'elephant' ]
# list_animals(animals) == '1. dog\n2. cat\n3. elephant\n'
#
# kata: 55ca43fb05c5f2f97f0000fd
#
#--- GLOBAL VARS ---#
animals = [ 'dog', 'cat', 'elephant' ]

#--- SOLUTION START ---#
def list_animals(animals):
    list = ''
    # FIX :: TypeError: 'list' object cannot be interpreted as an integer
    #        range arg needs to be a int, so we need the length of the list
    # for i in range(animals):
    for i in range(len(animals)):
        list += str(i + 1) + '. ' + animals[i] + '\n'
    return list
#--- SOLUTION END ---#

#--- PRINT RETURN VALUE ---#
print(list_animals(animals))

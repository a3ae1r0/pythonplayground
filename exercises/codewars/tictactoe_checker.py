"""
If we were to set up a Tic-Tac-Toe game, we would want to know whether the
board's current state is solved, wouldn't we? Our goal is to create a function
that will check that for us!

Assume that the board comes in the form of a 3x3 array, where the value is 0 if
a spot is empty, 1 if it is an "X", or 2 if it is an "O", like so:

[[0, 0, 1],
 [0, 1, 2],
 [2, 1, 0]]

We want our function to return:
. -1 if the board is not yet finished AND no one has won yet (there are empty
spots),
. 1 if "X" won,
. 2 if "O" won,
. 0 if it's a cat's game (i.e. a draw).

You may assume that the board passed in is valid in the context of a game of
Tic-Tac-Toe.

kata: https://www.codewars.com/kata/525caa5c1bf619d28c000335
kyu: 5
"""


def is_solved(board):
    """Loop over the board and for each possible win direction, check if we
    have a winner.
    """
    for i in range(3):
        # Row win
        row = set(board[i])
        if row == {1}:
            return 1
        elif row == {2}:
            return 2

        # Column win
        column = set([lst[i] for lst in board])
        if column == {1}:
            return 1
        elif column == {2}:
            return 2

        # Diagonal win
        diagonal1 = set([board[0][0], board[1][1], board[2][2]])
        diagonal2 = set([board[2][0], board[1][1], board[0][2]])
        if diagonal1 == {1} or diagonal2 == {1}:
            return 1
        elif diagonal1 == {2} or diagonal2 == {2}:
            return 2

    # Gaming still on...
    if any(0 in lst for lst in board):
        return -1

    return 0


# Sample Tests
board = [[0, 0, 1], [0, 1, 2], [2, 1, 0]]
assert is_solved(board) == -1
board = [[1, 1, 1], [0, 2, 2], [0, 0, 0]]
assert is_solved(board) == 1
board = [[2, 1, 2], [2, 1, 1], [1, 1, 2]]
assert is_solved(board) == 1
board = [[2, 1, 2], [2, 1, 1], [1, 2, 1]]
assert is_solved(board) == 0
board = [[2, 1, 2], [1, 2, 0], [2, 1, 2]]
assert is_solved(board) == 2
board = [[2, 2, 1], [1, 1, 0], [1, 0, 2]]
assert is_solved(board) == 1

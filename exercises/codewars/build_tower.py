"""
Build a pyramid-shaped tower given a positive integer number of floors. A tower
block is represented with "*" character.

For example, a tower with 3 floors looks like this:
[
  "  *  ",
  " *** ",
  "*****"
]

And a tower with 6 floors looks like this:
[
  "     *     ",
  "    ***    ",
  "   *****   ",
  "  *******  ",
  " ********* ",
  "***********"
]

kata: https://www.codewars.com/kata/576757b1df89ecf5bd00073b
kyu: 6
"""


def tower_builder(n_floors):
    """Build a pyramid-shaped tower into a list."""
    tower = []
    padding = n_floors-1
    for floor in range(n_floors):
        block = " " * padding + "*" * (floor*2+1) + " " * padding
        tower.append(block)
        padding -= 1
    return tower


# Fixed Tests
print(tower_builder(1))
print(tower_builder(2))
print(tower_builder(3))

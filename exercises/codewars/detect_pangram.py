#******************************************************************************
# A pangram is a sentence that contains every single letter of the alphabet at
# least once. 
# For example, the sentence "The quick brown fox jumps over the lazy dog" is a
# pangram, because it uses the letters A-Z at least once (case is irrelevant).
# 
# Given a string, detect whether or not it is a pangram.
# Return True if it is, False if not. Ignore numbers and punctuation.
#
# Kata: 545cedaa9943f7fe7b000048
#******************************************************************************
import string                           # Didn't use this module in my solution
  
string  = "The quick brown fox jumps over the lazy dog"

def is_pangram(string):
    alphabet  = { }                                   # Empty dictionary
    
    # Dictionary: add all alphanet letters, and set default value to zero
    for letter in range(65,91):                       # Decimal value from A-Z
        alphabet.setdefault(chr(letter), 0)           # Get the char from int
    
    for letter in string.upper():                     # Work with uppercase
        if ( "A" <= letter <= "Z" ):                  # Exclude outside A-Z
          alphabet[letter] += 1                       # Increment value count

    # Check if we have any zero count for any alphebet letter
    if 0 not in alphabet.values():
        return True                                   # It's a pangram
    else:
        return False

print(is_pangram(string))

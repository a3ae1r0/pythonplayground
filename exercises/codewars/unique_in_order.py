#******************************************************************************
# Implement the function unique_in_order which takes as argument a sequence and
# returns a list of items without any elements with the same value next to each
# other and preserving the original order of elements.
#
# For example:
# unique_in_order('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
# unique_in_order('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
# unique_in_order([1,2,2,3,3])       == [1,2,3]
#
# Kata: 54e6533c92449cc251001667
#******************************************************************************
def unique_in_order(sequence):
    list_of_elements = []
    for item in range(len(sequence)):
        # Save item if it is not equal to the previous one. Skip if the current
        # element that we are working is the first one
        if (sequence[item] != sequence[item-1] or item == 0):
            list_of_elements.append(sequence[item])
    return list_of_elements

#--- SAMPLE TESTS ---#
print(unique_in_order('AAAABBBCCDAABBB'))   # ['A', 'B', 'C', 'D', 'A', 'B']
print(unique_in_order('ABBCcAD'))           # ['A', 'B', 'C', 'c', 'A', 'D']
print(unique_in_order([1,2,2,3,3]))         # [1,2,3]
print(unique_in_order('A'))                 # ['A']
print(unique_in_order('AA'))                # ['A']

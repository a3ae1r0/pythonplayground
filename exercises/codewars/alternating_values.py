#******************************************************************************
# Alternating Among Three Values
#
# Suppose a variable x can have only three possible different values a, b and
# c, and you wish to assign to x the value other than its current one, and you
# wish your code to be independent of the values of a, b and c.
#
# What is the most efficient way to cycle among three values? Write a function f so that it satisfies
# f(a) = b
# f(b) = c
# f(c) = a
#
# EXAMPLE:
# f(10, a=10, b=20, c=100) -> 20
# f(20, a=10, b=20, c=100) -> 100
# f(100, a=10, b=20, c=100) -> 10
#
# Kata: 596776fbb4f24d0d82000141
#******************************************************************************
def f(x, a, b, c):
    values = {"a": a, "b": b, "c": c}       # Set dict values from vars values

    # Find the mach between x and each dict value (searching with key/index)
    if (x == values["a"]):
        return values["b"]                  # Return value of "a" key
    elif (x == values["b"]):
        return values["c"]
    else:
        return values["a"]

#--- SAMPLE TESTS ---#
print(f(3, a=3, b=4, c=5), "4")
print(f(5, a=3, b=4, c=5), "3")

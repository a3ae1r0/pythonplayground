#******************************************************************************
# Given an array of integers, find the one that appears an odd number of times.
#
# There will always be only one integer that appears an odd number of times.
# Examples
#
# [7] should return 7, because it occurs 1 time (which is odd).
# [0] should return 0, because it occurs 1 time (which is odd).
# [1,1,2] should return 2, because it occurs 1 time (which is odd).
# [0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
# [1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time 
#                             (which is odd).
#
# Kata: 54da5a58ea159efa38000836
#******************************************************************************
def find_it(seq):
    numDict = {}

    # Populate dictionary with number of occurences
    for digit in seq:
        numDict.setdefault(digit, 0)
        numDict[digit] += 1

    # Return key (int) with a odd number of occurences
    for key, value in numDict.items():          # Get both dict keys and values
        if (value % 2 != 0):
            return key

#--- SAMPLE TESTS ---#
print(find_it([0, 1, 0, 1, 0]))
print(find_it([10, 10]))
print(find_it([10, 10, 10]))
print(find_it([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]))

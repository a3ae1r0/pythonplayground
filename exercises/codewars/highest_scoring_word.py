#******************************************************************************
# Given a string of words, you need to find the highest scoring word.
#
# Each letter of a word scores points according to its position in the
# alphabet: a = 1, b = 2, c = 3 etc.
#
# You need to return the highest scoring word as a string.
#
# If two words score the same, return the word that appears earliest in the
# original string.
#
# All letters will be lowercase and all inputs will be valid.
#
# Kata: 57eb8fcdf670e99d9b000272
#******************************************************************************
def high(src_word):
    # Dictionary with alphabet + scores using dict comprehension
    alphabet_scores     = {chr(97+i): i+1 for i in range(26)}
    all_scr_words       = src_word.lower().split()  # List w/ all words + lower
    highest_score_word  = ""                        # Word with highest score
    highest_score       = 0                         # Score of highest word

    for word in all_scr_words:
        current_word_score = 0
        # Get score for each letter and add to word score
        for letter in word:
            current_word_score += alphabet_scores.get(letter)    # Word total

        # Check if current word has the highest score
        if current_word_score > highest_score:
            # Update score and word values
            highest_score = current_word_score
            highest_score_word = word

    return highest_score_word

#--- SAMPLE TESTS ---#
print(high("man i need a taxi up to ubud"))                    # 'taxi'
print(high("what time are we climbing up the volcano"))        # 'volcano'
print(high("take me to semynak"))                              # 'semynak'
print(high("aa b"))                                            # 'aa'
print(high("b aa"))                                            # 'b'
print(high("bb d"))                                            # 'bb'
print(high("d bb"))                                            # 'd'
print(high("aaa b"))                                           # "aaa"

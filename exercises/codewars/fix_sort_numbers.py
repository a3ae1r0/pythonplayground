#******************************************************************************
# Failed Sort - Bug Fixing #4
#
# Oh no, Timmy's Sort doesn't seem to be working?
# Your task is to fix the sortArray function to sort all numbers in ascending
# order
#
# Kata: 55c7f90ac8025ebee1000062
#******************************************************************************
def sort_array(value):
    # Fix: we can simply and remove all sorted() functions parameters
    # return "".join(sorted(value,key=lambda a: -int(a)))
    return "".join(sorted(value))

#--- SAMPLE TESTS ---#
print(sort_array('12345'))
print(sort_array('54321'))
print(sort_array('34251'))

#******************************************************************************
# Since there are lots of katas requiring you to round numbers to 2 decimal
# places, you decided to extract the method to ease out the process.
#
# And you can't even get this right!
#
# Quick, fix the bug before everyone in CodeWars notices that you can't even
# round a number correctly!
#
# Kata: 5996eb39cdc8eb39f80000a0
#******************************************************************************
import decimal

def round_by_2_decimal_places(n):
    # Add decimal parameters, to allow to round up
    n = decimal.Decimal(n).quantize(decimal.Decimal('0.00'),
                                    rounding=decimal.ROUND_HALF_UP)
    return round(n,2)

    # Edit: Clean solution below from
    # .../kata/reviews/599858d7ec08e2d61c000fe2/groups/5e18885a0c2e9f00013fc742
    # from decimal import Decimal, ROUND_HALF_UP
    # def round_by_2_decimal_places(n):
        # return n.quantize('.01'), rounding=ROUND_HALF_UP

#--- SAMPLE TESTS ---#
print(round_by_2_decimal_places(decimal.Decimal('2.675')))
print(round_by_2_decimal_places(decimal.Decimal('16.765')))

"""
Write an algorithm that will identify valid IPv4 addresses in dot-decimal
format. IPs should be considered valid if they consist of four octets, with
values between 0 and 255, inclusive.

kata: https://www.codewars.com/kata/515decfd9dcfc23bb6000006/
kyu: 6
"""


def is_valid_IP(strng):
    """Return `False` if any validation conditions are not met"""
    input = strng.split('.')

    # Length: 4 octets
    if len(input) != 4:
        return False

    for octet in input:
        # Number: numeric only
        if not octet.isdecimal():
            return False
        # Values: between 0 and 255
        elif not (0 <= int(octet) <= 255):
            return False
        # Zeros: leading zero(s)
        elif octet.startswith('0') and octet != '0':
            return False

    return True


# Sample Tests
assert is_valid_IP('12.255.56.1') is True
assert is_valid_IP('') is False
assert is_valid_IP('abc.def.ghi.jkl') is False
assert is_valid_IP('123.456.789.0') is False
assert is_valid_IP('12.34.56') is False
assert is_valid_IP('12.34.56 .1') is False
assert is_valid_IP('12.34.56.-1') is False
assert is_valid_IP('123.045.067.089') is False
assert is_valid_IP('127.1.1.0') is True
assert is_valid_IP('127.1.1.00') is False
assert is_valid_IP('0.0.0.0') is True
assert is_valid_IP('0.0.0.00') is False
assert is_valid_IP('0.34.82.53') is True
assert is_valid_IP('192.168.1.300') is False

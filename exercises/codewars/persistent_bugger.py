#******************************************************************************
# PERSISTENT BUGGER
#
# Write a function, persistence, that takes in a positive parameter num and
# returns its multiplicative persistence, which is the number of times you must
# multiply the digits in num until you reach a single digit.
#
# For example:
# persistence(39)  # returns 3, because 3*9=27, 2*7=14, 1*4=4
#                  # and 4 has only one digit
# persistence(999) # returns 4, because 9*9*9=729, 7*2*9=126,
#                  # 1*2*6=12, and finally 1*2=2
# persistence(4)   # returns 0, because 4 is already a one-digit number
#
# Kata: 55bf01e5a717a0d57e0000ec
#******************************************************************************
import functools

def persistence(n):
    n_iter = 0                                          # count iterations var
    # Do something if h has more than one-digit (indexes); otherwise return i
    while (n >= 10):
        n = [int(x) for x in str(n)]                    # converts int n to list
        n = functools.reduce(lambda a, b: a*b, n)       # multiply list elemtents
        n_iter += 1
    return n_iter

#--- SAMPLE TESTS ---#
print("Returned:", persistence(39), "Expected: 3")
print("Returned:", persistence(4), "Expected: 0")
print("Returned:", persistence(25), "Expected: 2")
print("Returned:", persistence(999), "Expected: 4")

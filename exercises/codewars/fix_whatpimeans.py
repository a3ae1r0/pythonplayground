#******************************************************************************
# Well, for my first kata, I did a mess. Would you help me, please, to make my
# code work ?
# I'm sure I didn't mix the numbers, but all the rest...
#
# Kata: https://www.codewars.com/kata/57c0484849324c4174000b18/t
#******************************************************************************
from math import pi

def whatpimeans(alpha = 'abcdefghijklmnopqrstuvwxyz'):
    #create a dictionnary linking alphabet to 'secret encryption'
    #dico = {85:'a', 24:'b',32:'c', [...],10:'z'}
    # Fix:
    # - Comprehension values inside parentheses
    # - Parentheses after upper function name
    # Also better sytle/formatting (?)
    scrt_values = [85,24,32,64,11,52,91,79,78,99,62,27,74,
                   35,14,16,66,81,19,39,13,33,45,49,95,10]
    dico = {(k , v) for k, v in zip(scrt_values, alpha.upper())}

    #take the number PI as string and prepare it for decoding
    # Fix:
    # - NameError: name 'math' is not defined when calling Pi
    # - Remove (Pi) dot from string
    # - Reverse string with slices
    crypt = str(pi).replace('.','')
    code = []
        #reverse the string and group 2 by 2 to form a list
    for i in range(0, len(crypt), 2):
        code += crypt[::-1]

    ##take the modified string and try to decode
    # decrypt = ''
    # for binom in code :
    #     decrypt += dico[bino]

    ##stuck somewhere ? try to print() some of the steps
    #print decrypt
    ##hopefully...
    #return decrypt

whatpimeans()

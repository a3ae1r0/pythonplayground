#******************************************************************************
# Digital root is the recursive sum of all the digits in a number.
#
# Given n, take the sum of the digits of n. If that value has more than one
# digit, continue reducing in this way until a single-digit number is
# produced. The input will be a non-negative integer.
# Examples
#     16  -->  1 + 6 = 7
#    942  -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
# 132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
#
# Kata: 541c8630095125aba6000c00
#******************************************************************************
def digital_root(n):
    while int(n/10) != 0:             # Repeat until n is one digit
        n = sum(map(int, str(n)))     # First map n to a list, then sum indexes
    return n

#--- SAMPLE TESTS ---#
print(digital_root(16))          # 7
print(digital_root(942))         # 6
print(digital_root(132189))      # 6
print(digital_root(493193))      # 2

#******************************************************************************
# Polycarpus works as a DJ in the best Berland nightclub, and he often uses
# dubstep music in his performance. Recently, he has decided to take a couple
# of old songs and make dubstep remixes from them.
#
# Let's assume that a song consists of some number of words (that don't contain
# WUB). To make the dubstep remix of this song, Polycarpus inserts a certain
# number of words "WUB" before the first word of the song (the number may be
# zero), after the last word (the number may be zero), and between words (at
# least one between any pair of neighbouring words), and then the boy glues
# together all the words, including "WUB", in one string and plays the song
# at the club.
#
# For example, a song with words "I AM X" can transform into a dubstep remix as
# "WUBWUBIWUBAMWUBWUBX" and cannot transform into "WUBWUBIAMWUBX".
#
# Recently, Jonny has heard Polycarpus's new dubstep track, but since he isn't
# into modern music, he decided to find out what was the initial song that
# Polycarpus remixed. Help Jonny restore the original song.
#
# Input
# The input consists of a single non-empty string, consisting only of uppercase
# English letters, the string's length doesn't exceed 200 characters
#
# Output
# Return the words of the initial song that Polycarpus used to make a dubsteb
# remix. Separate the words with a space.
#
# Examples
# song_decoder("WUBWEWUBAREWUBWUBTHEWUBCHAMPIONSWUBMYWUBFRIENDWUB")
#  =>  WE ARE THE CHAMPIONS MY FRIEND
#
# Kata: 551dc350bf4e526099000ae5
#******************************************************************************
import re

def song_decoder(lyrics):
    # Replace all "WUB" occurrences with whitespaces + remove whitespaces at
    # the start and end of the string
    lyrics = lyrics.replace("WUB", " ").strip()

    # Use regex to replace duplicated (\+1) whitespaces (\s)
    # https://stackoverflow.com/a/49695567
    return re.sub(r'(\s)\1+', '\\1', lyrics)

    # Another possible solution from the solutions section
    # [...]/reviews/555aca9335d4c4602600018c/groups/555e52465b7f06f9ac00006e
    #
    # replace() --> replace unwanted patter with spaces
    # split()   --> create a list of string (each words). This also remove 
    #               leading and trailing spaces (strip() in my code)
    # join()    --> join all strings (words) separated by whitespaces
    # return " ".join(lyrics.replace("WUB", " ").split())

#--- SAMPLE TESTS ---#
print(song_decoder("AWUBBWUBC"))                        # "A B C"
print(song_decoder("AWUBWUBWUBBWUBWUBWUBC"))            # "A B C"
print(song_decoder("WUBAWUBBWUBCWUB"))                  # "A B C"
print(song_decoder("WUBWEWUBAREWUBWUBTHEWUBCHAMPIONSWUBMYWUBFRIENDWUB"))
# Failed random tests after commiting
print(song_decoder("WUUUUU"))                           # "WUUUUU"
# "Q QQ I WW JOPJPBRH"
print(song_decoder("QWUBQQWUBWUBWUBIWUBWUBWWWUBWUBWUBJOPJPBRH"))

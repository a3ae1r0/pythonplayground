'''
Description:
For this exercise you will be strengthening your page-fu mastery. You will
complete the PaginationHelper class, which is a utility class helpful for
querying paging information related to an array.

The class is designed to take in an array of values and an integer indicating
how many items will be allowed per each page. The types of values contained
within the collection/array are not relevant.

The following are some examples of how this class is used:
helper = PaginationHelper(['a','b','c','d','e','f'], 4)
helper.page_count() # should == 2
helper.item_count() # should == 6
helper.page_item_count(0) # should == 4
helper.page_item_count(1) # last page - should == 2
helper.page_item_count(2) # should == -1 since the page is invalid

# page_index takes an item index and returns the page that it belongs on
helper.page_index(5) # should == 1 (zero based index)
helper.page_index(2) # should == 0
helper.page_index(20) # should == -1
helper.page_index(-10) # should == -1 because negative indexes are invalid

kata: https://www.codewars.com/kata/515bb423de843ea99400000a
kyu: 5
'''
import copy


class PaginationHelper:

    # The constructor takes in an array of items and an integer indicating
    # how many items fit within a single page
    def __init__(self, collection, items_per_page):
        self.collection = collection
        self.items_per_page = items_per_page
        self.pagination = self.make_pagination()

    # Do pagination of the collection based on the number of items per page
    def make_pagination(self):
        col = self.collection
        size_col = len(self.collection)
        size_page = self.items_per_page
        return [col[n : n + size_page] for n in range(0, size_col, size_page)]

    # returns the number of items within the entire collection
    def item_count(self):
        return len(self.collection)

    # returns the number of pages
    def page_count(self):
        return len(self.pagination)

    # returns the number of items on the given page. page_index is zero based
    # this method should return -1 for page_index values that are out of range
    def page_item_count(self, page_index):
        if page_index > self.page_count() - 1 or page_index < 0:
            return -1
        return len(self.pagination[page_index])

    # determines what page an item at the given index is on. Zero based
    # indexes. this method should return -1 for item_index values that are out
    # of range
    def page_index(self, item_index):
        if item_index > self.item_count() - 1 or item_index < 0:
            return -1
        return item_index // self.items_per_page


collection = ['a', 'b', 'c', 'd', 'e', 'f']
helper = PaginationHelper(collection, 4)

assert helper.page_count() == 2

assert helper.item_count() == 6

assert helper.page_item_count(0) == 4
assert helper.page_item_count(1) == 2
assert helper.page_item_count(2) == -1

assert helper.page_index(5) == 1
assert helper.page_index(2) == 0
assert helper.page_index(20) == -1
assert helper.page_index(-10) == -1

empty = PaginationHelper([], 10)
assert empty.item_count() == 0
assert empty.page_count() == 0
assert empty.page_index(0) == -1
assert empty.page_index(1) == -1
assert empty.page_index(-1) == -1
assert empty.page_item_count(0) == -1
assert empty.page_item_count(1) == -1
assert empty.page_item_count(-1) == -1

collection = [1, 2, 3, 4, 5, 6]
helper = PaginationHelper(collection, 4)
assert helper.page_count() == 2
assert helper.item_count() == 6

#******************************************************************************
# In this kata you are expected to recover a scattered password in a (m x n)
# grid (you'll be given directions of all password pieces in the array)
#
# The array will contain pieces of the password to be recovered, you'll get
# directions on how to get all the the pieces, your initial position in the
# array will be the character "x".
#
# Heres what the array looks like
# [
#   ["p", "x", "m"],
#   ["a", "$", "$"],
#   ["k", "i", "t"]
# ]
#
# The given directions would consist of [left, right, up, down] and
# [leftT, rightT, upT, downT], the former would be used to move around the
# grid while the latter would be used when you have a password to that
# direction of you.( E.g if you are in a position and the move to make is
# leftT it means theres a password to the left of you, so take the value
# and move there)
#
# So in the 2d array example above, you will be given the directions
# ["lefT", "downT", "rightT", "rightT"], making for the word "pa$$".
#
# Remember you initial position is the character "x".
#
# So you write the function getPassword(grid, directions) that uses
# the directions to get a password in the grid.
#
# Another example.
# grid = [
#   ["a", "x", "c"],
#   ["g", "l", "t"],
#   ["o", "v", "e"]
# ];
#
# directions = ["downT", "down", "leftT", "rightT", "rightT", "upT"]
#
# getPassword(grid, directions) // => "lovet"
#
# Once again, Your initial position is the character "x", so
# from the position of "x" you follow the directions given and
# get all pieces in the grid.
#
# Kata: 58f6e7e455d7597dcc000045
#******************************************************************************
def get_password(grid, directions):
    password = ""                           # Var to save retrieved password

    # Finding the "x" location
    for x in range(len(grid)):              # Loop through all rows
        for y in range(len(grid[0])):       # Loop through all columns
            if ( grid[x][y] == "x" ):       # Check if the value is a "x"
                # Save coordinates as a starting position --> grid[x][y]
                row     = x
                column  = y

    for instruction in directions:
        # Movement
        if ( instruction == "left"):
            column -= 1
        elif ( instruction == "right"):
            column += 1
        elif ( instruction == "up"):
            row -= 1
        elif ( instruction == "down"):
            row += 1

        # Movement + get/save chars
        if ( instruction == "leftT"):
            column -= 1
            password += grid[row][column]
        elif ( instruction == "rightT"):
            column += 1
            password += grid[row][column]
        elif ( instruction == "upT"):
            row -= 1
            password += grid[row][column]
        elif ( instruction == "downT"):
            row += 1
            password += grid[row][column]

    return password

#--- SAMPLES TESTS ---#
# 1
grid = [
    ["x", "l", "m"],
    ["o", "f", "c"],
    ["k", "i", "t"]
]
directions = ["rightT", "down", "leftT", "right", "rightT", "down", "left",
              "leftT"]
print(get_password(grid, directions))   # "lock"

# 2
grid = [
  ["a", "x", "c"],
  ["g", "l", "t"],
  ["o", "v", "e"]
];
directions = ["downT", "down", "leftT", "rightT", "rightT", "upT"]
print(get_password(grid, directions))   # "lovet"

# 3
grid = [
    ['i', 'd', 'u'],
    ['+', 'x', 'l'],
    ['f', 'm', 'e'],
    ['-', 'y', 'h']
]
directions = ['upT', 'rightT', 'downT']
print(get_password(grid, directions))   # "dul"

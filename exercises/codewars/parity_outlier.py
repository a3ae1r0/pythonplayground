#******************************************************************************
# You are given an array (which will have a length of at least 3, but could be
# very large) containing integers. The array is either entirely comprised of
# odd integers or entirely comprised of even integers except for a single
# integer N. Write a method that takes the array as an argument and returns
# this "outlier" N.
#
# Examples
# [2, 4, 0, 100, 4, 11, 2602, 36]
# Should return: 11 (the only odd number)
#
# [160, 3, 1719, 19, 11, 13, -21]
# Should return: 160 (the only even number)
#
# Kata: 5526fc09a1bbd946250002dc
#******************************************************************************
def find_outlier(integers):
    odd  = []       # Required dict in order to return the outlier integer
    even = []
    for n in integers:
        # Calculate if n is odd or even
        if n % 2 != 0:
            odd.append(n)
        else:
            even.append(n)
        # Break if already found the outlier
        if len(odd) > 1 and len(even) == 1:
            return even[0]
        if len(even) > 1 and len(odd) == 1:
            return odd[0]

print(find_outlier([2, 4, 6, 8, 10, 3]))
print(find_outlier([160, 3, 1719, 19, 11, 13, -21]))

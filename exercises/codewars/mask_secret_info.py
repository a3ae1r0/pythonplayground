###############################################################################
# Your task is to write a function maskify, which changes all but the last four
# characters into '#'.
#
# maskify("4556364607935616") == "############5616"
# maskify(     "64607935616") ==      "#######5616"
# maskify(               "1") ==                "1"
# maskify(                "") ==                 ""
#
# "What was the name of your first pet?"
# maskify("Skippy")                                   == "##ippy"
# maskify("Nananananananananananananananana Batman!")
# == "####################################man!"
#
# Kata: 5412509bd436bd33920011bc
###############################################################################
#--- SOLUTION START ---#
def maskify(cc):
    return "#" * (len(cc) - 4) + cc[-4:]

#--- SOLUTION END ---#

#--- SAMPLE TESTS ---#
print(maskify("4556364607935616"))
print(maskify("64607935616"))
print(maskify("1"))
print(maskify("Skippy"))
print(maskify("Nananananananananananananananana Batman!"))

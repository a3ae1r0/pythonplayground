"""
Complete the solution so that it strips all text that follows any of a set of
comment markers passed in. Any whitespace at the end of the line should also be
stripped out.

Example:

Given an input string of:
    apples, pears # and bananas
    grapes
    bananas !apples

The output expected would be:
    apples, pears
    grapes
    bananas

The code would be called like so:
    result = solution(
        "apples, pears # and bananas\ngrapes\nbananas !apples", ["#", "!"]
    )
    # result should == "apples, pears\ngrapes\nbananas"

kata: https://www.codewars.com/kata/51c8e37cee245da6b40000bd/
kyu: 4
"""


def strip_comments(s, markers):
    """Check if any comment marker is present, iterate over them and use slices
    to update list (lines) to remove comments
    """
    out = s.split('\n')

    for i in range(len(out)):
        comments = [marker for marker in markers if marker in out[i]]
        if comments:
            for marker in comments:
                # Make sure the marker were not removed in previous iteration
                if out[i].find(marker) != -1:
                    out[i] = (out[i][:out[i].find(marker)]).rstrip()

    return "\n".join(out)


# Sample tests
assert (
    strip_comments(
        'apples, pears # and bananas\ngrapes\nbananas #!apples', ['#', '!']
    )
    == 'apples, pears\ngrapes\nbananas'
)
assert strip_comments('a #b\nc\nd $e f g', ['#', '$']) == 'a\nc\nd'
assert strip_comments(' a #b\nc\nd $e f g', ['#', '$']) == ' a\nc\nd'
assert (
    strip_comments(
        "\tstrawberries ^\n- apples bananas ' oranges\navocados\n.pears @",
        ['=', '!', '#', '?', '@', '^', '.', '-', ','],
    )
    == '\tstrawberries\n\navocados\n'
)

"""
A child is playing with a ball on the nth floor of a tall building. The height
of this floor, h, is known.

He drops the ball out of the window. The ball bounces (for example), to
two-thirds of its height (a bounce of 0.66).

His mother looks out of a window 1.5 meters from the ground.

How many times will the mother see the ball pass in front of her window
(including when it's falling and bouncing?

Three conditions must be met for a valid experiment:
- Float parameter "h" in meters must be greater than 0
- Float parameter "bounce" must be greater than 0 and less than 1
- Float parameter "window" must be less than h.

If all three conditions above are fulfilled, return a positive integer,
otherwise return -1.

Note: The ball can only be seen if the height of the rebounding ball is
strictly greater than the window parameter. Examples:

- h = 3, bounce = 0.66, window = 1.5, result is 3
- h = 3, bounce = 1, window = 1.5, result is -1
(Condition 2) not fulfilled).

kata: https://www.codewars.com/kata/5544c7a5cb454edb3c000047
kyu: 6
"""


def bouncing_ball(h, bounce, window):
    # Conditions to be met
    if not (h > 0 and 0 < bounce < 1 and window < h):
        return -1

    # First ball pass (down) and initial bouncing
    seen = 1
    bouncing = h * bounce

    # See ball (falling and bouncing back) and new bouncing
    while bouncing > window:
        seen += 2
        bouncing = bouncing * bounce

    return seen


# Fixed Tests
print(bouncing_ball(3, 1, 1.5))             # -1
print(bouncing_ball(3, 0.66, 1.5))          # 3
print(bouncing_ball(30, 0.66, 1.5))         # 15
print(bouncing_ball(30, 0.75, 1.5))         # 21

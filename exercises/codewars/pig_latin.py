"""
Move the first letter of each word to the end of it, then add "ay" to the end
of the word. Leave punctuation marks untouched.
Examples:
pig_it('Pig latin is cool') # igPay atinlay siay oolcay
pig_it('Hello world !')     # elloHay orldway !

kata: https://www.codewars.com/kata/520b9d2ad5c005041100000f/
kyu: 5
"""
import string


def pig_it(text):
    words = text.split()

    for word in range(len(words)):
        if words[word].isalpha():
            words[word] = words[word][1:] + words[word][0] + 'ay'
        # Case: 'foobar!'
        elif (words[word][:-1].isalpha() and words[word][-1] in
              string.punctuation):
            words[word] = (words[word][1:-1] + words[word][0] + 'ay' +
                           words[word][-1])

    return ' '.join(words)


# Sample tests
assert pig_it('Pig latin is cool') == 'igPay atinlay siay oolcay'
assert pig_it('This is my string') == 'hisTay siay ymay tringsay'
assert pig_it('O tempora o mores !') == 'Oay emporatay oay oresmay !'
assert pig_it('Mars!') == 'arsMay!'

#******************************************************************************
# It's a Pokemon battle! Your task is to calculate the damage that a particular
# move would do using the following formula (not the actual one from the game):
# damage = 50 * (attack / defense) * effectiveness
#
# Effectiveness:
# - Super effective: 2x damage
# - Neutral: 1x damage
# - Not very effective: 0.5x damage
#
# Here is the effectiveness of each matchup:
# - fire > grass
# - fire < water
# - fire = electric
# - water < grass
# - water < electric
# - grass = electric
#
# For this kata, any type against itself is not very effective. Also, assume
# that the relationships between different types are symmetric (if A is super
# effective against B, then B is not very effective against A).
#
# Kata: 536e9a7973130a06eb000e9f
#******************************************************************************
import math

def calculate_damage(your_type, opponent_type, attack, defense):
    effectiveness = {
        "fire":  {
            "fire": 0.5,
            "water": 0.5,
            "grass": 2,
            "electric": 1
        },
        "water": {
            "fire" : 2,
            "water": 0.5,
            "grass": 0.5,
            "electric": 0.5
        },
        "grass": {
            "fire": 0.5,
            "water": 2,
            "grass": 0.5,
            "electric": 1
        },
        "electric": {
            "fire": 1,
            "water": 2,
            "grass": 1,
            "electric": 0.5
        }
    }

    # Return value rounded upward to nearest integer
    return math.ceil(50 * (attack / defense) *
                     effectiveness[your_type][opponent_type])

#--- SAMPLE TESTS ---#
print(calculate_damage("grass", "water", 100, 100))         # 100
print(calculate_damage("electric", "fire", 100, 100))       # 50
print(calculate_damage("grass", "electric", 57, 19))        # 150
print(calculate_damage("grass", "water", 40, 40))           # 100
print(calculate_damage("grass", "fire", 35, 5))             # 175
print(calculate_damage("fire", "electric", 10, 2))          # 250

#
# Given an array length 1 or more of ints, return the difference
# between the largest and smallest values in the array.
#
# Note: the built-in min(v1, v2) and max(v1, v2) functions
# return the smaller or larger of two values.)
#
# https://codingbat.com/prob/p184853
#

#--- SOLUTION START ---#
def big_diff(nums):
    # start point: assign 1st element of list to both variables
    smallest = nums[0]
    largest  = nums[0]
    # length -1 because we are using the next element
    # as a 2nd value for min() and max()
    for n in range(len(nums) - 1):
        # check if is smallest that the previous stored value
        if min(nums[n], nums[n+1]) < smallest:
            # if yes, SAVED
            smallest = min(nums[n], nums[n+1])
        # do the same for largest
        if max(nums[n], nums[n+1]) > largest:
            largest = max(nums[n], nums[n+1])
    return largest - smallest
#--- SOLUTION END ---#

#--- SAMPLES ---#
print(big_diff([10, 3, 5, 6]))
print(big_diff([7, 2, 10, 9]))
print(big_diff([2, 10, 7, 2]))

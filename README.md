# Python Playground
A place for learning and play with Python

## In this repo...
* [**Books**](./books): learning from paper (or online...)
* [**Exercises**](./exercises): small exercises and challenges (e.g. katas)
* [**Projects**](./projects): slightly more complex programs
